在线客服系统
===============

> 运行环境要求PHP7.1+。

部分截图（新版UI）

![image.png](https://s3.bmp.ovh/imgs/2022/03/a78fe19a2a69a717.png)
![image.png](https://s3.bmp.ovh/imgs/2022/03/aee64f56cf78ffbf.png)
![image.png](https://s3.bmp.ovh/imgs/2022/03/2e297eae019ab2ca.png)

部分截图（老版UI）

![image.png](https://s3.bmp.ovh/imgs/2021/08/f34d5220e74d329a.png)

![image.png](https://s3.bmp.ovh/imgs/2021/08/4ae080db68f9e322.png)


## 主要新特性

* 项目使用thinkphp6+layui搭建
* 在线客服使用workman即时通讯
* 每个IM分组的后台账户均可视为独立的平台接入，互不影响



## 安装

~~~
运行环境 推荐 nginx + php7.3  其他版本并未测试

1、git clone https://gitee.com/tubaibai/online-customer.git 下载源代码
2、composer install 安装外部扩展

然后直接运行项目，会自动导入sql数据库

3、运行worker服务器  
启动 php think worker:gateway
守护进程启动 php think worker:gateway -d
停止 php think worker:gateway stop

~~~

##  nginx伪静态配置
~~~
niginx 伪静态
location / {
try_files $uri $uri/ /index.php?s=$uri&$query_string;
}

location ~ .php$ {
try_files $uri =404;
include fastcgi.conf;
fastcgi_pass 127.0.0.1:9000;
}
~~~


运行目录
~~~
public
~~~

## thinkphp6开发文档

[完全开发手册](https://www.kancloud.cn/manual/thinkphp6_0/content)

## 关于

原小牛AdminQQ交流群：1027226338

二次开发作者联系方式：1020857701

因为不经常查看仓库留言，所以在安装或者使用上遇到什么问题，可以加我咨询解决


## 更新日志
##### 2021-8-11   兔白白 v2.2
* 基本上完善了会话咨询，部分bug延后修改
