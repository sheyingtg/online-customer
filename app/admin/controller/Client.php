<?php
namespace app\admin\controller;

use app\common\controller\AdminBase;
use app\common\model\ClientModel;
use app\common\model\AuthRule;
use think\Db;
use think\facade\Session;

class Client extends AdminBase
{

    //首页
    function Index(){
        if($this->request->isAjax()){
            $param = $this->request->param();
            $model = new ClientModel();
            $list = $model->getList($param);
            foreach ($list as $k=>$v){
                $v->create_time_text.='';
                $v->update_time_text.='';
            }
            $this->success('ok',"",$list);
        }
        $reData = [
            
        ];
        return view('',$reData);
    }

    //新增or编辑数据
    function edit(){
        if( $this->request->isPost() ) {
            $param = $this->request->param();
            
            $result = ClientModel::saveData($param);
            if( $result ) {
                $this->success('操作成功');
            } else {
                $this->error('操作失败');
            }
        }
        $id = $this->request->get('id');
        $copy = $this->request->get('copy');
        $data = [];
        if(!empty($id)){
            $data = ClientModel::where('id',$id)->find();
            if($copy==1){
                unset($data['id']);
            }
            
        }
        $reData = [
            'data'=>$data,
            
            
        ];
        return view('index_edit',$reData);
    }


    function autoLogin(){
        $id = $this->request->get('id');
        $admin = Session::get('admin_auth');
        //?secret=123456&userid=123&depart_id=1&staff_id=1&username=兔白白
        $info = ClientModel::getInfo($id);
        $params = [
            'secret'=>$admin['secret'],
            'userid'=>$info['uid'],
            'username'=>$info['name'],
            'mobile'=>$info['mobile'],
            'depart_id'=>$info['depart_id'],
            'staff_id'=>$info['staff_id'],
            'headimg'=>$info['headimg'],
        ];
        $this->redirect(url('home/login/AccessLogin',$params));
    }

    /**
     * 删除节点
     */
    public function delete()
    {
        $id = $this->request->param('id');
        !($id>1) && $this->error('参数错误');
        ClientModel::where([['id','in',$id]])->delete();
        $this->success('删除成功');
    }

    //数据导出
    public function exportData(){
        $param = $this->request->param();
        $model = new ClientModel();
        $exportData = $model->getExport($param,'客户列表列表');
        if($exportData){
            $this->success('ok','',$exportData);
        }
        $this->error('没有数据被导出！');
    }
}
