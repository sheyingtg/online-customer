<?php
// +----------------------------------------------------------------------
// | 小牛Admin
// +----------------------------------------------------------------------
// | Website: www.xnadmin.cn
// +----------------------------------------------------------------------
// | Author: dav <85168163@qq.com>
// +----------------------------------------------------------------------

namespace app\admin\controller;

use app\common\controller\AdminBase;
use \think\facade\Config as ConfigModel;
use think\facade\View;

class Config extends AdminBase
{
    //配置文件名
    protected $filename = 'tutu_system';

    /**
     * 基本配置
     * @return \think\response\View
     */
    public function base()
    {
        $labelList = [
            //基本配置
            'base'=>[
                ['title'=>'系统名称','name'=>'sys_name','type'=>'text','placeholder'=>'请填写系统名称'],
                ['title'=>'Logo','name'=>'logo','type'=>'img'],
                ['title'=>'网站名称','name'=>'sitename','type'=>'text','placeholder'=>'请填写网站名称'],
                ['title'=>'首页标题','name'=>'title','type'=>'text','placeholder'=>'请填写首页标题'],
                ['title'=>'META关键词','name'=>'keywords','type'=>'textarea','placeholder'=>'请填写META关键字'],
                ['title'=>'META描述','name'=>'description','type'=>'textarea','placeholder'=>'请填写META描述'],
                ['title'=>'版权信息','name'=>'copyright','type'=>'textarea','placeholder'=>'请填写版权信息'],
            ],
            //其他配置
            'qita'=>[
                ['title'=>'登录验证码','name'=>'login_vercode','type'=>'checkbox'],
//                ['title'=>'调试模式','name'=>'app_debug','type'=>'checkbox'],
            ],
        ];
        View::assign('labelList',$labelList);

        return $this->_init();
    }

    /**
     * 上传配置
     * @return \think\response\View
     */
    public function upload()
    {
        $labelList = [
            //基本配置
            'oss'=>[
                ['title'=>'AccessKey','name'=>'oss_ak','type'=>'text'],
                ['title'=>'AccessKeySecret','name'=>'oss_sk','type'=>'text'],
                ['title'=>'Endpoint','name'=>'oss_endpoint','type'=>'text'],
                ['title'=>'Bucket','name'=>'oss_bucket','type'=>'text'],
            ],
            //其他配置
            'qiniu'=>[
                ['title'=>'AccessKey','name'=>'qiniu_ak','type'=>'text'],
                ['title'=>'AccessKeySecret','name'=>'qiniu_sk','type'=>'text'],
                ['title'=>'域名','name'=>'qiniu_domain','type'=>'text'],
                ['title'=>'Bucket','name'=>'qiniu_bucket','type'=>'text'],
            ],
        ];
        View::assign('labelList',$labelList);
        return $this->_init();
    }

    /**
     *  支付设置
     */
    public function payment(){
        $labelList = [
            'weixin'=>[
                ['title'=>'公众号支付状态','name'=>'open','type'=>'checkbox'],
                ['title'=>'公众号(AppID)','name'=>'appid','type'=>'text'],
                ['title'=>'应用密钥(appsecret)','name'=>'secret','type'=>'text'],
                ['title'=>'商户号(MchId)','name'=>'mchid','type'=>'text'],
                ['title'=>'支付密钥(md5)','name'=>'secretkey','type'=>'text'],
            ],
            'alipay'=>[
                ['title'=>'支付状态','name'=>'open','type'=>'checkbox'],
                ['title'=>'支付宝AppID','name'=>'appid','type'=>'text'],
                ['title'=>'支付宝私钥(rsaPrivateKey)','name'=>'privateKey','type'=>'textarea'],
                ['title'=>'支付宝应用公匙(rsaPublicKey)','name'=>'publicKey','type'=>'textarea'],
            ],
        ];
        View::assign('labelList',$labelList);
        return $this->_init();
    }

    //默认设置
    public function _init(){
        $file = $this->request->action();

        if( $this->request->isPost() ) {
            $param = $this->request->post();
            $this->_set($param, $file);
            $this->success('设置成功');
        }
        return view($file,['data'=>$this->_load($file)]);
    }

    /**
     * 写入配置文件
     * @param $param
     * @param string $filename
     */
    protected function _set($value,$key)
    {
            $file = config_path() . $this->filename . '.php';
            $cfg = [];
            if(file_exists($file)){
                $cfg = include $file;
            }
            $item = explode('.', $key);
            switch (count($item)) {
                case 1:
                    $cfg[$item[0]] = $value;
                    break;
                case 2:
                    $cfg[$item[0]][$item[1]] = $value;
                    break;
            }
            return file_put_contents($file, "<?php\nreturn " . var_export($cfg, true) . ";");
    }


    /**
     * 加载配置文件
     * @param $filename
     * @return array
     */
    protected function _load($filename)
    {
        $data = ConfigModel::get($this->filename.'.'.$filename);
        return $data;
    }
}
