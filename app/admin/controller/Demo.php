<?php
namespace app\admin\controller;

use app\common\controller\AdminBase;
use app\common\model\DemoModel;
use app\common\model\AuthRule;
use think\Db;

class Demo extends AdminBase
{

                                                        //测试
    public $arr = [
        '1'=>1,
        2=>11,
        2=>11,//测试不复写
        2=>11,
    ];

    /**
     * @var 测试
     */
    public $a;//测试

//新的方法
    static public function new_code()
    {
        $a = 1;
        $b =2 ;
        if($a==1){
            return $a+$b;
        }else{
            return $a-$b;
        }
        //这是段随意的 无用代码，尝试合并到新的文件中
    }

/**
     * 新的方法
     * @return int
     */
    function new_code2(){
        $a = 1;
        $b =2 ;
        return $a+$b;
        //这是段随意的 无用代码，尝试合并到新的文件中

    }


    //首页
    function Index(){
        if($this->request->isAjax()){
            $param = $this->request->param();
            $model = new DemoModel();
            $list = $model->getList($param);
            foreach ($list as $k=>$v){
                $v->create_time_text.='';
                $v->update_time_text.='';
            }
            $this->success('ok',"",$list);
        }
        $reData = [
            
        ];
        return view('',$reData);
    }

    //新增or编辑数据
    function edit(){
        if( $this->request->isPost() ) {
            $param = $this->request->param();
            
            $result = DemoModel::saveData($param);
            if( $result ) {
                $this->success('操作成功');
            } else {
                $this->error('操作失败');
            }
        }
        $id = $this->request->get('id');
        $copy = $this->request->get('copy');
        $data = [];
        if(!empty($id)){
            $data = DemoModel::where('id',$id)->find();
            if($copy==1){
                unset($data['id']);
            }
            
        }
        $reData = [
            'data'=>$data,
            
            
        ];
        return view('index_edit',$reData);
    }

    /**
     * 删除节点
     */
    public function delete()
    {
        $id = $this->request->param('id');
        !($id>1) && $this->error('参数错误');
        DemoModel::where([['id','in',$id]])->delete();
        $this->success('删除成功');
    }

    //数据导出
    public function exportData(){
        $param = $this->request->param();
        $model = new DemoModel();
        $exportData = $model->getExport($param,'curd功能演示demo列表');
        if($exportData){
            $this->success('ok','',$exportData);
        }
        $this->error('没有数据被导出！');
    }
}
