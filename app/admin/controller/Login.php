<?php
// +----------------------------------------------------------------------
// | 小牛Admin
// +----------------------------------------------------------------------
// | Website: www.xnadmin.cn
// +----------------------------------------------------------------------
// | Author: dav <85168163@qq.com>
// +----------------------------------------------------------------------

namespace app\admin\controller;

use app\common\controller\Base;
use app\common\model\Admin;
use think\captcha\facade\Captcha;
use think\exception\ValidateException;
use think\facade\Session;
use utils\Jwt;

class Login extends Base
{
    public function index()
    {
//        $config = (new \think\Config())->get('connections');
//        dump($config);exit;
        if( $this->request->isPost() ) {
            $param = $this->request->param();
            try {
                $this->validate($param,'login');
            } catch (ValidateException $e) {
                $this->error($e->getError());
            }

            //是否开启验证码
            if( xn_cfg('base.login_vercode') == 1 ) {
                if( !captcha_check($param['vercode']) ) {
                    $this->error('验证码错误');
                }
            }
            $admin_data = Admin::where([
                'username' => $param['username'],
                'password' => xn_encrypt($param['password']),
            ])->field('id,username,status,last_login_ip,last_login_time,avatar,secret')->find();

            if( empty($admin_data) ) {
                $this->error('用户名或密码不正确');
            }
            if($admin_data['status']!=1) {
                $this->error('您的账户已被禁用');
            }
            if(empty($admin_data['secret'])){
                Admin::createSecret($admin_data['id']);
            }
            //获取用户的角色
            $admin_data['role_name'] = $admin_data->auth_group_access[0]['title'];
            Session::set('admin_auth', $admin_data);

            $this->success('登录成功', url('admin/index'));
        }
        return view();
    }

    public function logout()
    {
        Session::set('admin_auth',null);
        $this->redirect(url('index'));
    }

    //快捷登录
    public function quick(){
        $user_token = input('user_token');
        $user_info = Jwt::verifyToken($user_token);
        if($user_info &&$user_info['sub']){
            $admin_data = Admin::where([
                'username' => $user_info['sub'],
            ])->field('id,username,status,last_login_ip,last_login_time,avatar,secret')->find();
            if($admin_data['status']!=1) {
                $this->error('您的账户已被禁用');
            }
            if(empty($admin_data['secret'])){
                Admin::createSecret($admin_data['id']);
            }
            //获取用户的角色
            $admin_data['role_name'] = $admin_data->auth_group_access[0]['title'];
            Session::set('admin_auth', $admin_data);
            $this->success('登录成功', url('/admin/index'));
        }else{
            $this->error("非法访问",url('/admin/login'));
        }
    }
    
    /**
     * 生成验证码
     * @return \think\Response
     */
    public function verify()
    {
        return Captcha::create();
    }
}
