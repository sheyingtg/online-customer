<?php
namespace app\admin\controller;

use app\common\controller\AdminBase;
use app\common\model\MemberLogs as MemberLogsModel;
use think\Db;

class MemberLogs extends AdminBase
{
    //首页
    function Index(){
        if($this->request->isAjax()){
            $param = $this->request->param();
            $model = new MemberLogsModel();
            $list = $model->getList($param);
            foreach ($list as $k=>$v){
                $list[$k]['role_text'] = $v->role_text;
            }
            $this->success('ok',"",$list);
        }
        $reData = [
            'roleList'=>MemberLogsModel::$roleList,
            'statusList'=>MemberLogsModel::$statusList,
        ];
        return view('',$reData);

    }

    //新增or编辑数据
    function edit(){
        if( $this->request->isPost() ) {
            $param = $this->request->param();
            if(empty($param['id'])){
                //新增
                $result = MemberLogsModel::insertGetId($param);
            }else{
                //更新
                $result = MemberLogsModel::update($param);
            }
            if( $result ) {
                $this->success('操作成功');
            } else {
                $this->error('操作失败');
            }
        }
        $id = $this->request->get('id');
        $copy = $this->request->get('copy');
        $data = [];
        if(!empty($id)){
            $data = MemberLogsModel::where('id',$id)->find();
            if($copy==1){
                unset($data['id']);
            }
        }
        $reData = [
            'data'=>$data,
            'roleList'=>MemberLogsModel::$roleList,
            'statusList'=>MemberLogsModel::$statusList,
            
        ];
        return view('index_edit',$reData);
    }

    /**
     * 删除节点
     */
    public function delete()
    {
        $id = $this->request->param('id');
        !($id>1) && $this->error('参数错误');
        MemberLogsModel::where([['id','in',$id]])->delete();
        $this->success('删除成功');
    }

    //数据导出
    public function exportData(){
        $param = $this->request->param();
        $model = new MemberLogsModel();
        $where = $model->getListWhere($param);
        $exportData = $model->getExport($where,'用户管理1列表');
        if($exportData){
            $this->success('ok','',$exportData);
        }
        $this->error('没有数据被导出！');
    }
}
