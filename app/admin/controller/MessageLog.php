<?php
namespace app\admin\controller;

use app\common\controller\AdminBase;
use app\common\model\MessageLogModel;
use app\common\model\AuthRule;
use think\Db;

class MessageLog extends AdminBase
{

    


    //首页
    function Index(){
        if($this->request->isAjax()){
            $param = $this->request->param();
            $model = new MessageLogModel();
            $list = $model->getList($param);
            foreach ($list as $k=>$v){
                $v->type_text.='';
            }
            $this->success('ok',"",$list);
        }
        $reData = [
            'typeList'=>MessageLogModel::$typeList,
            
        ];
        return view('',$reData);
    }

    //新增or编辑数据
    function edit(){
        if( $this->request->isPost() ) {
            $param = $this->request->param();
            
            $result = MessageLogModel::saveData($param);
            if( $result ) {
                $this->success('操作成功');
            } else {
                $this->error('操作失败');
            }
        }
        $id = $this->request->get('id');
        $copy = $this->request->get('copy');
        $data = [];
        if(!empty($id)){
            $data = MessageLogModel::where('id',$id)->find();
            if($copy==1){
                unset($data['id']);
            }
            
        }
        $reData = [
            'data'=>$data,
            
            'typeList'=>MessageLogModel::$typeList,
            
        ];
        return view('index_edit',$reData);
    }

    /**
     * 删除节点
     */
    public function delete()
    {
        $id = $this->request->param('id');
        !($id>1) && $this->error('参数错误');
        MessageLogModel::where([['id','in',$id]])->delete();
        $this->success('删除成功');
    }

    //数据导出
    public function exportData(){
        $param = $this->request->param();
        $model = new MessageLogModel();
        $exportData = $model->getExport($param,'消息列表列表');
        if($exportData){
            $this->success('ok','',$exportData);
        }
        $this->error('没有数据被导出！');
    }
}
