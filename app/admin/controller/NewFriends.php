<?php
namespace app\admin\controller;

use app\common\controller\AdminBase;
use app\common\model\NewFriendsModel;
use app\common\model\AuthRule;
use think\Db;

class NewFriends extends AdminBase
{

    


    //首页
    function Index(){
        if($this->request->isAjax()){
            $param = $this->request->param();
            $model = new NewFriendsModel();
            $list = $model->getList($param);
            foreach ($list as $k=>$v){
                $v->status_text.='';
                $v->create_time_text.='';
            }
            $this->success('ok',"",$list);
        }
        $reData = [
            'statusList'=>NewFriendsModel::$statusList,
            
        ];
        return view('',$reData);
    }

    //新增or编辑数据
    function edit(){
        if( $this->request->isPost() ) {
            $param = $this->request->param();
            
            $result = NewFriendsModel::saveData($param);
            if( $result ) {
                $this->success('操作成功');
            } else {
                $this->error('操作失败');
            }
        }
        $id = $this->request->get('id');
        $copy = $this->request->get('copy');
        $data = [];
        if(!empty($id)){
            $data = NewFriendsModel::where('id',$id)->find();
            if($copy==1){
                unset($data['id']);
            }
            
        }
        $reData = [
            'data'=>$data,
            
            'statusList'=>NewFriendsModel::$statusList,
            
        ];
        return view('index_edit',$reData);
    }

    /**
     * 删除节点
     */
    public function delete()
    {
        $id = $this->request->param('id');
        !($id>1) && $this->error('参数错误');
        NewFriendsModel::where([['id','in',$id]])->delete();
        $this->success('删除成功');
    }

    //数据导出
    public function exportData(){
        $param = $this->request->param();
        $model = new NewFriendsModel();
        $exportData = $model->getExport($param,'新的好友申请记录列表');
        if($exportData){
            $this->success('ok','',$exportData);
        }
        $this->error('没有数据被导出！');
    }
}
