<?php


namespace app\admin\controller;


use app\common\components\Wxpay;
use app\common\controller\AdminBase;
use app\common\controller\Base;
use app\common\exception\ApiException;
use app\common\model\Payment;
use app\common\Notices\WxpayNotice;
use app\common\services\admin\PayService;
use app\common\Notices\AlipayNotice;
use think\facade\Config as ConfigModel;
use think\facade\View;

class PayDemo extends Base
{

    //支付测试
    function pay()    {
        $file = $this->request->action();
        $ordersn = getOrdersn(1,'S');
        $labelList = [
            //支付
            'pay'=>[
                ['title'=>'支付标题','name'=>'title','type'=>'text','default'=>'支付测试'],
                ['title'=>'支付单号','name'=>'out_trade_no','type'=>'text','default'=>$ordersn],
                ['title'=>'支付金额','name'=>'money','type'=>'text','default'=>'0.01'],
            ],
            //退款
            'tuikuan'=>[
                ['title'=>'订单单号','name'=>'out_trade_no','type'=>'text','default'=>''],
                ['title'=>'退款金额','name'=>'money','type'=>'text','default'=>''],
            ],
        ];
        View::assign('labelList',$labelList);
        return view($file);
    }

    //获取支付记录列表
    function getPaymentList(){
        if($this->request->isAjax()){
            $param = $this->request->param();
            $model = new Payment();
            $list = $model->getList($param);
            foreach ($list as $k=>$v){
                $list[$k]->status_text .= '';
                $list[$k]['create_time_text'] = $v->create_time_text;
            }
            $this->success('ok',"",$list);
        }
        $reData = [
            'statusList'=>Payment::$statusList,
        ];
        return view('',$reData);
    }

    //确认提交 支付（微信支付）
    function ajax_pay(){
        $param = $this->request->param('ceshi');
        $type = $this->request->param('type');
//        $param['money'] = $param['money']?:0.01;
        if(empty($param['out_trade_no'])){
            $param['out_trade_no'] = getOrdersn(1,'O');
        }
//        $openId = 'oeZg85LqMdEzmx3sjlWUVauWyRgE';
        $openId = 'o72cD5C_AH49FWz7UMuvUjM4JlV8';
        $payConfig = PayService::pay([
            'money'=>$param['money'],
            'out_trade_no'=>$param['out_trade_no'],
            'openid'=>$openId,
            'product_id'=>1,
            'notify_url'=>get_host().'/admin/payDemo/wxPayNotify',//留空使用默认的回调地址
//            'notify_url'=>'http://paidan.funwin.cn/admin/payDemo/wxPayNotify',//留空使用默认的回调地址
        ],$type);
        $this->success('ok','',$payConfig);
    }

    // 确认提交 退款
    function ajax_tuikaun(){
        $param = $this->request->param('ceshi');
        if(empty($param['out_trade_no'])){
            throw new ApiException('缺少商户订单号');
        }
        $data = PayService::refund($param['money'],$param['out_trade_no']);
        $msg = '';
        if($data['return_msg']=='OK'){
            $msg = "订单已支付，支付金额：".($data['cash_fee']/100);
        }
        $this->success($msg,'',$data);
    }

    //查询订单
    function orderQuery(){
        $param = $this->request->param('ceshi');
        if(empty($param['out_trade_no'])){
            throw new ApiException('缺少商户订单号');
        }
        $data = Wxpay::orderQuery($param['out_trade_no']);
        if($data['trade_state']=='SUCCESS'){
            $msg = "订单已支付，支付金额：".($data['cash_fee']/100);
        }else{
            $msg = $data['trade_state_desc']." 订单金额：".($data['total_fee']/100);
        }
        $this->success($msg,'',$data);
    }

    // 微信支付回调
    function wxPayNotify(){
        $notify = new WxpayNotice();
        return $notify->sendXcxapiProcess();
    }

    //支付宝支付回调
    function alipayNotify(){
        $notify = new AlipayNotice();
        return $notify->sendProcess();
    }
}