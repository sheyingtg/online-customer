<?php
namespace app\admin\controller;

use app\common\controller\AdminBase;
use app\common\model\DepartmentModel;
use app\common\model\StaffModel;
use app\common\model\AuthRule;
use think\Db;
use think\facade\Session;
use utils\Jwt;

class Staff extends AdminBase
{
    //首页
    function Index(){
        if($this->request->isAjax()){
            $param = $this->request->param();
            $model = new StaffModel();
            $list = $model->getList($param);
            foreach ($list as $k=>$v){
                //检查是否在线
                $v->online_text.='';

                $v->depart_id_text.='';
                $v->work_status_text.='';
                $v->create_time_text.='';
                $v->update_time_text.='';
                $v->login_time_text.='';
            }
            $this->success('ok',"",$list);
        }
        $adminData = Session::get('admin_auth');
        $reData = [
            'depart_idList'=>StaffModel::getDepartmentList(),
            'statusList'=>StaffModel::$statusList,
            'work_statusList'=>StaffModel::$work_statusList,
            'staffUrl'=>[
                'hostUrl'=>get_host(),
                'secret'=>$adminData['secret'],
            ],
        ];
//        dump($reData);exit;
        return view('',$reData);
    }

    //新增or编辑数据
    function edit(){
        if( $this->request->isPost() ) {
            $param = $this->request->param();
            if(empty($param['id']) && empty($param['password'])){
                $this->error("请输入账户的初始密码！");
            }
            if($param['password']){
                $param['password'] = xn_encrypt($param['password']);
            }

            $result = StaffModel::saveData($param);
            DepartmentModel::RefreshNumber(ADMIN_ID);
            if( $result ) {
                $this->success('操作成功');
            } else {
                $this->error('操作失败');
            }
        }
        $id = $this->request->get('id');
        $copy = $this->request->get('copy');
        $data = [];
        if(!empty($id)){
            $data = StaffModel::where('id',$id)->find();
            if($copy==1){
                unset($data['id']);
            }
            
        }
        $reData = [
            'data'=>$data,
            
            'depart_idList'=>StaffModel::getDepartmentList(),
            'statusList'=>StaffModel::$statusList,
            'work_statusList'=>StaffModel::$work_statusList,
            
        ];
        return view('index_edit',$reData);
    }


    //快捷登陆
    function autoLogin(){
        $id = $this->request->get('id');
        $time = time();
        $ticket = Jwt::getToken([
            'iat'=>$time,
            'exp'=>$time+60,//有效期 1分钟
            'userid'=>$id,
        ]);
        $this->redirect(url('home/login/autoLogin',['ticket'=>$ticket]));
    }

    /**
     * 删除节点
     */
    public function delete()
    {
        $id = $this->request->param('id');
        !($id>1) && $this->error('参数错误');
        StaffModel::where([['id','in',$id]])->delete();
        $this->success('删除成功');
    }

    //数据导出
    public function exportData(){
        $param = $this->request->param();
        $model = new StaffModel();
        $exportData = $model->getExport($param,'员工列表列表');
        if($exportData){
            $this->success('ok','',$exportData);
        }
        $this->error('没有数据被导出！');
    }
}
