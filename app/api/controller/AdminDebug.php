<?php


namespace app\api\controller;


use app\api\services\UserListService;
use app\common\model\ClientModel;

class AdminDebug extends Base
{

    /**
     *
     * http://imservice.hnbjsj.com/api/adminDebug/index?mid=100000,100001
     * @throws \app\common\exception\ApiException
     */
    function index(){
        $uid = $this->request->param('uid')?:'100020';// 加好友的
        $mid = $this->request->param('mid');

        if(empty($mid)){
            //直接获取所有
            $where = "id != {$uid}";
            $userList = ClientModel::findAll($where,'id');
            $id = array_column($userList,'id');
            $mid = join(',',$id);
        }

        if(strpos($mid,',')!==false){
            $midArr = explode(',',$mid);
        }else{
            $midArr = [$mid];
        }

        //循环加好友
        $number = 0;
        foreach ($midArr as $mid){
            $res = UserListService::addFriends($uid,$mid);
            $number++;
        }
        echo "添加好友{$number}人";

    }
}