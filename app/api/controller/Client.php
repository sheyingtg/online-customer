<?php


namespace app\api\controller;


use app\api\services\ClientService;
use app\common\model\ClientModel;

class Client extends Auth
{

    /**
     * 获取用户的基本信息
     */
    function getUserInfo(){
        $params = $this->request->param();
        $params['uid'] = $this->uid;
        $msg_data = ClientService::getUserInfo($params);
        return json(getJson('msg',1,$msg_data));
    }

    /**
     * 获取我的好友申请列表
     */
    function getNewFriends(){
        $params = $this->request->param();
        $params['uid'] = $this->uid;
        $msg_data = ClientService::getNewFriends($params);
        return json(getJson('msg',1,$msg_data));
    }

    /**
     * 通过好友申请
     */
    function passFriends(){
        $params = $this->request->param();
        $params['uid'] = $this->uid;
        $msg_data = ClientService::passFriends($params);
        return json(getJson('msg',1,$msg_data));
    }


    /**
     * 删除好友
     */
    function deleteFriend(){
        $params = $this->request->param();
        $params['uid'] = $this->uid;
        $msg_data = ClientService::deleteFriend($params);
        return json(getJson('msg',1,$msg_data));
    }

    /**
     * 发送好友申请
     */
    function addFrinedMsg(){
        $params = $this->request->param();
        $params['uid'] = $this->uid;
        $friendStatus = ClientService::addFrinedMsg($params);
        $msg = $friendStatus ==3 ?'添加好友成功':'发送好友申请成功';
        return json(getJson($msg,1,['friend_status'=>$friendStatus]));
    }

    /**
     * 查询好友申请状态
     */
    function getNewFriendStatus(){
        $params = $this->request->param();
        $params['uid'] = $this->uid;
        // 1 未审核 0 没有待处理的申请记录
        $auditStatus = ClientService::getNewFriendStatus($params);
        return json(getJson('ok',1,['auditStatus'=>$auditStatus]));
    }
}