<?php


namespace app\api\controller;


use app\api\services\DemoService;

class Demo extends Base
{
    /**
     * 自动生成 showdoc API文档
     * https://www.showdoc.com.cn/page/741656402509783
     */

    /**
     * curd功能演示列表 showdoc
     * @catalog 自动文档/curd功能演示
     * @title curd功能演示 列表
     * @description curd功能演示列表的接口
     * @method get
     * @url 接口域名/api/Demo/lists
     * @param page 可选 string 页码
     * @return {"code":1,"msg":"ok","data":{"total":"总条数","per_page":"每页记录数","current_page":"当前页面","last_page":"总页码","data":[{"id":"ID","img":"图片","title":"标题","status":"状态","display_order":"排序","create_time":"创建时间","update_time":"更新时间"}]}}
     * @return_param id int ID
     * @return_param img string 图片
     * @return_param title string 标题
     * @return_param status int 状态
     * @return_param display_order int 排序
     * @return_param create_time int 创建时间
     * @return_param update_time int 更新时间
     * @remark 这里是备注信息
     * @number 1
     */
    function ceshi(){
        $a=1;
        echo $a;
        return $a;
    }

    /**
     * curd功能演示列表 showdoc
     * @catalog 自动文档/curd功能演示
     * @title curd功能演示 列表
     * @description curd功能演示列表的接口
     * @method get
     * @url 接口域名/api/Demo/lists
     * @param page 可选 string 页码
     * @return {"code":1,"msg":"ok","data":{"total":"总条数","per_page":"每页记录数","current_page":"当前页面","last_page":"总页码","data":[{"id":"ID","img":"图片","title":"标题","status":"状态","display_order":"排序","create_time":"创建时间","update_time":"更新时间"}]}}
     * @return_param id int ID
     * @return_param img string 图片
     * @return_param title string 标题
     * @return_param status int 状态
     * @return_param display_order int 排序
     * @return_param create_time int 创建时间
     * @return_param update_time int 更新时间
     * @remark 这里是备注信息
     * @number 1
     */
    function lists(){
        $params = $this->request->get();
        $data = DemoService::getList($params);
        return json(getJson('ok',1,$data));
    }

    /**
     * curd功能演示详情 showdoc
     * @catalog 自动文档/curd功能演示
     * @title curd功能演示 详情
     * @description curd功能演示详情的接口
     * @method get
     * @url 接口域名/api/Demo/detail
     * @param id 必选 string 记录ID
     * @return {"code":1,"msg":"ok","data":{"id":"ID","img":"图片","title":"标题","status":"状态","display_order":"排序","create_time":"创建时间","update_time":"更新时间"}}
     * @return_param id int ID
     * @return_param img string 图片
     * @return_param title string 标题
     * @return_param status int 状态
     * @return_param display_order int 排序
     * @return_param create_time int 创建时间
     * @return_param update_time int 更新时间
     * @remark 这里是备注信息
     * @number 2
     */
    function detail(){
        $params = $this->request->get();
//        $params['uid'] = $this->uid;//获取用户自身数据
        $this->validate($params);//数据效验
        if($data = DemoService::getDetail($params)){
            return json(getJson('ok',1,$data));
        }
        return json(getJson('记录不存在',0,$params));
    }

    /**
     * curd功能演示新增数据 showdoc
     * @catalog 自动文档/curd功能演示
     * @title curd功能演示 新增
     * @description curd功能演示新增的接口
     * @method post
     * @url 接口域名/api/Demo/add
         * @param id 必选 int ID 
     * @param img 必选 string 图片 
     * @param title 必选 string 标题 
     * @param status 必选 int 状态 
     * @param display_order 必选 int 排序 
     * @param create_time 必选 int 创建时间 
     * @param update_time 必选 int 更新时间 
     * @return {"code":1,"msg":"新增成功"}
     * @remark 这里是备注信息
     * @number 3
     */
    function add(){
        $params = $this->request->post();
        $this->validate($params);//数据效验
//        $params['uid'] = $this->uid;//获取用户自身数据
        if($data = DemoService::add($params)){
            return json(getJson('新增成功',1));
        }else{
            return json(getJson('新增失败'));
        }
    }

    /**
     * curd功能演示更新 showdoc
     * @catalog 自动文档/curd功能演示
     * @title curd功能演示 更新
     * @description curd功能演示更新的接口
     * @method post
     * @url 接口域名/api/Demo/update
     * @param id 必选 string 记录ID
     * @return {"code":1,"msg":"更新成功"}
     * @remark 这里是备注信息
     * @number 4
     */
    function update(){
        $params = $this->request->post();
        $this->validate($params);//数据效验
//        $params['uid'] = $this->uid;//获取用户自身数据
        if($data = DemoService::update($params)){
            return json(getJson('更新成功',1));
        }
        return json(getJson('更新失败'));
    }

    /**
     * curd功能演示删除 showdoc
     * @catalog 自动文档/curd功能演示
     * @title curd功能演示 删除
     * @description curd功能演示删除的接口
     * @method post
     * @url 接口域名/api/Demo/delete
     * @param id 必选 string 记录ID
     * @return {"code":1,"msg":"删除成功"}
     * @remark 这里是备注信息
     * @number 5
     */
    function delete(){
        $params = $this->request->post();
        $this->validate($params);//数据效验
//        $params['uid'] = $this->uid;//获取用户自身数据
        if($data = DemoService::delete($params)){
            return json(getJson('删除成功',1));
        }
        return json(getJson('删除失败'));
    }



}