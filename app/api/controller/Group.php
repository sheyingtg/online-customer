<?php


namespace app\api\controller;

//群管理

use app\api\services\GroupService;


class Group extends Auth
{
    /**
     *  传递群组id 读取群组相关信息
     */
    function GetGroupInfo(){
        $params = $this->request->param();
        $params['uid'] = $this->uid;
        $data_msg = GroupService::GetGroupInfo($params);
        success('ok',$data_msg);
    }

    /**
     * 创建/加入 群聊
     */
    function CreateGroup(){
        $params = $this->request->param();
        $params['uid'] = $this->uid;
        $data_msg = GroupService::CreateGroup($params);
        success('ok',$data_msg);
    }

    /**
     * 删除群组某些成员
     */
    function delGroupUser(){
        $params = $this->request->param();
        $params['uid'] = $this->uid;
        $data_msg = GroupService::delGroupUser($params);
        success('ok',$data_msg);
    }

    /**
     * 退出群聊
     */
    function logoutGroup(){
        $params = $this->request->param();
        $params['uid'] = $this->uid;
        $data_msg = GroupService::logoutGroup($params);
        success('退出成功',$data_msg);
    }
}