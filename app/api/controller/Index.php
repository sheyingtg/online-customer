<?php


namespace app\api\controller;


use app\api\services\IndexService;
use app\api\services\StaffService;
use app\api\services\UserListService;
use app\common\model\ClientModel;

class Index extends Auth
{
    function Index(){
        $param = $this->request->param();
        //参数效验
        $this->validate($param);
        return json(['code'=>1,'msg'=>'请求成功','data'=>$param]);
    }

    /**
     * 获取会话列表
     */
    function getDialogueList(){
        $params = $this->request->param();
        $params['uid'] = $this->uid;
        $data = IndexService::getDialogueList($params);
        return json(getJson('msg',1,$data));
    }


    /**
     * 获取好友列表
     */
    function getFriendList(){
        $friend = IndexService::getFriendList($this->uid,1);

        // 追加客服列表
        $kefu = StaffService::getList($this->admin_id,true);

        $data = [
            'friend'=>$friend,
            'kefu'=>$kefu,
            'newFriendNumber'=>UserListService::getNewFriendNumber($this->uid),
        ];
        return json(getJson('msg',1,$data));
    }

    /**
     * 获取自己的基本信息
     */
    function getMyInfo(){
        $data = ClientModel::findOne(['id'=>$this->uid]);
        return json(getJson('msg',1,$data));
    }

}