<?php


namespace app\api\controller;



use app\api\services\LoginService;

class Login extends Base
{

    /**
     * 用户发起登录
     * 登录成功 返回ticket
     */
    function index(){
        $params = $this->request->param();
        $params['admin_id'] = $params['admin_id']?:$this->admin_id;
        if($reData = LoginService::login($params)){
            success('登录成功',$reData);
        }
        error('登录失败');
    }

    /**
     * 用户注册
     */
    function register(){
        $params = $this->request->param();

        //校验注册数据
        $this->validate($params);
        $params['admin_id'] = $params['admin_id']?:$this->admin_id;
        if($reData = LoginService::register($params)){
            success('登录成功',$reData);
        }
        error('登录失败');
    }


    /**
     * 快捷自动登录
     */
    function autoLogin(){
        $params = $this->request->param();
        $this->validate($params);
        if($reData = LoginService::autoLogin($params)){
            success('登录成功',$reData);
        }
        error('登录失败');
    }


}