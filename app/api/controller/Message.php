<?php


namespace app\api\controller;


use app\api\services\MessageService;
use app\api\services\UserListService;

class Message extends Auth
{

    /**
     * 获取聊天记录
     */
    function index(){
        $params = $this->request->param();
        $params['uid'] = $this->uid;
        $data_msg = MessageService::getMessageList($params);
        success('ok',$data_msg);
    }

    /**
     * 给某人发送消息
     */
    function sendMessage(){
        $params = $this->request->param();
        $params['uid'] = $this->uid;
        $data_msg = MessageService::sendMessage($params,$params['type']?:1);
        success('ok',$data_msg);
    }


    /**
     * 设置置顶 or 消息免打扰等
     */
    function SetUp(){
        $params = $this->request->param();
        $params['uid'] = $this->uid;
        $data_msg = MessageService::SetUp($params);
        success('ok',$data_msg);
    }

    /**
     * 设置消息为已读
     */
    function setRead(){
        $params = $this->request->param();
        $params['uid'] = $this->uid;
        $data_msg = UserListService::setRead($params);
        success('ok',$data_msg);
    }

}