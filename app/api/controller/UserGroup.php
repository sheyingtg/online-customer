<?php


namespace app\api\controller;


use app\api\services\MessageService;
use app\api\services\UserGroupService;

class UserGroup extends Auth
{
    /**
     * 修改群聊名称
     */
    function upName(){
        $params = $this->request->param();
        $params['uid'] = $this->uid;
        $data_msg = UserGroupService::upName($params);
        success('ok',$data_msg);
    }



}