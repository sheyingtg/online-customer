<?php


namespace app\api\controller;


use app\api\services\UserListService;

class UserList extends Auth
{
    /**
     * 修改用户昵称
     */
    function upUserAlias(){
        $params = $this->request->param();
        $params['uid'] = $this->uid;
        $data_msg = UserListService::upUserAlias($params);
        success('ok',$data_msg);
    }
}