<?php


namespace app\api\services;


use app\admin\controller\NewFriends;
use app\common\exception\ApiException;
use app\common\model\ClientModel;
use app\common\model\NewFriendsModel;
use app\common\model\UserListModel;
use app\common\services\BaseService;

class ClientService extends BaseService
{
    /**
     * 获取用户的基本信息
     */
    static function getUserInfo($params){
        /**
         * 用户信息
         */
        if($params['mid']){
            $where = ['id'=>$params['mid']];
        }else if($params['key']){
            $where = ['id|mobile'=>$params['key']];
        }

        $info = ClientModel::findOne($where);
        if(empty($info)){
            return $info;
        }
        //判断2人的好友状态
        $info['friend_status'] = self::getStatus($params['uid'],$params['mid']);

        //我发起了好友申请，对方还未审核
        if($info['friend_status']==1){
            $info['new_friend'] = NewFriendsModel::findOne(['uid'=>$params['uid'],'mid'=>$params['mid']]);
        }

        // 如果对方是客服 那么增加客服标识
        StaffService::replaceInfo($info);


        return $info;
    }

    /**
     * 获取用户的好友申请状态
     */
    static function getNewFriends($params){
        $uid = $params['uid'];
        $where = [
            'uid|mid'=>$uid,
        ];
        //只显示最近30条
        $model = new NewFriendsModel();
        $list = $model->with(['getUid','getMid'])->where($where)->limit(30)->select();

        return $list;
    }

    /**
     * 通过好友申请
     */
    static function passFriends($params){
        $uid = $params['uid'];
        $id = $params['id'];
        $status = $params['status']?:1;// 1 通过 2 拒绝 默认通过
        $msg = $params['msg']?:'';// 拒绝时需要填写理由

        $info = NewFriendsModel::findOne(['id'=>$id]);
        if(empty($info)){
            throw new ApiException("没有找到该申请记录");
        }
        if($info['mid']!=$uid){
            throw new ApiException("您无权操作哦~");
        }

        $updata = ['status'=>$status,'msg'=>$msg];
        NewFriendsModel::updates(['id'=>$id],$updata);

        if($status==1){
            //如果是通过 则需要 互加好友  我通过对方 所以
            UserListService::addFriends($uid,$info['uid']);
            //我通过了对方的好友请求  对方自动发一条消息给我
//            [uid,mid,msg]
            $params = [
                'uid'=>$info['uid'],
                'mid'=>$uid,
                'msg'=>$info['desc']?:'',
            ];
            MessageService::sendMessage($params,1);

            // 发送系统消息
//            ClientModel
            $uid_nickname = ClientModel::findValue(['id'=>$info['uid']],'nickname');
            $params = [
                'uid'=>$info['uid'],
                'mid'=>$uid,
                'msg'=>"已添加了{$uid_nickname}，现在可以聊天了",
            ];
            MessageService::sendMessage($params,5);
        }
    }

    /**
     * 删除好友  将对方从自己的好友列表删除
     */
    static function deleteFriend($params){
        $uid = $params['uid'];
        $mid = $params['mid'];

        $res = UserListModel::findOne(['uid'=>$uid,'mid'=>$mid],'id');
        if(!$res){
            throw new ApiException("对方不是您的好友，无需删除");
        }

        //软删除
        return UserListModel::destroy($res['id']);
    }

    /**
     * 添加好友
     * @param $params
     */
    static function addFrinedMsg($params){
        $uid = $params['uid'];
        $mid = $params['mid'];

        //查询软删除的数据
        $where = [
            'uid'=>$uid,
            'mid'=>$mid,
        ];

        // 查询是否有申请记录
        $newFriend = NewFriendsModel::findOne($where);
        if($newFriend && $newFriend['status']==0){
            throw new ApiException("您的好友申请已发送，请耐心等候~");
        }

        $friendStatus = ClientService::getStatus($uid,$mid);
        if($friendStatus==3){
            throw new ApiException("对方已经是您的好友了，无需申请");
        }

        //我单方面删除对方  所以我添加对方即可
        if($friendStatus==2){
            UserListService::addFriends($uid,$mid);
            // 寻找是否有对方添加我为好友 且未审核的记录
            NewFriendsModel::updates(['uid'=>$mid,'mid'=>$uid,'status'=>0],['status'=>1]);

            $friendStatus = 3;
        }else{
            //判断是否是客服  增加了 自动通过好友的字段  auto_pass = 1 通过
            $mid_auto_pass = ClientModel::findValue(['id'=>$mid],'auto_pass')?:0;
            if($mid_auto_pass){
                //开启了直接通过好友申请 直接添加为好友
                UserListService::addFriends($uid,$mid);
                $friendStatus = 3;

                // 你们已经是好友了，现在可以聊天了
//                $uid_nickname = ClientModel::findValue(['id'=>$mid],'nickname');
                $params = [
                    'uid'=>$mid,
                    'mid'=>$uid,
                    'msg'=>"你们已经是好友了，现在可以聊天了",
                ];
                MessageService::sendMessage($params,5);
            } else {
                NewFriendsModel::create([
                    'uid'=>$uid,
                    'mid'=>$mid,
                    'desc'=>$params['desc'],
                    'status'=>0,
                    'from'=>$params['from'],
                ]);

                //获取申请数量 并通知给用户
                $number = NewFriendsModel::findCount(['mid'=>$mid,'status'=>0]);
                GatewayService::upNewFriendNumber($mid,$number);
            }
        }
        return $friendStatus;
    }

    /**
     * 判断好友状态
     * 好友状态 0  陌生人 1 对方未加我  2 我未加对方  3 互为好友
     * @param $uid
     * @param $mid
     */
    static function getStatus($uid,$mid){
        if($uid == $mid) return 3;//傻屌自己肯定是自己的好友啊
        $status = 0; // 好友状态
        // 判断是否是好友
        if(UserListModel::findOne(['uid'=>$uid,'mid'=>$mid],'id')){
            $status += 1;
        }
        if(UserListModel::findOne(['mid'=>$uid,'uid'=>$mid],'id')){
            $status += 2;
        }
        return $status;
    }

    /**
     * 查询好友申请状态
     */
    static function getNewFriendStatus($params){
        $uid = $params['uid'];
        $mid = $params['mid'];
        return  NewFriendsModel::findCount(['uid'=>$uid,'mid'=>$mid,'status'=>0])?1:0;
    }
}