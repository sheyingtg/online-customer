<?php


namespace app\api\services;


use app\common\exception\ApiException;
use app\common\model\DemoModel;
use app\common\services\BaseService;

class DemoService extends BaseService
{

    /**
     * 获取列表数据
     */
    static function getList2($params){
        $model = new DemoModel();
        $model = $model->order('id desc');
        //设置查询条件
        self::getListWhere($model,$params);
        //分页查询
//        $list = $model->paginate($params['pageSize']);
        //直接查询
        $list = $model->select();

        foreach ($list as $k=>$v){
            $v->create_time_text.='';
            $v->update_time_text.='';
        }

        return $list;
    }

    /**
     * 获取列表数据
     */
    static function getList($params){
        $model = new DemoModel();
        $model = $model->order('id desc');
        //设置查询条件
        self::getListWhere($model,$params);
        //分页查询
//        $list = $model->paginate($params['pageSize']);
        //直接查询
        $list = $model->select();

        foreach ($list as $k=>$v){
                $v->create_time_text.='';
                $v->update_time_text.='';
            }

        return $list;
    }

    /**
     * 获取数据详情
     * @param $params
     */
    static function getDetail($params){
        $model = new DemoModel();
        //设置查询条件
        self::getListWhere($model,$params);
        $data = $model->find();

                $data->create_time_text.='';
                $data->update_time_text.='';


        return $data;
    }

    /**
     * 删除数据
     * @param $params
     */
    static function delete($params){
        $model = new DemoModel();
        self::getListWhere($model,$params);
        $data = $model->find();
        if(empty($data)){
            throw new ApiException("记录不存在！");
        }
        $res = $model->delete();

        return $res;
    }

    /**
     * 更新数据
     * @param $params
     */
    static function update($params){
        $where = [
            'id'=>$params['id'],
        ];
        $bannerId = DemoModel::findOne($where,'id');
        if(empty($bannerId)){
            throw new ApiException("记录不存在！");
        }
        $res = DemoModel::where($where)->update($params);

        return $res;
    }

    /**
     * 新增数据记录
     * @param $params
     */
    static function add($params){
        $model = new DemoModel();

        return $model->save($params);
    }

    /**
     * 列表查询条件处理
     * @param DemoModel $model
     * @param array $param
     */
    static private function getListWhere(&$model,$params=[]){
        if(empty($params)){
            return $model;
        }
        $where = [];
        if($params['id']){
            $where['id'] = $params['id'];
        }
        if($where){
            $model = $model->where($where);
        }
    }

}