<?php


namespace app\api\services;


use app\common\exception\ApiException;
use app\common\model\ClientModel;
use app\common\services\BaseService;
use GatewayWorker\Lib\Gateway;


class GatewayService extends BaseService
{

    /**
     * 给某人发送私聊消息
     * @param $mid
     * @param $msg
     */
    static function toMessage($msgData){
        //接收消息的人
        $mid = $msgData['to_user_id'];//接收人
        $uid = $msgData['from_user_id'];//发送人
        $is_group = $msgData['is_group'];// 1 群聊  0 私聊
        $type = $msgData['type']; // 1 文字 2 图片  3 系统消息  4 仅自身可见
        $msg = $msgData['msg'];


        //组件消息结构体
        $data = [
            'msgType'=>'msg',//判断区分消息的类型
            'data'=>$msgData,//实际的消息结构
        ];
        $strMsg = json_encode($data,256);

        try {
            if($is_group){
                //群推送
                $group = "group_list:{$mid}";
                //发送群组消息时  不应该给当前客户端发送
                Gateway::sendToGroup($group, $strMsg);
            }else{
                //私聊推送
                // 如果对方在线
                if(Gateway::isUidOnline($mid)){
                    //进行消息推送
                    Gateway::sendToUid($mid,$strMsg);
//                    Gateway::sendToUid($uid,json_encode($data,256));
                }
            }
        }catch (\Exception $e){
            $msg = iconv("UTF-8", "GB2312//IGNORE", $e->getMessage());
            throw new ApiException("socket推送失败：{$msg}");
        }
        return true;
    }


    /**
     * 判断某人是否在线
     * @param $mid
     */
    static function online($mid){
        $res = Gateway::isUidOnline($mid);
        return $res;
    }

    /**
     * 在大厅中发送消息(在线 离线消息)
     * @param $userInfo  发送人的个人信息
     * @param $status  在线 离线状态 1 在线 0 离线
     */
    static function onlineState($userInfo,$status=1){
        $uid = $userInfo['id'];
        echo "发送大厅消息{$uid}__{$status}";
        // 大厅key
        $sala_list_key = "sala_list:{$userInfo['admin_id']}";
        if($status==0){
            //如果是发送离线消息 则判断当前我的客户端是否全部离线
            if(Gateway::isUidOnline($uid)){
                return true;
            }
        }
        //组件消息结构体
        $data = [
            'msgType'=>'upUserStatus',
            'data'=>[
                'uid'=>$uid,
                'status'=>$status
            ],
        ];
        $strMsg = json_encode($data,256);
        Gateway::sendToGroup($sala_list_key, $strMsg);

        // 更新数据库 在线离线状态
        ClientModel::updates(['id'=>$uid],['online'=>$status]);
    }

    /**
     * 通知群组中的所有人 更新群昵称
     * @param $groupId
     * @param $newName
     */
    static function upGroupNameNotification($groupId,$newName){
        //组件消息结构体
        $data = [
            'msgType'=>'upGroupName',//判断区分消息的类型
            'data'=>[
                'groupId'=>$groupId,
                'newName'=>$newName,
            ],//实际的消息结构
        ];
        $strMsg = json_encode($data,256);
        //群推送
        $group = "group_list:{$groupId}";
        //发送群组消息时  不应该给当前客户端发送
        Gateway::sendToGroup($group, $strMsg);
    }

    /**
     * 通知某人 有新的好友申请了
     * @param $mid
     * @param $number
     */
    static function upNewFriendNumber($mid,$number){
        //组件消息结构体
        $data = [
            'msgType'=>'upNewFriendNumber',//判断区分消息的类型
            'data'=>[
                'number'=>$number,
            ],//实际的消息结构
        ];
        $strMsg = json_encode($data,256);
        //群推送
        if(Gateway::isUidOnline($mid)){
            //进行消息推送
            Gateway::sendToUid($mid,$strMsg);
        }
    }

    /**
     * 通知某人 有多少条未读消息
     * @param $mid
     * @param $number
     */
    static function toMsgRedaNumber($mid,$number){
        //组件消息结构体
        $data = [
            'msgType'=>'toMsgRedaNumber',//判断区分消息的类型
            'data'=>[
                'number'=>$number,
            ],//实际的消息结构
        ];
        $strMsg = json_encode($data,256);
        if(Gateway::isUidOnline($mid)){
            //进行消息推送
            Gateway::sendToUid($mid,$strMsg);
        }
    }

    /**
     * 将某人加入到群组
     * @param $uid
     * @param $groupId
     */
    static function addGroupUser($uid,$groupId){
        //获取所有的连接
        $client_list = Gateway::getClientIdByUid($uid);
        $group = "group_list:{$groupId}";
        foreach ($client_list as $client_id){
            Gateway::joinGroup($client_id, $group);
        }
    }

    /**
     * 将某人从某个组中移除
     */
    static function delGroupUser($uid,$groupId){
        //获取所有的连接
        $client_list = Gateway::getClientIdByUid($uid);
        $group = "group_list:{$groupId}";
        foreach ($client_list as $client_id){
            Gateway::leaveGroup($client_id, $group);
        }
    }

    /**
     * 解散某个群
     */
    static function delGroup($groupId){
        $group = "group_list:{$groupId}";
        Gateway::ungroup($group);
    }

    /**
     * 使用当前连接 获取对应的信息
     * @param $client_id
     * @param $status
     */
    static function onlineStateClient($client_id,$status){
        $uid = $_SESSION[$client_id];
        if(!$uid){
            return false;
        }
        unset($_SESSION[$client_id]);//清理该变量
        $userInfo = ClientModel::findOne(['id'=>$uid],"id,admin_id");
        self::onlineState($userInfo,$status);
    }
}