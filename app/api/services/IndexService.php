<?php


namespace app\api\services;


use app\common\model\MessageLogModel;
use app\common\model\UserGroupModel;
use app\common\model\UserListModel;
use app\common\services\BaseService;
use utils\FirstSpell;

class IndexService extends BaseService
{

    /**
     * 获取对话列表
     */
    static function getDialogueList($params){
        $uid = $params['uid'];
        $model = new UserListModel();
        $where = [
            'uid'=>$uid,
            'is_show'=>1,//会话列表 不显示隐藏的会话记录
        ];
        if($params['mid']){
            $where['mid'] = $params['mid'];
            unset($where['is_show']);//显示隐藏 都会被查询
        }
        $field = '*';
        //查询会话列表
        $list = $model->where($where)->field($field)->order("is_top desc")->select();
//        dump($where);exit;
//        dump($list);exit;
        //处理 好友会话  和 群组会话的数据
        foreach ($list as $k=>$v){
            if($v->type==1){
                //好友会话
                StaffService::replaceInfo($v->getClientMid);//获取好友信息
                //在线状态
                $v['online'] = GatewayService::online($v['mid']);
                // 好友状态
                $v['friend_status'] = ClientService::getStatus($uid,$v['mid']);
            }else{
                //群组会话
                $userGroup = $v->getUserGroup;
                //获取群里面的9个用户 作为群组头像
                $userGroup['user_list'] = UserGroupModel::getUserListAll($userGroup->id,9);
            }
            //获取会话中的 最后一条记录信息
            $is_group = MessageService::isGroup($v['mid']);
            if($is_group){
                //群组 直接获取接收对象是群组的最后一条记录
                $where = "is_group = 1 and type in (1,2) and to_user_id = {$v['mid']}";
            }else{
                $where = "is_group = 0 and type in (1,2) and ((from_user_id = {$v['uid']} and to_user_id = {$v['mid']}) or (from_user_id = {$v['mid']} and to_user_id = {$v['uid']}))";
            }
            $field = "id,type,from_user_id,from_user_nickname,msg,create_time,datetime";
            $v['msg_list'] = MessageLogModel::findOne($where,$field,'id desc');
            //未读消息数
//            $v['msg_reda_number'] = MessageLogModel::findCount([
//                'from_user_id'=>$v['mid'],
//                'to_user_id'=>$v['uid'],
//                'is_reda'=>1,
//            ]);
        }
        return $list;
    }

    /**
     * 获取用户的好友列表
     * @param $uid
     */
    static function getFriendList($uid,$type=1){
        $model = new UserListModel();
        $where = [
            'uid'=>$uid,
//            'type'=>$type,
        ];
        $field = '*';
        $list = $model->with(['getClientMid'])->where($where)->field($field)->select();

        foreach ($list as $k=>$v){
            if($v->type!=1){
                $userGroup = $v->getUserGroup;
                // 获取群头像
                $userGroup['user_list'] = UserGroupModel::getUserListAll($userGroup->id,9);
//                continue;
            }else{
                // 如果是客服号  则先替换客服信息给个人
                StaffService::replaceInfo($v->getClientMid);

                $nickname = $v->remark?:$v->getClientMid->nickname;
                //计算首拼
                $v['initial'] = FirstSpell::get($nickname)[0];
                // 好友状态
                $v->getClientMid['friend_status'] = ClientService::getStatus($uid,$v['mid']);
            }
        }

//        foreach ($list as &$v){
//            if($v->type==1){
////                dump($v);
//            }
//        }
//        exit;


        return $list;

    }
}