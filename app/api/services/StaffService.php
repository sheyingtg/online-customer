<?php


namespace app\api\services;


use app\common\model\StaffModel;
use app\common\services\BaseService;

class StaffService extends BaseService
{
    /**
     * 获取公司的客服列表
     * @param $admin_id 平台id
     * @param bool $isOnline 是否在线
     */
    static function getList($admin_id){
        $model = new StaffModel();

        //必须是有绑定客服id的
        $hasWhere = [['id','>',0]];
        $list = StaffModel::hasWhere('getClient',$hasWhere)
            ->where(['StaffModel.admin_id'=>$admin_id])
            ->order('priority desc')
            ->select();
        foreach ($list as $v){
            $v->getClient;
            $v['online'] = $v->getClient->online;
        }
        if($list){
            //在线的显示在前面
            $list = $list->toArray();
            foreach ($list as &$v){
                StaffService::replaceInfo2($v);
            }
            $list = arraySort($list,"online",SORT_DESC);
        }
        return $list;
    }

    /**
     * 判断是否是客服
     */
    static function has($client_id){
        return  StaffModel::findCount(['client_id'=>$client_id]) ? : 0;
    }

    /**
     * 传递一个用户的信息进来 如果是客服 则替换他的基本信息
     * @param $info
     */
    static function replaceInfo(&$info){
        if($info['role']==1){
            //是客服， 替换掉头像，昵称，手机号
            $field = "name,mobile,head";
            $kefu = StaffModel::findOne(['client_id'=>$info['id']],$field);
            if($kefu){
                $info['name'] = $kefu['name'];
                $info['nickname'] = $kefu['name'];
                $info['mobile'] = $kefu['mobile'];
                $info['headimg'] = $kefu['head'];
            }
        }
    }


    /**
     * 传递一个用户的信息进来 如果是客服 则替换他的基本信息
     * @param $info
     */
    static function replaceInfo2(&$info){
        $info['getClient']['name'] = $info['name'];
        $info['getClient']['nickname'] = $info['name'];
        $info['getClient']['mobile'] = $info['mobile'];
        $info['getClient']['headimg'] = $info['head'];
    }
}