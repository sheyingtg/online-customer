<?php


namespace app\api\services;


use app\common\exception\ApiException;
use app\common\model\ClientModel;
use app\common\model\UserGroupModel;
use app\common\model\UserListModel;
use app\common\services\BaseService;

class UserGroupService extends BaseService
{
    /**
     * 修改群聊名称
     */
    static function upName($params){
        $groupId = $params['groupId'];
        $name = $params['name'];
        $uid = $params['uid'];

        //检查自己是否在群聊中
        $res = UserListModel::findOne(['uid'=>$uid,'mid'=>$groupId,'type'=>2],'id');
        if(empty($res)){
            throw new ApiException('您不在该群中');
        }
        //如果是群聊成员大于10  则 群成员不允许修改群名称
        $userGroup = UserGroupModel::findOne(['id'=>$groupId]);
        if($userGroup['member_number']>10 && $userGroup['admin_uid']!=$uid){
            throw new ApiException('该群成员人数过多，只有群主可以修改群名称');
        }

        $res = UserGroupModel::updates(['id'=>$groupId],['name'=>$name]);

        $nickname = ClientModel::findValue(['id'=>$uid],'nickname');
        // 修改昵称后 增加群消息推送 uid,mid,msg,is_group
        MessageService::sendMessage([
            'uid'=>$uid,
            'mid'=>$groupId,
            'msg'=>'修改群名为“'.$name.'”',
        ],3);

        //给群里面的所有人 推送更新群名的消息
        GatewayService::upGroupNameNotification($groupId,$name);


        return $res;
    }
}