<?php


namespace app\api\services;


use app\common\exception\ApiException;
use app\common\model\ClientModel;
use app\common\model\MessageLogModel;
use app\common\model\NewFriendsModel;
use app\common\model\UserListModel;
use app\common\services\BaseService;

class UserListService extends BaseService
{

    /**
     * 设置用户昵称
     */
    static function upUserAlias($params){
        $uid = $params['uid'];
        $mid = $params['mid'];
        $alias = $params['alias'];

        $res = UserListModel::updates(['uid'=>$uid,'mid'=>$mid,'type'=>1],[
            'remark'=>$alias
        ]);
        return $res;
    }

    /**
     * 发消息给mid  mid 增加未读消息数量
     * @param $uid
     * @param $mid
     */
    static function addRedaNumber($uid,$mid){
        if(MessageService::isGroup($mid)){
            //群聊  给群聊中 除了自己以外的所有人增加未读消息数量
            $where = "mid = {$mid} and uid != {$uid}";
            UserListModel::findInc($where,'msg_reda_number');

            UserListModel::updates(['type'=>2,'mid'=>$mid,'is_show'=>0],['is_show'=>1]);
        }else{
            UserListModel::findInc(['uid'=>$mid,'mid'=>$uid],'msg_reda_number');

            //在会话列表显示
            UserListModel::updates(['uid'=>$mid,'mid'=>$uid,'is_show'=>0],['is_show'=>1]);
            UserListModel::updates(['uid'=>$uid,'mid'=>$mid,'is_show'=>0],['is_show'=>1]);
        }
        return true;
    }

    /**
     * 设置消息已阅读
     */
    static function setRead($params){
        $uid = $params['uid'];
        $mid = $params['mid'];
        // 设置未读数量 0
        UserListModel::updates(['uid'=>$uid,'mid'=>$mid],['msg_reda_number'=>0]);
        // 对方发给我的消息更全部改为已读
        if(!MessageService::isGroup($mid)){
            MessageLogModel::updates(['from_user_id'=>$uid,'to_user_id'=>$mid],['is_reda'=>2]);
        }
    }



    /**
     * 互加好友 新朋友
     */
    static function addFriends($uid,$mid){
        $u1 = ClientModel::findOne(['id'=>$uid],'id');
        $u2 = ClientModel::findOne(['id'=>$mid],'id');
        if(!$u1) throw new ApiException("申请人不存在");
        if(!$u2) throw new ApiException("添加人不存在");

        //好友状态
        $friendStatus = ClientService::getStatus($uid,$mid);
        if($friendStatus==3){
            return true;
        }else if($friendStatus == 2){
            // 对方加了我  我没有添加对方
            self::_addFrineds($uid,$mid);
        }else if($friendStatus ==1){
            // 我加了对方  对方没有添加我
            self::_addFrineds($mid,$uid);
        }else if(!$friendStatus){
            //陌生人
            self::_addFrineds($mid,$uid);
            self::_addFrineds($uid,$mid);
        }

        // mid 通过了 uid的好友请求
        return true;
    }

    /**
     * 实际的添加好友
     * @param $uid
     * @param $mid
     */
    static function _addFrineds($uid,$mid){
        $data = [
            'uid'=>$uid,
            'mid'=>$mid,
            'type'=>1,
            'online'=>0,
            'status'=>1,
            'is_show'=>1,
            'remark'=>'',//备注
            'quiet'=>0,
            'is_top'=>0,
            'msg_reda_number'=>0
        ];
        //如果是有软删除 直接恢复
        $info = UserListModel::onlyTrashed()->where([
            'uid'=>$uid,
            'mid'=>$mid,
        ])->find();
        if($info){
            $info->restore();//恢复软删除
        }else{
            UserListModel::create($data);
        }
    }


    /**
     * 获取我的待处理的好友申请
     */
    static function getNewFriendNumber($uid){
        return NewFriendsModel::findCount(['mid'=>$uid,'status'=>0])?:0;
    }

}