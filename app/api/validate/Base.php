<?php


namespace app\api\validate;

use think\Validate;
class Base extends Validate
{
    protected $rule = [
        'id|ID' => 'require',
    ];

    protected $message = [
        'id.require'=>'ID不能为空',
    ];

    //验证场景
    protected $scene = [
        'demo'  =>  ['id'],
    ];
}