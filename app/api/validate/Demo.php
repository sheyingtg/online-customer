<?php


namespace app\api\validate;


class Demo extends Base
{
    protected $rule = [
        'id|ID' => 'require|int',
'img|图片' => 'require|url',
'title|标题' => 'require',
'status|状态' => 'require|int',
'display_order|排序' => 'require|int',

    ];

    protected $message = [
    ];

    //验证场景
    protected $scene = [
        //列表
        'lists'=>[],
        //查看详情
        'detail'=>['id'],
        //更新
        'update'=>['id'],
        //删除
        'delete'=>['id'],
        //新增
        'add'=>['id','img','title','status','display_order']
    ];
}