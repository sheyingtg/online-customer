<?php


namespace app\api\validate;


class Index extends Base
{
    protected $rule = [
        'id|ID' => 'require',
    ];

    protected $message = [
        'id.require'=>'ID不能为空',
    ];

    //验证场景
    protected $scene = [
        'Index'  =>  ['id'],
    ];
}