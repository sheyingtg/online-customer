<?php


namespace app\api\validate;


class Login extends Base
{
    protected $rule = [
        'id|ID' => 'require',
        'uid|客户端id' => 'require',
        'nickname|昵称' => 'require',
        'mobile|手机号' => 'require',
        'admin_id|平台id' => 'require',
        'sign|签名' => 'require',
//        'nickname|昵称' => 'require',
    ];

    protected $message = [
        'id.require'=>'ID不能为空',
    ];

    //验证场景
    protected $scene = [
        // 注册
        'register'  =>  ['uid','nickname','sign','admin_id'],
        'autoLogin' =>  ['admin_id','sign','uid'],
    ];
}