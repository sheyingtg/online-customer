<?php

namespace app\common\Notices;

use app\common\components\AlipayTrade;
use app\common\exception\ApiException;
use app\common\services\admin\PayService;
use think\facade\Log;

/**
* 支付宝异步通知处理方法
**/

class AlipayNotice{
    /**
     * 验证异步回调参数
     */
    private function NotifyProcess()
    {
        $params = $_POST;//post参数
        Log::debug("服务器通知参数:" . json_encode($params,320));
        //进行签名验证
        $flag = AlipayTrade::checkSign($params);
        if($flag){
            // TODO 验签成功后
            //按照支付结果异步通知中的描述，对支付结果中的业务内容进行1\2\3\4二次校验，校验成功后在response中返回success，校验失败返回failure
            if($params['trade_status'] == 'TRADE_FINISHED' || $params['trade_status'] == 'TRADE_SUCCESS') {//交易成功
                //处理平台订单支付状态同步
                try {
                    PayService::sysTransactionNotify($params,$params["total_amount"],$params['trade_no'],strtotime($params['gmt_payment']));
                }catch (ApiException $e){
                    Log::debug('业务处理失败：'.$e->getMessage());//获取订单状态更新失败原因
                    return false;
                }
                echo "success";    //请不要修改或删除
                Log::debug("通知返回结果:success");
            }
        }else{
            // TODO 验签失败则记录异常日志，并在response中返回failure.
            //验证失败
            echo "failure";
            Log::debug("验证签名结果:" .(int)$flag);
            Log::debug("通知返回结果:failure");
        }
    }
    /**
     *
     * 初始化异步通知
     *
     **/
    public function sendProcess()
    {
        //初始化日志
        Log::debug("sendProcess begin notify");
        self::NotifyProcess();
    }

}

