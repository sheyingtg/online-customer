<?php

namespace app\common\Notices;

require_once ROOT_PATH."/extend/wxpay/WxPayConfig.php";
use app\common\exception\ApiException;
use app\common\services\admin\PayService;
use \think\facade\Log;


/**
 * 微信异步通知处理方法
 **/

class PayNotifyCallBack extends \WxPayNotify
{
    /**
     *
     * 回包前的回调方法
     * 业务可以继承该方法，打印日志方便定位
     * @param string $xmlData 返回的xml参数
     *
     **/
    public function LogAfterProcess($xmlData)
    {
        Log::debug("LogAfterProcess call back， return xml:" . $xmlData);
        return;
    }

    //重写回调处理函数
    /**
     * @param WxPayNotifyResults $data 回调解释出的参数
     * @param WxPayConfigInterface $config
     * @param string $msg 如果回调处理失败，可以将错误信息输出到该方法
     * @return true回调出来完成不需要继续回调，false回调处理未完成需要继续回调
     */
    public function NotifyProcess($objData, $config, &$msg)
    {
        $data = $objData->GetValues();
        //TODO 1、进行参数校验
        if(!array_key_exists("return_code", $data)
            ||(array_key_exists("return_code", $data) && $data['return_code'] != "SUCCESS")) {
            //TODO失败,不是支付成功的通知
            //如果有需要可以做失败时候的一些清理处理，并且做一些监控
            $msg = "异常异常";
            Log::debug("异常异常:" . $config);
            return false;
        }
        if(!array_key_exists("transaction_id", $data)){
            $msg = "输入参数不正确";
            Log::debug($msg);
            return false;
        }

        //TODO 2、进行签名验证
        try {
            $checkResult = $objData->CheckSign($config);
            if($checkResult == false){
                //签名错误
                Log::debug("签名错误...");
                return false;
            }
        } catch(\Exception $e) {
            Log::debug(json_encode($e));
        }

        //TODO 3、处理业务逻辑
        Log::debug("支付回调参数:" . json_encode($data,320));

        //处理平台订单支付状态同步
        try {
            PayService::sysTransactionNotify($data,$data["cash_fee"]/100,$data['transaction_id'],strtotime($data['time_end']));
        }catch (ApiException $e){
            Log::debug('业务处理失败：'.$e->getMessage());//获取订单状态更新失败原因
            return false;
        }
        return true;
    }
}

class WxpayNotice{
    /**
     *
     * APP微信支付初始化异步通知
     *
     **/
    public function sendAppProcess()
    {
        $config = new \WxPayConfig();
        $config->setAppTrade();
        LogHandler::DEBUG("sendProcess begin notify");
        $notify = new PayNotifyCallBack();
        $notify->Handle($config);
    }
    /**
     *
     * Jsapi微信支付初始化异步通知
     *
     **/
    public function sendJsapiProcess()
    {
        $config = new \WxPayConfig();
        $config->setJsapiTrade();
        Log::debug("sendProcess begin notify");
        $notify = new PayNotifyCallBack();
        $notify->Handle($config);
    }
    /**
     *
     * 小程序微信支付初始化异步通知
     *
     **/
    public function sendXcxapiProcess()
    {
        $config = new \WxPayConfig();
        $config->setXcxapiTrade();
        Log::debug("sendProcess begin notify");
        $notify = new PayNotifyCallBack();
        $notify->Handle($config);
    }
}
