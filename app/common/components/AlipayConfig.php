<?php


namespace app\common\components;


/**
 * 支付宝参数配置类
 * Class AlipayConfig
 * @package app\common\components
 */
class AlipayConfig
{
    private $appId = "";//APPID
    private $rsaPrivateKey = "";//支付宝私钥
    private $rsaPublicKey = ""; // 支付宝公钥
//    private $gatewayUrl = 'https://openapi.alipay.com/gateway.do';//正式网关
    private $gatewayUrl = 'https://openapi.alipaydev.com/gateway.do';//沙盒网关
    private $notifyUrl = '';//默认回调地址


    public function __construct($config = [])
    {
        if(empty($config) && is_null($this->config)){
            $config = xn_cfg('payment.alipay');//支付配置
        }
        //默认回调地址
        $this->notifyUrl = get_host().'/admin/payDemo/alipayNotify';

        if($config){
            $this->appId = $config['appid'];
            $this->rsaPrivateKey = $config['privateKey'];
            $this->rsaPublicKey = $config['publicKey'];
        }
    }


    //获取appid
    public function getAppId(){
        return $this->appId;
    }

    //获取私钥
    public function getPrivateKey(){
        return $this->rsaPrivateKey;
    }

    //获取公钥
    public function getPublicKey(){
        return $this->rsaPublicKey;
    }

    //默认网关
    public function getGatewayUrl(){
        return $this->gatewayUrl;
    }

    //回调地址
    public function getNotifyUrl(){
        return $this->notifyUrl;
    }
}