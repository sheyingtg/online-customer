<?php

namespace app\common\components;

require_once ROOT_PATH."extend/alipay/aop/AopClient.php";
require_once ROOT_PATH."extend/alipay/aop/request/AlipayTradePagePayRequest.php";
require_once ROOT_PATH."extend/alipay/aop/request/AlipayTradeRefundRequest.php";

/**
 * 支付宝支付类
 */
class AlipayTrade  extends BaseComponents
{
    /**
     * @param $payData 支付信息(包含订单号 订单金额 订单主体 订单描述)
     * 统一下单
     * @author 小聂
     * @since 2020/8/28
     */
    static function unifiedOrder_h5($payData){
        $payConfig = new AlipayConfig();

        $aop = new  \AopClient(); // 从根目录开始加载，第三方的类会找不到
        $aop->gatewayUrl = $payConfig->getGatewayUrl();
        $aop->appId = trim($payConfig->getAppId()); // APP ID 应用ID
        $aop->rsaPrivateKey = trim($payConfig->getPrivateKey()); // 开发者私钥
        $aop->format  = "json"; // 加密方式
        $aop->signType = "RSA2"; // 加密方式
        $aop->alipayrsaPublicKey = trim($payConfig->getPublicKey()); // 支付宝公钥

        $request = new \AlipayTradePagePayRequest(); // 实例化具体API对应的request类,类名称和接口名称对应,当前调用接口名称：alipay.trade.app.pay
        //SDK已经封装掉了公共参数，这里只需要传入业务参数
        $bizcontent = [
            'body' => $payData['title'],
            'subject' => $payData['product_body']?:'body',
            'out_trade_no' => $payData['out_trade_no'],
            'total_amount' => $payData['money'],
            'product_code' => 'FAST_INSTANT_TRADE_PAY'// h5支付
        ];
        //设置回调地址
        if($payData['notifyUrl']){
            $request->setNotifyUrl($payData['notifyUrl']);
        }else{
            $request->setNotifyUrl($payConfig->getNotifyUrl());
        }
        $request->setBizContent(json_encode($bizcontent,320));
        //这里和普通的接口调用不同，使用的是sdkExecute
        $response = $aop->pageExecute($request,'post');// h5跳转支付
        $result = [
            //'alipay_res' => htmlspecialchars($response),
            'alipay_res' => $response,
        ];
        return $result;
    }

    /**
 * @param $payData 支付信息(包含订单号 订单金额 订单主体 订单描述)
 * 统一下单
 * @author 小聂
 * @since 2020/8/28
 */
    static function unifiedOrder($payData){
        $payConfig = new AlipayConfig();

        $aop = new  \AopClient(); // 从根目录开始加载，第三方的类会找不到
        $aop->gatewayUrl = $payConfig->getGatewayUrl();
        $aop->appId = trim($payConfig->getAppId()); // APP ID 应用ID
        $aop->rsaPrivateKey = trim($payConfig->getPrivateKey()); // 开发者私钥
        $aop->signType = "RSA2"; // 加密方式
        $aop->alipayrsaPublicKey = trim($payConfig->getPublicKey()); // 支付宝公钥
        $request = new \AlipayTradeAppPayRequest(); // 实例化具体API对应的request类,类名称和接口名称对应,当前调用接口名称：alipay.trade.app.pay
        //SDK已经封装掉了公共参数，这里只需要传入业务参数
        $bizcontent = [
            'body' => $payData['title'],
            'subject' => $payData['product_body']?:'body',
            'out_trade_no' => $payData['out_trade_no'],
            'total_amount' => $payData['money'],
            'product_code' => 'QUICK_MSECURITY_PAY'  //app 支付
        ];
        //设置回调地址
        if($payData['notifyUrl']){
            $request->setNotifyUrl($payData['notifyUrl']);
        }else{
            $request->setNotifyUrl($payConfig->getNotifyUrl());
        }
        $request->setBizContent(json_encode($bizcontent,320));
        //这里和普通的接口调用不同，使用的是sdkExecute
        $response = $aop->sdkExecute($request);
        $result = [
            //'alipay_res' => htmlspecialchars($response),
            'alipay_res' => $response,
        ];
        return $result;
    }

    /**
     * 进行签名验证
     * @param $params
     */
    public static function checkSign($params){
        $payConfig = new AlipayConfig();
        $aop = new \AopClient();
        $aop->alipayrsaPublicKey = trim($payConfig->getPublicKey());//支付宝公钥
        $flag = $aop->rsaCheckV1($params,NULL,"RSA2");
        return $flag;
    }
    /**
     * 支付宝退款
     * @param $out_trade_no 商户订单号(微信订单号和商户订单号二选一)
     * @param $out_refund_no 商户退款单号
     * @param $total_fee 订单金额
     * @param $refund_fee 退款金额
     * @author 小聂
     * @since 2020/10/27
     */
    static public function refundOrder($out_trade_no,$out_refund_no,$total_fee,$refund_fee){
        $payConfig = new AlipayConfig();
        $aop = new \AopClient();
        $aop->gatewayUrl = $payConfig->getGatewayUrl();
        $aop->appId = trim($payConfig->getAppId()); // APP ID 应用ID
        $aop->rsaPrivateKey = trim($payConfig->getPrivateKey()); // 开发者私钥
        $aop->alipayrsaPublicKey = trim($payConfig->getPublicKey()); // 支付宝公钥
        $aop->apiVersion = '1.0';
        $aop->signType = 'RSA2';
        $aop->postCharset='UTF-8';
        $aop->format='json';
        $request = new \AlipayTradeRefundRequest ();
        $bizcontent = [
            'out_trade_no' => $out_trade_no,//订单号
            'refund_amount' => $refund_fee,//退款的金额
        ];
        if($total_fee!=$refund_fee){ //部分退款
            $bizcontent['out_request_no'] = 'HZ01RF001';//如需部分退款，则此参数必传
        }
        $request->setBizContent(json_encode($bizcontent,320));
        $result = $aop->execute ( $request);
        $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
        $resultCode = $result->$responseNode->code;
        if(!empty($resultCode)&&$resultCode == 10000){
            echo "成功";
        } else {
            echo "失败:".$result->$responseNode->sub_msg;
        }
    }

}
