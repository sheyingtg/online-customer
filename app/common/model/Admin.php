<?php
// +----------------------------------------------------------------------
// | 小牛Admin
// +----------------------------------------------------------------------
// | Website: www.xnadmin.cn
// +----------------------------------------------------------------------
// | Author: dav <85168163@qq.com>
// +----------------------------------------------------------------------

namespace app\common\model;

class Admin extends BaseModel
{
    protected $autoWriteTimestamp = true;

    public function authGroupAccess()
    {
        return $this->belongsToMany(AuthGroup::class, AuthGroupAccess::class, 'group_id', 'admin_id');
    }

    /**
     * 给某个用户 变更密钥
     */
    static function createSecret($id){
        $data = [
          'secret'=>md5($id.time().mt_rand(111,999)),
        ];
        self::updates(['id'=>$id],$data);
    }
}