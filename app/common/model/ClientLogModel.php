<?php

namespace app\common\model;

use think\db\BaseQuery;

class ClientLogModel extends BaseModel
{
    //protected $autoWriteTimestamp = true;

    public $name = 'client_log';

    

        //数据查询
    function getList($param){
        $order = $param['order']?:'id desc';
        $model = $this->order($order);
        $this->getListWhere($model,$param);
        $list = $model->paginate($param['limit']);
        return $list;
    }

    //获取导出数据
    function getExport($param,$fileName='',$type='xlsx'){
        $fileName = $fileName?:'数据表格';
        $fileName.='-'.date('YmdHis');
        //获取数据
        $order = $param['order']?:'id desc';
        $model = $this->order($order);
        $this->getListWhere($model,$param);
        $list = $model->select();
        if(empty($list)){
            return [];
        }
        foreach ($list as $k=>$v){
            if(is_numeric($v->create_time))$list[$k]['create_time'] = $v->create_time_text;

        }
        $list = $list->toArray();
        //得到表头
        $top = array_intersect_key(self::$fieldsList,$list[0]);
        //移除部分表头
        $top = array_diff_key($top,array_flip([]));
        //移除多余字段
        $list_new = [];
        foreach ($list as $k=>$v){
            $list_new[] = array_intersect_key($v,$top);
        }
        $list = $list_new;
        return [
            'fileName'=>$fileName,
            'top'=>$top,
            'data'=>$list,
            'type'=>$type,
        ];
    }

    /**
     * 设置列表查询条件
     * @param BaseQuery $model
     * @param array $param
     * @return array
     */
    function getListWhere($model,$param=[]){
        if(empty($param)){
            return [];
        }
        $where = [];
            
        if($param['id']){
            $where['id'] = $param['id'];
        }
    
        if($param['uid']){
            $where['uid'] = $param['uid'];
        }
    
        if($param['depart_id']){
            $where['depart_id'] = $param['depart_id'];
        }
    
        if($param['staff_id']){
            $where['staff_id'] = $param['staff_id'];
        }
    
        if($param['source']){
            $where['source'] = $param['source'];
        }
    
        if($param['extend']){
            $where['extend'] = $param['extend'];
        }
    
        if($param['header']){
            $where['header'] = $param['header'];
        }
    
        if($param['os']){
            $where['os'] = $param['os'];
        }
        
        if( $param['create_time']!='' ) {
            $create_time = explode('至',$param['create_time']);
            $date_time = [
                strtotime($create_time[0]),
                strtotime($create_time[1].' 23:59:59'),
            ];
            $model->whereBetween('create_time',$date_time);
        }


//        //检索查询
        if($param['search_key']){
            $where['id'] = $param['search_key'];
        }
        if($where){
            $model->where($where);
        }
    }

    //表字段别名
    public static $fieldsList = [
            'id'=>'',
            'uid'=>'客户id',
            'depart_id'=>'部门id',
            'staff_id'=>'接待人id',
        'ip'=>'ip地址',
        'ip_text'=>'ip地址所在地',
        'source'=>'来源网址',
            'extend'=>'客户附加信息',
            'header'=>'客户请求头',
            'os'=>'客户请求',
            'create_time'=>'',
            'headimg'=>'头像',

    ];

        //表字段状态
        public function getCreateTimeTextAttr($value,$data){
        if(is_numeric($data['create_time'])){
            return $data['create_time']>0 ? date(self::$formatTime,$data['create_time']) : '--';
        }else{
            return $data['create_time'];
        }
    }



}
