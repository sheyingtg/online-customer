<?php

namespace app\common\model;

use think\db\BaseQuery;

class ClientSignModel extends BaseModel
{
    //protected $autoWriteTimestamp = true;

    public $name = 'client_sign';
    static public $pastTime = 3600 * 24 * 7;//过期时间 7天

    /**
     * 传入用户id 计算登录凭证
     * @param $uid 用户id
     * @param bool $isCrowded 是否挤号
     * @return string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    static function create_sign($uid,$isCrowded=false){
        $model = new self();
        $time = time();
        $token = md5($uid.$time);
        $sign = $model->where(['uid'=>$uid])->find();
        if(empty($sign)){
            //新增
            $data = [
                'uid'=>$uid,
                'ticket'=>$token,
                'create_time'=>date('Y-m-d H:i:s'),
                'past_time'=>$time+self::$pastTime,
            ];
            $res = $model->insertGetId($data);
        }else{
            if($isCrowded){
                $sign->ticket = $token;
            }
            $sign->past_time = $time+self::$pastTime;
            $res = $sign->save();
        }
        if($res){
            return $sign->ticket?:$token;
        }
        return '';
    }

        //数据查询
    function getList($param){
        $order = $param['order']?:'id desc';
        $model = $this->order($order);
        $this->getListWhere($model,$param);
        $list = $model->paginate($param['limit']);
        return $list;
    }

    //获取导出数据
    function getExport($param,$fileName='',$type='xlsx'){
        $fileName = $fileName?:'数据表格';
        $fileName.='-'.date('YmdHis');
        //获取数据
        $order = $param['order']?:'id desc';
        $model = $this->order($order);
        $this->getListWhere($model,$param);
        $list = $model->select();
        if(empty($list)){
            return [];
        }
        foreach ($list as $k=>$v){
            if(is_numeric($v->create_time))$list[$k]['create_time'] = $v->create_time_text;
            if(is_numeric($v->past_time))$list[$k]['past_time'] = $v->past_time_text;

        }
        $list = $list->toArray();
        //得到表头
        $top = array_intersect_key(self::$fieldsList,$list[0]);
        //移除部分表头
        $top = array_diff_key($top,array_flip([]));
        //移除多余字段
        $list_new = [];
        foreach ($list as $k=>$v){
            $list_new[] = array_intersect_key($v,$top);
        }
        $list = $list_new;
        return [
            'fileName'=>$fileName,
            'top'=>$top,
            'data'=>$list,
            'type'=>$type,
        ];
    }

    /**
     * 设置列表查询条件
     * @param BaseQuery $model
     * @param array $param
     * @return array
     */
    function getListWhere($model,$param=[]){
        if(empty($param)){
            return [];
        }
        $where = [];
            
        if($param['id']){
            $where['id'] = $param['id'];
        }
    
        if($param['uid']){
            $where['uid'] = $param['uid'];
        }
    
        if($param['ticket']){
            $where['ticket'] = $param['ticket'];
        }
        
        if( $param['create_time']!='' ) {
            $create_time = explode('至',$param['create_time']);
            $date_time = [
                strtotime($create_time[0]),
                strtotime($create_time[1].' 23:59:59'),
            ];
            $model->whereBetween('create_time',$date_time);
        }
        
        if( $param['past_time']!='' ) {
            $create_time = explode('至',$param['past_time']);
            $date_time = [
                strtotime($create_time[0]),
                strtotime($create_time[1].' 23:59:59'),
            ];
            $model->whereBetween('past_time',$date_time);
        }


//        //检索查询
        if($param['search_key']){
            $where['id'] = $param['search_key'];
        }
        if($where){
            $model->where($where);
        }
    }

    //表字段别名
    public static $fieldsList = [
            'id'=>'id',
            'uid'=>'用户id',
            'ticket'=>'凭证',
            'create_time'=>'创建时间',
            'past_time'=>'过期时间',

    ];

        //表字段状态
        public function getCreateTimeTextAttr($value,$data){
        if(is_numeric($data['create_time'])){
            return $data['create_time']>0 ? date(self::$formatTime,$data['create_time']) : '--';
        }else{
            return $data['create_time'];
        }
    }

    public function getPastTimeTextAttr($value,$data){
        if(is_numeric($data['past_time'])){
            return $data['past_time']>0 ? date(self::$formatTime,$data['past_time']) : '--';
        }else{
            return $data['past_time'];
        }
    }



}
