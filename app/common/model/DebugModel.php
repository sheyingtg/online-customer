<?php

namespace app\common\model;

use think\db\BaseQuery;

class DebugModel extends BaseModel
{
    //protected $autoWriteTimestamp = true;

    public $name = 'debug';

    //写入访问日志
    static function addInfo($uid = 0,$type=0,$params=false){
        $data = [
            'uid'=>$uid,
            'url'=>'',//url(),
            'params'=>'',// json_encode($params?:input('param.'),302),
            'createtime'=>date('Y-m-d H:i:s'),
            'ip'=>'',// request()->ip(),
            'type'=>$type,
            'header'=>json_encode($_SERVER,302)
        ];
        self::insert($data);
        return true;
    }

        //数据查询
    function getList($param){
        $order = $param['order']?:'id desc';
        $model = $this->order($order);
        $this->getListWhere($model,$param);
        $list = $model->paginate($param['limit']);
        return $list;
    }

    //获取导出数据
    function getExport($param,$fileName='',$type='xlsx'){
        $fileName = $fileName?:'数据表格';
        $fileName.='-'.date('YmdHis');
        //获取数据
        $order = $param['order']?:'id desc';
        $model = $this->order($order);
        $this->getListWhere($model,$param);
        $list = $model->select();
        if(empty($list)){
            return [];
        }
        foreach ($list as $k=>$v){

        }
        $list = $list->toArray();
        //得到表头
        $top = array_intersect_key(self::$fieldsList,$list[0]);
        //移除部分表头
        $top = array_diff_key($top,array_flip([]));
        //移除多余字段
        $list_new = [];
        foreach ($list as $k=>$v){
            $list_new[] = array_intersect_key($v,$top);
        }
        $list = $list_new;
        return [
            'fileName'=>$fileName,
            'top'=>$top,
            'data'=>$list,
            'type'=>$type,
        ];
    }

    /**
     * 设置列表查询条件
     * @param BaseQuery $model
     * @param array $param
     * @return array
     */
    function getListWhere($model,$param=[]){
        if(empty($param)){
            return [];
        }
        $where = [];
            
        if($param['id']){
            $where['id'] = $param['id'];
        }
    
        if($param['uid']){
            $where['uid'] = $param['uid'];
        }
    
        if($param['url']){
            $where['url'] = $param['url'];
        }
    
        if($param['params']){
            $where['params'] = $param['params'];
        }
    
        if($param['createtime']){
            $where['createtime'] = $param['createtime'];
        }
    
        if($param['ip']){
            $where['ip'] = $param['ip'];
        }
    
        if($param['type']){
            $where['type'] = $param['type'];
        }
    
        if($param['header']){
            $where['header'] = $param['header'];
        }


//        //检索查询
        if($param['search_key']){
            $where['id'] = $param['search_key'];
        }
        if($where){
            $model->where($where);
        }
    }

    //表字段别名
    public static $fieldsList = [
            'id'=>'',
            'uid'=>'',
            'url'=>'',
            'params'=>'',
            'createtime'=>'',
            'ip'=>'',
            'type'=>'',
            'header'=>'',

    ];

        //表字段状态
    

}
