<?php

namespace app\common\model;

use think\db\BaseQuery;

class DemoModel extends BaseModel
{
    //protected $autoWriteTimestamp = true;

    public $name = 'demo';

                                                        //表字段别名
    public static $fieldsList2 = [
        'id'=>'ID',
        'img'=>'图片',
        'title'=>'标题',
        'status'=>'状态',
        'display_order'=>'排序',
        'create_time'=>'创建时间',
        'update_time'=>'更新时间',
    ];

//测试
    function getUserInfo(){
        return $this->hasOne("Member","id","id");
    }

        //数据查询
    function getList($param){
        $order = $param['order']?:'id desc';
        $model = $this->order($order);
        $this->getListWhere($model,$param);
        $list = $model->paginate($param['limit']);
        return $list;
    }

    //获取导出数据
    function getExport($param,$fileName='',$type='xlsx'){
        $fileName = $fileName?:'数据表格';
        $fileName.='-'.date('YmdHis');
        //获取数据
        $order = $param['order']?:'id desc';
        $model = $this->order($order);
        $this->getListWhere($model,$param);
        $list = $model->select();
        if(empty($list)){
            return [];
        }
        foreach ($list as $k=>$v){
            if(is_numeric($v->create_time))$list[$k]['create_time'] = $v->create_time_text;
            if(is_numeric($v->update_time))$list[$k]['update_time'] = $v->update_time_text;

        }
        $list = $list->toArray();
        //得到表头
        $top = array_intersect_key(self::$fieldsList,$list[0]);
        //移除部分表头
        $top = array_diff_key($top,array_flip([]));
        //移除多余字段
        $list_new = [];
        foreach ($list as $k=>$v){
            $list_new[] = array_intersect_key($v,$top);
        }
        $list = $list_new;
        return [
            'fileName'=>$fileName,
            'top'=>$top,
            'data'=>$list,
            'type'=>$type,
        ];
    }

    /**
     * 设置列表查询条件
     * @param BaseQuery $model
     * @param array $param
     * @return array
     */
    function getListWhere($model,$param=[]){
        if(empty($param)){
            return [];
        }
        $where = [];
            
        if($param['id']){
            $where['id'] = $param['id'];
        }
    
        if($param['img']){
            $where['img'] = $param['img'];
        }
    
        if($param['tag']){
            $where['tag'] = $param['tag'];
        }
    
        if($param['title']){
            $where['title'] = $param['title'];
        }
    
        if($param['status']){
            $where['status'] = $param['status'];
        }
    
        if($param['display_order']){
            $where['display_order'] = $param['display_order'];
        }
        
        if( $param['create_time']!='' ) {
            $create_time = explode('至',$param['create_time']);
            $date_time = [
                strtotime($create_time[0]),
                strtotime($create_time[1].' 23:59:59'),
            ];
            $model->whereBetween('create_time',$date_time);
        }
        
        if( $param['update_time']!='' ) {
            $create_time = explode('至',$param['update_time']);
            $date_time = [
                strtotime($create_time[0]),
                strtotime($create_time[1].' 23:59:59'),
            ];
            $model->whereBetween('update_time',$date_time);
        }


//        //检索查询
        if($param['search_key']){
            $where['id'] = $param['search_key'];
        }
        if($where){
            $model->where($where);
        }
    }

    //表字段别名
    public static $fieldsList = [
            'id'=>'',
            'img'=>'图片',
            'tag'=>'多选标签',
            'title'=>'标题',
            'status'=>'是否显示',
            'display_order'=>'排序',
            'create_time'=>'创建时间',
            'update_time'=>'更新时间',

    ];

        //表字段状态
        public function getCreateTimeTextAttr($value,$data){
        if(is_numeric($data['create_time'])){
            return $data['create_time']>0 ? date(self::$formatTime,$data['create_time']) : '--';
        }else{
            return $data['create_time'];
        }
    }

    public function getUpdateTimeTextAttr($value,$data){
        if(is_numeric($data['update_time'])){
            return $data['update_time']>0 ? date(self::$formatTime,$data['update_time']) : '--';
        }else{
            return $data['update_time'];
        }
    }



}
