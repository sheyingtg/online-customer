<?php

namespace app\common\model;

use think\db\BaseQuery;

class DepartmentModel extends BaseModel
{
    //protected $autoWriteTimestamp = true;

    public $name = 'department';

    /**
     * 更新各部门人数
     * @param int $admin_id
     */
    static function RefreshNumber($admin_id=0){
        $where = [];
        $staffWhere =[];
        if($admin_id){
            $where['admin_id'] = $admin_id;
            $staffWhere['admin_id'] = $admin_id;
        }
        $list = self::findAll($where,'id');
        foreach ($list as $k=>$v){
            $staffWhere['depart_id'] = $v['id'];
            self::updates(['id'=>$v['id']],[
                'number'=>StaffModel::findCount($staffWhere)
            ]);
        }
        return true;
    }

        //数据查询
    function getList($param){
        $order = $param['order']?:'id desc';
        $model = $this->order($order);
        $this->getListWhere($model,$param);
        $this->setAdmin($model,ADMIN_ID);
        $list = $model->paginate($param['limit']);
        return $list;
    }

    //获取导出数据
    function getExport($param,$fileName='',$type='xlsx'){
        $fileName = $fileName?:'数据表格';
        $fileName.='-'.date('YmdHis');
        //获取数据
        $order = $param['order']?:'id desc';
        $model = $this->order($order);
        $this->getListWhere($model,$param);
        $list = $model->select();
        if(empty($list)){
            return [];
        }
        foreach ($list as $k=>$v){
            $list[$k]['status'] = $v->status_text;
            if(is_numeric($v->create_time))$list[$k]['create_time'] = $v->create_time_text;
            if(is_numeric($v->update_time))$list[$k]['update_time'] = $v->update_time_text;

        }
        $list = $list->toArray();
        //得到表头
        $top = array_intersect_key(self::$fieldsList,$list[0]);
        //移除部分表头
        $top = array_diff_key($top,array_flip([]));
        //移除多余字段
        $list_new = [];
        foreach ($list as $k=>$v){
            $list_new[] = array_intersect_key($v,$top);
        }
        $list = $list_new;
        return [
            'fileName'=>$fileName,
            'top'=>$top,
            'data'=>$list,
            'type'=>$type,
        ];
    }

    /**
     * 设置列表查询条件
     * @param BaseQuery $model
     * @param array $param
     * @return array
     */
    function getListWhere($model,$param=[]){
        if(empty($param)){
            return [];
        }
        $where = [];
            
        if($param['id']){
            $where['id'] = $param['id'];
        }
    
        if($param['name']){
            $where['name'] = $param['name'];
        }
    
        if($param['number']){
            $where['number'] = $param['number'];
        }
    
        if($param['status']){
            $where['status'] = $param['status'];
        }
    
        if($param['priority']){
            $where['priority'] = $param['priority'];
        }
        
        if( $param['create_time']!='' ) {
            $create_time = explode('至',$param['create_time']);
            $date_time = [
                strtotime($create_time[0]),
                strtotime($create_time[1].' 23:59:59'),
            ];
            $model->whereBetween('create_time',$date_time);
        }
        
        if( $param['update_time']!='' ) {
            $create_time = explode('至',$param['update_time']);
            $date_time = [
                strtotime($create_time[0]),
                strtotime($create_time[1].' 23:59:59'),
            ];
            $model->whereBetween('update_time',$date_time);
        }


//        //检索查询
        if($param['search_key']){
            $where['id'] = $param['search_key'];
        }
        if($where){
            $model->where($where);
        }
    }

    //表字段别名
    public static $fieldsList = [
            'id'=>'ID',
            'admin_id'=>'公司ID',
            'name'=>'部门名',
            'number'=>'部门人数',
            'status'=>'部门状态',
            'priority'=>'优先级',
            'create_time'=>'创建时间',
            'update_time'=>'更新时间',

    ];

        //表字段状态
        public static $statusList = [
            '1'=>'启用',
            '2'=>'禁用',
    ];

    //部门状态获取器
    public function getStatusTextAttr($value,$data){
        if(strpos($data['status'],',')){
            $arr = explode(',',$data['status']);
            $arrText = [];
            foreach ($arr as $v){
                $arrText[]=self::$statusList[$v];
            }
            return join(',',$arrText);
        }else{
            return self::$statusList[$data['status']]?:'--';
        }
    }

    public function getCreateTimeTextAttr($value,$data){
        if(is_numeric($data['create_time'])){
            return $data['create_time']>0 ? date(self::$formatTime,$data['create_time']) : '--';
        }else{
            return $data['create_time'];
        }
    }

    public function getUpdateTimeTextAttr($value,$data){
        if(is_numeric($data['update_time'])){
            return $data['update_time']>0 ? date(self::$formatTime,$data['update_time']) : '--';
        }else{
            return $data['update_time'];
        }
    }



}
