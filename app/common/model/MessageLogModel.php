<?php

namespace app\common\model;

use think\db\BaseQuery;

class MessageLogModel extends BaseModel
{
    //protected $autoWriteTimestamp = true;

    public $name = 'message_log';

    /**
     * 获取俩人之间的最后一条聊天记录
     * @param $uid
     * @param $toid
     */
    static function getLastMsg($uid,$toid){
        //获取最后的一条聊天记录
        $where = " (from_user_id = {$uid} and to_user_id = {$toid}) or (from_user_id = {$toid} and to_user_id = {$uid}) ";
        return MessageLogModel::findOne($where,'id,type,msg,datetime','id desc');
    }

    /**
     * 新增一条聊天记录
     */
    static function addMessage($data){
        $data['datetime'] =date('Y-m-d H:i:s');
        return self::saveData($data);
    }

        //数据查询
    function getList($param){
        $order = $param['order']?:'id desc';
        $model = $this->order($order);
        $this->getListWhere($model,$param['where']);
        $list = $model->paginate($param['limit']);
        return $list;
    }

    //获取导出数据
    function getExport($param,$fileName='',$type='xlsx'){
        $fileName = $fileName?:'数据表格';
        $fileName.='-'.date('YmdHis');
        //获取数据
        $order = $param['order']?:'id desc';
        $model = $this->order($order);
        $this->getListWhere($model,$param);
        $list = $model->select();
        if(empty($list)){
            return [];
        }
        foreach ($list as $k=>$v){
            $list[$k]['type'] = $v->type_text;

        }
        $list = $list->toArray();
        //得到表头
        $top = array_intersect_key(self::$fieldsList,$list[0]);
        //移除部分表头
        $top = array_diff_key($top,array_flip([]));
        //移除多余字段
        $list_new = [];
        foreach ($list as $k=>$v){
            $list_new[] = array_intersect_key($v,$top);
        }
        $list = $list_new;
        return [
            'fileName'=>$fileName,
            'top'=>$top,
            'data'=>$list,
            'type'=>$type,
        ];
    }

    /**
     * 设置列表查询条件
     * @param BaseQuery $model
     * @param array $param
     * @return array
     */
    function getListWhere($model,$param=[]){
        if(empty($param)){
            return [];
        }
        $where = [];
        if(!is_array($param)){
            $where = $param;
        }

        if($param['id']){
            $where['id'] = $param['id'];
        }
    
        if($param['type']){
            $where['type'] = $param['type'];
        }
    
        if($param['from_user_id']){
            $where['from_user_id'] = $param['from_user_id'];
        }
    
        if($param['from_user_nickname']){
            $where['from_user_nickname'] = $param['from_user_nickname'];
        }
    
        if($param['from_user_headimg']){
            $where['from_user_headimg'] = $param['from_user_headimg'];
        }
    
        if($param['from_role']){
            $where['from_role'] = $param['from_role'];
        }
    
        if($param['to_user_id']){
            $where['to_user_id'] = $param['to_user_id'];
        }
    
        if($param['msg']){
            $where['msg'] = $param['msg'];
        }
    
        if($param['datetime']){
            $where['datetime'] = $param['datetime'];
        }
    
        if($param['is_reda']){
            $where['is_reda'] = $param['is_reda'];
        }


//        //检索查询
        if($param['search_key']){
            $where['id'] = $param['search_key'];
        }
        if($where){
            $model->where($where);
        }
    }

    //表字段别名
    public static $fieldsList = [
            'id'=>'id',
            'type'=>'消息类型',
            'from_user_id'=>'发送人id',
            'from_user_nickname'=>'发送人昵称',
            'from_user_headimg'=>'发送人头像',
            'from_role'=>'级别',
            'to_user_id'=>'接收人id',
            'msg'=>'msg',
            'datetime'=>'发送时间',
            'is_reda'=>'是否已读',

    ];

        //表字段状态
        public static $typeList = [
            '1'=>'文本',
            '2'=>'图片',
            '3'=>'视频',
            '4'=>'音频',
            '5'=>'系统',
    ];

    //消息类型获取器
    public function getTypeTextAttr($value,$data){
        if(strpos($data['type'],',')){
            $arr = explode(',',$data['type']);
            $arrText = [];
            foreach ($arr as $v){
                $arrText[]=self::$typeList[$v];
            }
            return join(',',$arrText);
        }else{
            return self::$typeList[$data['type']]?:'--';
        }
    }

    public function getDatetimeAttr($value,$data){
//        if(!$value)return $value;
        return mdate2(strtotime($data['datetime']));
//        return date('Y-m-d H:i',strtotime($value));
    }

    //消息处理  主要处理时间消息
    public function getMsgAttr($value,$data){
        if($data['type']=='system'){
            if($value=='time'){
                //得到时间戳
                $time = strtotime($data['datetime']);
                return mdate2($time);
            }
        }else{
            return $value;
        }
    }


}
