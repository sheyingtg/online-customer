<?php

namespace app\common\model;

use think\db\BaseQuery;

class NewFriendsModel extends BaseModel
{
    //protected $autoWriteTimestamp = true;

    public $name = 'new_friends';


    function getUid(){
        $field = "id,name,nickname,headimg";
        return $this->hasOne("ClientModel",'id','uid')->field($field)->cache(10);
    }

    function getMid(){
        $field = "id,name,nickname,headimg";
        return $this->hasOne("ClientModel",'id','mid')->field($field)->cache(10);
    }

        //数据查询
    function getList($param){
        $order = $param['order']?:'id desc';
        $model = $this->order($order);
        $this->getListWhere($model,$param);
        $list = $model->paginate($param['limit']);
        return $list;
    }

    //获取导出数据
    function getExport($param,$fileName='',$type='xlsx'){
        $fileName = $fileName?:'数据表格';
        $fileName.='-'.date('YmdHis');
        //获取数据
        $order = $param['order']?:'id desc';
        $model = $this->order($order);
        $this->getListWhere($model,$param);
        $list = $model->select();
        if(empty($list)){
            return [];
        }
        foreach ($list as $k=>$v){
            $list[$k]['status'] = $v->status_text;
            if(is_numeric($v->create_time))$list[$k]['create_time'] = $v->create_time_text;

        }
        $list = $list->toArray();
        //得到表头
        $top = array_intersect_key(self::$fieldsList,$list[0]);
        //移除部分表头
        $top = array_diff_key($top,array_flip([]));
        //移除多余字段
        $list_new = [];
        foreach ($list as $k=>$v){
            $list_new[] = array_intersect_key($v,$top);
        }
        $list = $list_new;
        return [
            'fileName'=>$fileName,
            'top'=>$top,
            'data'=>$list,
            'type'=>$type,
        ];
    }

    /**
     * 设置列表查询条件
     * @param BaseQuery $model
     * @param array $param
     * @return array
     */
    function getListWhere($model,$param=[]){
        if(empty($param)){
            return [];
        }
        $where = [];
            
        if($param['id']){
            $where['id'] = $param['id'];
        }
    
        if($param['uid']){
            $where['uid'] = $param['uid'];
        }
    
        if($param['mid']){
            $where['mid'] = $param['mid'];
        }
    
        if($param['desc']){
            $where['desc'] = $param['desc'];
        }
    
        if($param['msg']){
            $where['msg'] = $param['msg'];
        }
    
        if($param['status']){
            $where['status'] = $param['status'];
        }
        
        if( $param['create_time']!='' ) {
            $create_time = explode('至',$param['create_time']);
            $date_time = [
                strtotime($create_time[0]),
                strtotime($create_time[1].' 23:59:59'),
            ];
            $model->whereBetween('create_time',$date_time);
        }
    
        if($param['from']){
            $where['from'] = $param['from'];
        }


//        //检索查询
        if($param['search_key']){
            $where['id'] = $param['search_key'];
        }
        if($where){
            $model->where($where);
        }
    }

    //表字段别名
    public static $fieldsList = [
            'id'=>'id',
            'uid'=>'用户id',
            'mid'=>'申请人id',
            'desc'=>'申请理由',
            'msg'=>'拒绝理由',
            'status'=>'状态',
            'create_time'=>'申请时间',
            'update_time'=>'处理时间',
            'from'=>'来源',

    ];

        //表字段状态
        public static $statusList = [
            '0'=>'待审核',
            '1'=>'已通过',
            '2'=>'已拒绝',
    ];

    //状态获取器
    public function getStatusTextAttr($value,$data){
        if(strpos($data['status'],',')){
            $arr = explode(',',$data['status']);
            $arrText = [];
            foreach ($arr as $v){
                $arrText[]=self::$statusList[$v];
            }
            return join(',',$arrText);
        }else{
            return self::$statusList[$data['status']]?:'--';
        }
    }

    public function getCreateTimeTextAttr($value,$data){
        if(is_numeric($data['create_time'])){
            return $data['create_time']>0 ? date(self::$formatTime,$data['create_time']) : '--';
        }else{
            return $data['create_time'];
        }
    }



}
