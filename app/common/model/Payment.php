<?php

namespace app\common\model;

class Payment extends BaseModel
{
    protected $autoWriteTimestamp = true;

    public $name = 'payment';

        //数据查询
    function getList($param){
        $order = $param['order']?:'id desc';
        $model = $this->order($order);
        $this->getListWhere($model,$param);
        $list = $model->paginate($param['limit']);
        return $list;
    }

    //获取导出数据
    function getExport($param,$fileName='',$type='xlsx'){
        $fileName = $fileName?:'数据表格';
        $fileName.='-'.date('YmdHis');
        //获取数据
        $order = $param['order']?:'id desc';
        $model = $this->order($order);
        $this->getListWhere($model,$param);
        $list = $model->select();
        if(empty($list)){
            return [];
        }
        foreach ($list as $k=>$v){
            if(is_numeric($v->create_time))$list[$k]['create_time'] = $v->create_time_text;
            $list[$k]['is_delete'] = $v->is_delete_text;
            $list[$k]['status'] = $v->status_text;
            $list[$k]['type'] = $v->type_text;
            if(is_numeric($v->update_time))$list[$k]['update_time'] = $v->update_time_text;

        }
        $list = $list->toArray();
        //得到表头
        $top = array_intersect_key(self::$fieldsList,$list[0]);
        return [
            'fileName'=>$fileName,
            'top'=>$top,
            'data'=>$list,
            'type'=>$type,
        ];
    }
    
    //设置列表查询条件
    function getListWhere($model,$param=[]){
        if(empty($param)){
            return [];
        }
        $where = [];
            
        if($param['attach']){
            $where['attach'] = $param['attach'];
        }
    
        if($param['client_ip']){
            $where['client_ip'] = $param['client_ip'];
        }
        
        if( $param['create_time']!='' ) {
            $create_time = explode('至',$param['create_time']);
            $date_time = [
                strtotime($create_time[0]),
                strtotime($create_time[1].' 23:59:59'),
            ];
            $model->whereBetween('create_time',$date_time);
        }
    
        if($param['id']){
            $where['id'] = $param['id'];
        }
    
        if($param['is_delete']){
            $where['is_delete'] = $param['is_delete'];
        }
    
        if($param['money']){
            $where['money'] = $param['money'];
        }
    
        if($param['openid']){
            $where['openid'] = $param['openid'];
        }
    
        if($param['order_type']){
            $where['order_type'] = $param['order_type'];
        }
    
        if($param['out_trade_no']){
            $where['out_trade_no'] = $param['out_trade_no'];
        }
    
        if($param['pay_trade_no']){
            $where['pay_trade_no'] = $param['pay_trade_no'];
        }
    
        if($param['product_body']){
            $where['product_body'] = $param['product_body'];
        }
    
        if($param['status']){
            $where['status'] = $param['status'];
        }
    
        if($param['tui_money']){
            $where['tui_money'] = $param['tui_money'];
        }
    
        if($param['type']){
            $where['type'] = $param['type'];
        }
        
        if( $param['update_time']!='' ) {
            $create_time = explode('至',$param['update_time']);
            $date_time = [
                strtotime($create_time[0]),
                strtotime($create_time[1].' 23:59:59'),
            ];
            $model->whereBetween('update_time',$date_time);
        }


//        //检索查询
        if($param['search_key']){
            $where['id'] = $param['search_key'];
        }
        if($where){
            $model->where($where);
        }
    }

    //表字段别名
    public static $fieldsList = [
            'attach'=>'附加字段',
            'client_ip'=>'ip',
            'create_time'=>'创建时间',
            'id'=>'id',
            'is_delete'=>'是否删除',
            'money'=>'支付金额',
            'openid'=>'openid',
            'order_type'=>'支付类型',
            'out_trade_no'=>'商户订单号',
            'pay_trade_no'=>'支付订单号',
            'product_body'=>'订单描述',
            'status'=>'订单状态',
            'tui_money'=>'退款金额',
            'type'=>'支付方式',
            'update_time'=>'更新时间',

    ];

        //表字段状态
        public function getCreateTimeTextAttr($value,$data){
        if(is_numeric($data['create_time'])){
            return date(self::$formatTime,$data['create_time']);
        }else{
            return $data['create_time'];
        }
    }

    public static $is_deleteList = [
            '0'=>'正常',
            '1'=>'已删除',
    ];

    //是否删除获取器
    public function getIsDeleteTextAttr($value,$data){
        if(strpos($data['is_delete'],',')){
            $arr = explode(',',$data['is_delete']);
            $arrText = [];
            foreach ($arr as $v){
                $arrText[]=self::$is_deleteList[$v];
            }
            return join(',',$arrText);
        }else{
            return self::$is_deleteList[$data['is_delete']]?:'--';
        }
    }

    public static $statusList = [
            '0'=>'未处理',
            '1'=>'支付成功',
            '2'=>'部分退款',
            '3'=>'全额退款',
    ];

    //订单状态获取器
    public function getStatusTextAttr($value,$data){
        if(strpos($data['status'],',')){
            $arr = explode(',',$data['status']);
            $arrText = [];
            foreach ($arr as $v){
                $arrText[]=self::$statusList[$v];
            }
            return join(',',$arrText);
        }else{
            return self::$statusList[$data['status']]?:'--';
        }
    }

    public static $typeList = [
            '0'=>'未支付',
            '1'=>'微信支付',
    ];

    //支付方式获取器
    public function getTypeTextAttr($value,$data){
        if(strpos($data['type'],',')){
            $arr = explode(',',$data['type']);
            $arrText = [];
            foreach ($arr as $v){
                $arrText[]=self::$typeList[$v];
            }
            return join(',',$arrText);
        }else{
            return self::$typeList[$data['type']]?:'--';
        }
    }

    public function getUpdateTimeTextAttr($value,$data){
        if(is_numeric($data['update_time'])){
            return date(self::$formatTime,$data['update_time']);
        }else{
            return $data['update_time'];
        }
    }



}
