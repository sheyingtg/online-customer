<?php

namespace app\common\model;

class PaymentRefund extends BaseModel
{
    protected $autoWriteTimestamp = true;

    public $name = 'payment_refund';

        //数据查询
    function getList($param){
        $order = $param['order']?:'id desc';
        $model = $this->order($order);
        $this->getListWhere($model,$param);
        $list = $model->paginate($param['limit']);
        return $list;
    }

    //获取导出数据
    function getExport($param,$fileName='',$type='xlsx'){
        $fileName = $fileName?:'数据表格';
        $fileName.='-'.date('YmdHis');
        //获取数据
        $order = $param['order']?:'id desc';
        $model = $this->order($order);
        $this->getListWhere($model,$param);
        $list = $model->select();
        if(empty($list)){
            return [];
        }
        foreach ($list as $k=>$v){
            if(is_numeric($v->create_time))$list[$k]['create_time'] = $v->create_time_text;

        }
        $list = $list->toArray();
        //得到表头
        $top = array_intersect_key(self::$fieldsList,$list[0]);
        return [
            'fileName'=>$fileName,
            'top'=>$top,
            'data'=>$list,
            'type'=>$type,
        ];
    }
    
    //设置列表查询条件
    function getListWhere($model,$param=[]){
        if(empty($param)){
            return [];
        }
        $where = [];
            
        if($param['client_ip']){
            $where['client_ip'] = $param['client_ip'];
        }
        
        if( $param['create_time']!='' ) {
            $create_time = explode('至',$param['create_time']);
            $date_time = [
                strtotime($create_time[0]),
                strtotime($create_time[1].' 23:59:59'),
            ];
            $model->whereBetween('create_time',$date_time);
        }
    
        if($param['id']){
            $where['id'] = $param['id'];
        }
    
        if($param['money']){
            $where['money'] = $param['money'];
        }
    
        if($param['pid']){
            $where['pid'] = $param['pid'];
        }
    
        if($param['refund_sn']){
            $where['refund_sn'] = $param['refund_sn'];
        }


//        //检索查询
        if($param['search_key']){
            $where['id'] = $param['search_key'];
        }
        if($where){
            $model->where($where);
        }
    }

    //表字段别名
    public static $fieldsList = [
            'client_ip'=>'client_ip',
            'create_time'=>'create_time',
            'id'=>'id',
            'money'=>'money',
            'pid'=>'pid',
            'refund_sn'=>'refund_sn',

    ];

        //表字段状态
        public function getCreateTimeTextAttr($value,$data){
        if(is_numeric($data['create_time'])){
            return date(self::$formatTime,$data['create_time']);
        }else{
            return $data['create_time'];
        }
    }



}
