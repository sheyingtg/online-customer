<?php

namespace app\common\model;

use think\db\BaseQuery;

class StaffTicketModel extends BaseModel
{
    //protected $autoWriteTimestamp = true;

    public $name = 'staff_ticket';

    //检查 某个ticket 是否有用户
    static function check($ticket){
        $res = self::findOne(['ticket'=>$ticket],'uid');
        return $res['uid']?:0;
    }

    //给某个用户设置新的ticket
    static function setSticket($uid){
        $ticket = self::find(['uid'=>$uid],'id');
        $new_ticket = self::getTicket($uid);
        $data = [
            'ticket'=>$new_ticket,
            'update_time'=>date('Y-m-d H:i:s'),
            'equi'=>getOs(),//获取设备信息
        ];
        if($ticket){
            //存在 则修改
            self::updates(['id'=>$ticket['id']],$data);
        }else{
            $data['uid'] = $uid;
            $data['create_time'] = $data['update'];
            self::saveData($data);
        }
        return $new_ticket;
    }

    //生成随机的ticket
    static function getTicket($uid){
        return md5($uid.time());
    }

        //数据查询
    function getList($param){
        $order = $param['order']?:'id desc';
        $model = $this->order($order);
        $this->getListWhere($model,$param);
        $list = $model->paginate($param['limit']);
        return $list;
    }

    //获取导出数据
    function getExport($param,$fileName='',$type='xlsx'){
        $fileName = $fileName?:'数据表格';
        $fileName.='-'.date('YmdHis');
        //获取数据
        $order = $param['order']?:'id desc';
        $model = $this->order($order);
        $this->getListWhere($model,$param);
        $list = $model->select();
        if(empty($list)){
            return [];
        }
        foreach ($list as $k=>$v){
            if(is_numeric($v->create_time))$list[$k]['create_time'] = $v->create_time_text;
            if(is_numeric($v->update_time))$list[$k]['update_time'] = $v->update_time_text;

        }
        $list = $list->toArray();
        //得到表头
        $top = array_intersect_key(self::$fieldsList,$list[0]);
        //移除部分表头
        $top = array_diff_key($top,array_flip([]));
        //移除多余字段
        $list_new = [];
        foreach ($list as $k=>$v){
            $list_new[] = array_intersect_key($v,$top);
        }
        $list = $list_new;
        return [
            'fileName'=>$fileName,
            'top'=>$top,
            'data'=>$list,
            'type'=>$type,
        ];
    }

    /**
     * 设置列表查询条件
     * @param BaseQuery $model
     * @param array $param
     * @return array
     */
    function getListWhere($model,$param=[]){
        if(empty($param)){
            return [];
        }
        $where = [];
            
        if($param['id']){
            $where['id'] = $param['id'];
        }
    
        if($param['uid']){
            $where['uid'] = $param['uid'];
        }
    
        if($param['ticket']){
            $where['ticket'] = $param['ticket'];
        }
        
        if( $param['create_time']!='' ) {
            $create_time = explode('至',$param['create_time']);
            $date_time = [
                strtotime($create_time[0]),
                strtotime($create_time[1].' 23:59:59'),
            ];
            $model->whereBetween('create_time',$date_time);
        }
        
        if( $param['update_time']!='' ) {
            $create_time = explode('至',$param['update_time']);
            $date_time = [
                strtotime($create_time[0]),
                strtotime($create_time[1].' 23:59:59'),
            ];
            $model->whereBetween('update_time',$date_time);
        }


//        //检索查询
        if($param['search_key']){
            $where['id'] = $param['search_key'];
        }
        if($where){
            $model->where($where);
        }
    }

    //表字段别名
    public static $fieldsList = [
            'id'=>'id',
            'uid'=>'用户id',
        'ticket'=>'登录凭证',
        'equi'=>'登录凭证',
            'create_time'=>'创建时间',
            'update_time'=>'更新时间',

    ];

        //表字段状态
        public function getCreateTimeTextAttr($value,$data){
        if(is_numeric($data['create_time'])){
            return $data['create_time']>0 ? date(self::$formatTime,$data['create_time']) : '--';
        }else{
            return $data['create_time'];
        }
    }

    public function getUpdateTimeTextAttr($value,$data){
        if(is_numeric($data['update_time'])){
            return $data['update_time']>0 ? date(self::$formatTime,$data['update_time']) : '--';
        }else{
            return $data['update_time'];
        }
    }



}
