<?php

namespace app\common\model;

use think\db\BaseQuery;
use think\model\concern\SoftDelete;

class UserGroupModel extends BaseModel
{
    //protected $autoWriteTimestamp = true;
    use SoftDelete;
    public $name = 'user_group';
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;
    /**
     * 传入数量
     * @param $number
     */
    static function getUserListAll($id,$number=9){
        $model = new UserListModel();
        $where = ['mid'=>$id,'type'=>2];
        $list = $model->with(['getClientUid'])->where($where)->limit($number)->cache(60)->select();

        return $list;
//        UserListModel::findAll(,'')
    }

    /**
     * 通过群组 获取群组中的用户
     */
    function getUserList(){
        return $this->hasMany(UserListModel::class,'mid','id');
    }

        //数据查询
    function getList($param){
        $order = $param['order']?:'id desc';
        $model = $this->order($order);
        $this->getListWhere($model,$param);
        $list = $model->paginate($param['limit']);
        return $list;
    }

    //获取导出数据
    function getExport($param,$fileName='',$type='xlsx'){
        $fileName = $fileName?:'数据表格';
        $fileName.='-'.date('YmdHis');
        //获取数据
        $order = $param['order']?:'id desc';
        $model = $this->order($order);
        $this->getListWhere($model,$param);
        $list = $model->select();
        if(empty($list)){
            return [];
        }
        foreach ($list as $k=>$v){
            if(is_numeric($v->create_time))$list[$k]['create_time'] = $v->create_time_text;
            if(is_numeric($v->update_time))$list[$k]['update_time'] = $v->update_time_text;

        }
        $list = $list->toArray();
        //得到表头
        $top = array_intersect_key(self::$fieldsList,$list[0]);
        //移除部分表头
        $top = array_diff_key($top,array_flip([]));
        //移除多余字段
        $list_new = [];
        foreach ($list as $k=>$v){
            $list_new[] = array_intersect_key($v,$top);
        }
        $list = $list_new;
        return [
            'fileName'=>$fileName,
            'top'=>$top,
            'data'=>$list,
            'type'=>$type,
        ];
    }

    /**
     * 设置列表查询条件
     * @param BaseQuery $model
     * @param array $param
     * @return array
     */
    function getListWhere($model,$param=[]){
        if(empty($param)){
            return [];
        }
        $where = [];
            
        if($param['id']){
            $where['id'] = $param['id'];
        }
    
        if($param['admin_id']){
            $where['admin_id'] = $param['admin_id'];
        }
    
        if($param['admin_uid']){
            $where['admin_uid'] = $param['admin_uid'];
        }
    
        if($param['name']){
            $where['name'] = $param['name'];
        }
    
        if($param['member_number']){
            $where['member_number'] = $param['member_number'];
        }
        
        if( $param['create_time']!='' ) {
            $create_time = explode('至',$param['create_time']);
            $date_time = [
                strtotime($create_time[0]),
                strtotime($create_time[1].' 23:59:59'),
            ];
            $model->whereBetween('create_time',$date_time);
        }


//        //检索查询
        if($param['search_key']){
            $where['id'] = $param['search_key'];
        }
        if($where){
            $model->where($where);
        }
    }

    //表字段别名
    public static $fieldsList = [
            'id'=>'id',
            'admin_id'=>'admin_id',
            'admin_uid'=>'群主id',
            'name'=>'群名称',
            'member_number'=>'群成员人数',
            'create_time'=>'群创建时间',
            'update_time'=>'群更新时间',

    ];

        //表字段状态
        public function getCreateTimeTextAttr($value,$data){
        if(is_numeric($data['create_time'])){
            return $data['create_time']>0 ? date(self::$formatTime,$data['create_time']) : '--';
        }else{
            return $data['create_time'];
        }
    }

    public function getUpdateTimeTextAttr($value,$data){
        if(is_numeric($data['update_time'])){
            return $data['update_time']>0 ? date(self::$formatTime,$data['update_time']) : '--';
        }else{
            return $data['update_time'];
        }
    }



}
