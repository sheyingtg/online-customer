<?php

namespace app\common\model;

use think\db\BaseQuery;
use think\model\concern\SoftDelete;
class UserListModel extends BaseModel
{
    //protected $autoWriteTimestamp = true;
    use SoftDelete;
    public $name = 'user_list';
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;

    /**
     * 关联查询 对方 用户信息
     */
    function getClientUid(){
        $info = $this->hasOne(ClientModel::class,'id','uid');
        return $info;
    }

    /**
     * 关联查询 对方 用户信息
     */
    function getClientMid(){
        $info = $this->hasOne(ClientModel::class,'id','mid');
        return $info;
    }

    /**
     * 获取群组信息
     */
    function getUserGroup(){
        return $this->hasOne(UserGroupModel::class,'id','mid');
    }


    /**
     * 给某人好友列表  新增用户
     * @param $uid
     * @param $mid
     */
    static function addUser($uid,$mid,$exit=false){
        if(!$data = self::findOne(['uid'=>$uid,'mid'=>$mid],'id')){
            self::saveData([
                'uid'=>$uid,
                'mid'=>$mid,
                'status'=>1,
                'online'=>time(),
            ]);
        }else{
            self::updates(['id'=>$data['id']],['status'=>1]);
        }
        if(!$exit){
            //将自己 也加入到对方的好友列表中
            self::addUser($mid,$uid,true);
        }
    }

    /**
     * 获取我所有需要展示的用户列表
     * @param $uid
     */
    static function getMyUserList($uid){
        $userList = self::findAll(['uid'=>$uid,'status'=>1],'mid,online');
        if(empty($userList)) return [];
        //更改下标
        $userList = self::setListKey($userList,'mid','online');
        $midJoin = array_keys($userList);
        //获取离线的顾客列表
        $where = [
            ['id','in',$midJoin],
        ];
        $field = "id,role,admin_id,depart_id,staff_id,name,mobile,uid,headimg,extend,online";
        $list = ClientModel::findAll($where,$field);

        foreach ($list as $k=>$v){
            $list[$k]['uuid'] = $v['id'];
        }

        return $list;

    }

        //数据查询
    function getList($param){
        $order = $param['order']?:'id desc';
        $model = $this->order($order);
        $this->getListWhere($model,$param);
        $list = $model->paginate($param['limit']);
        return $list;
    }

    //获取导出数据
    function getExport($param,$fileName='',$type='xlsx'){
        $fileName = $fileName?:'数据表格';
        $fileName.='-'.date('YmdHis');
        //获取数据
        $order = $param['order']?:'id desc';
        $model = $this->order($order);
        $this->getListWhere($model,$param);
        $list = $model->select();
        if(empty($list)){
            return [];
        }
        foreach ($list as $k=>$v){
            if(is_numeric($v->create_time))$list[$k]['create_time'] = $v->create_time_text;
            $list[$k]['status'] = $v->status_text;

        }
        $list = $list->toArray();
        //得到表头
        $top = array_intersect_key(self::$fieldsList,$list[0]);
        //移除部分表头
        $top = array_diff_key($top,array_flip([]));
        //移除多余字段
        $list_new = [];
        foreach ($list as $k=>$v){
            $list_new[] = array_intersect_key($v,$top);
        }
        $list = $list_new;
        return [
            'fileName'=>$fileName,
            'top'=>$top,
            'data'=>$list,
            'type'=>$type,
        ];
    }

    /**
     * 设置列表查询条件
     * @param BaseQuery $model
     * @param array $param
     * @return array
     */
    function getListWhere($model,$param=[]){
        if(empty($param)){
            return [];
        }
        $where = [];
            
        if($param['id']){
            $where['id'] = $param['id'];
        }
    
        if($param['uid']){
            $where['uid'] = $param['uid'];
        }
    
        if($param['mid']){
            $where['mid'] = $param['mid'];
        }
        
        if( $param['create_time']!='' ) {
            $create_time = explode('至',$param['create_time']);
            $date_time = [
                strtotime($create_time[0]),
                strtotime($create_time[1].' 23:59:59'),
            ];
            $model->whereBetween('create_time',$date_time);
        }
    
        if($param['online']){
            $where['online'] = $param['online'];
        }
    
        if($param['status']){
            $where['status'] = $param['status'];
        }


//        //检索查询
        if($param['search_key']){
            $where['id'] = $param['search_key'];
        }
        if($where){
            $model->where($where);
        }
    }

    //表字段别名
    public static $fieldsList = [
            'id'=>'id',
            'uid'=>'用户id',
            'mid'=>'聊天对象id',
            'type'=>'type',

            'create_time'=>'创建时间',
            'online'=>'最后发送消息时间',
            'status'=>'1',
            'is_top'=>'1',//是否置顶
            'is_show'=>'is_show',//是否列表显示
            'remark'=>'remark',//备注
            'quiet'=>'quiet',//是否消息免打扰
    ];

        //表字段状态
        public function getCreateTimeTextAttr($value,$data){
        if(is_numeric($data['create_time'])){
            return $data['create_time']>0 ? date(self::$formatTime,$data['create_time']) : '--';
        }else{
            return $data['create_time'];
        }
    }

    public static $statusList = [
            '1'=>'显示',
            '2'=>'隐藏',
    ];

    //1获取器
    public function getStatusTextAttr($value,$data){
        if(strpos($data['status'],',')){
            $arr = explode(',',$data['status']);
            $arrText = [];
            foreach ($arr as $v){
                $arrText[]=self::$statusList[$v];
            }
            return join(',',$arrText);
        }else{
            return self::$statusList[$data['status']]?:'--';
        }
    }



}
