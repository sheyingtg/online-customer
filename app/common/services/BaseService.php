<?php


namespace app\common\services;


class BaseService
{
    protected static $overdue_time = 600;//支付订单有效期 10分钟
    protected static $payment_key = 'paymentData';//支付参数缓存

    protected static $goto_url = 'https://imserviceh5.hnbjsj.com/#/';//前端域名前缀
    /**
     * Gateway::bindUid(string $client_id, mixed $uid);  将$client_id 和 uid绑定 以便通过Gateway::sendToUid($uid)
     * Gateway::isOnline(string $client_id);  通过$client_id查询是否在线
     * Gateway::isUidOnline(mixed $uid);  通过$uid 查询是否在线
     * Gateway::getClientIdByUid(mixed $uid); 通过uid 拿到对应的所有 client_id
     * Gateway::getUidByClientId(string $client_id); 通过client_id 拿到对应的 uid
     *
     * Gateway::sendToUid(uid,message)   //向uid 绑定的所有在线的client_id 发送数据
     *
     * Gateway::joinGroup(string $client_id, mixed $group); // 将 client_id 加入某个组
     * Gateway::ungroup(mixed $group);  // 解散某个分组
     * Gateway::leaveGroup(string $client_id, mixed $group); // 将某个连接 从某个分组踢出
     * Gateway::sendToGroup(mixed $group, string $message [, array $exclude_client_id = null [, bool $raw = false]]);  给某个分组发消息
     *
     */

    /**
     * 对数据计算签名
     * @param $data
     */
    protected static function getSign($data,$secret){
        ksort($data);
        //数组转字符串
        $string = self::ToUrlParams($data);
        //拼接密钥
        $string = $string .$secret;
        $string = md5($string);
        //转大写
        $result = strtoupper($string);
        return $result;
    }

    /**
     * 格式化参数格式化成url参数
     */
    protected static function ToUrlParams($data)
    {
        $buff = "";
        foreach ($data as $k => $v)
        {
            if($k != "sign" && $v != "" && !is_array($v)){
                $buff .= $k . "=" . $v . "&";
            }
        }

        $buff = trim($buff, "&");
        return $buff;
    }

}