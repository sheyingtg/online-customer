<?php


namespace app\api\controller;


use app\api\services\[文件名]Service;

class [文件名] extends Base
{
    /**
     * 自动生成 showdoc API文档
     * https://www.showdoc.com.cn/page/741656402509783
     */

    [新代码插槽]

    /**
     * [模块名]列表 showdoc
     * @catalog 自动文档/[模块名]
     * @title [模块名] 列表
     * @description [模块名]列表的接口
     * @method get
     * @url 接口域名/api/[文件名]/lists
     * @param page 可选 string 页码
     * @return {"code":1,"msg":"ok","data":{"total":"总条数","per_page":"每页记录数","current_page":"当前页面","last_page":"总页码","data":[{[数据注释]}]}}
[返回字段注释]
     * @remark 这里是备注信息
     * @number 1
     */
    function lists(){
        $params = $this->request->get();
        $data = [文件名]Service::getList($params);
        return json(getJson('ok',1,$data));
    }

    /**
     * [模块名]详情 showdoc
     * @catalog 自动文档/[模块名]
     * @title [模块名] 详情
     * @description [模块名]详情的接口
     * @method get
     * @url 接口域名/api/[文件名]/detail
     * @param id 必选 string 记录ID
     * @return {"code":1,"msg":"ok","data":{[数据注释]}}
[返回字段注释]
     * @remark 这里是备注信息
     * @number 2
     */
    function detail(){
        $params = $this->request->get();
//        $params['uid'] = $this->uid;//获取用户自身数据
        $this->validate($params);//数据效验
        if($data = [文件名]Service::getDetail($params)){
            return json(getJson('ok',1,$data));
        }
        return json(getJson('记录不存在',0,$params));
    }

    /**
     * [模块名]新增数据 showdoc
     * @catalog 自动文档/[模块名]
     * @title [模块名] 新增
     * @description [模块名]新增的接口
     * @method post
     * @url 接口域名/api/[文件名]/add
    [新增参数]
     * @return {"code":1,"msg":"新增成功"}
     * @remark 这里是备注信息
     * @number 3
     */
    function add(){
        $params = $this->request->post();
        $this->validate($params);//数据效验
//        $params['uid'] = $this->uid;//获取用户自身数据
        if($data = [文件名]Service::add($params)){
            return json(getJson('新增成功',1));
        }else{
            return json(getJson('新增失败'));
        }
    }

    /**
     * [模块名]更新 showdoc
     * @catalog 自动文档/[模块名]
     * @title [模块名] 更新
     * @description [模块名]更新的接口
     * @method post
     * @url 接口域名/api/[文件名]/update
     * @param id 必选 string 记录ID
     * @return {"code":1,"msg":"更新成功"}
     * @remark 这里是备注信息
     * @number 4
     */
    function update(){
        $params = $this->request->post();
        $this->validate($params);//数据效验
//        $params['uid'] = $this->uid;//获取用户自身数据
        if($data = [文件名]Service::update($params)){
            return json(getJson('更新成功',1));
        }
        return json(getJson('更新失败'));
    }

    /**
     * [模块名]删除 showdoc
     * @catalog 自动文档/[模块名]
     * @title [模块名] 删除
     * @description [模块名]删除的接口
     * @method post
     * @url 接口域名/api/[文件名]/delete
     * @param id 必选 string 记录ID
     * @return {"code":1,"msg":"删除成功"}
     * @remark 这里是备注信息
     * @number 5
     */
    function delete(){
        $params = $this->request->post();
        $this->validate($params);//数据效验
//        $params['uid'] = $this->uid;//获取用户自身数据
        if($data = [文件名]Service::delete($params)){
            return json(getJson('删除成功',1));
        }
        return json(getJson('删除失败'));
    }



}