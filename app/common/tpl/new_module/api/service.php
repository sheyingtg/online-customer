<?php


namespace app\api\services;


use app\common\exception\ApiException;
use app\common\model\[文件名]Model;
use app\common\services\BaseService;

class [文件名]Service extends BaseService
{

    [新代码插槽]

    /**
     * 获取列表数据
     */
    static function getList($params){
        $model = new [文件名]Model();
        $model = $model->order('id desc');
        //设置查询条件
        self::getListWhere($model,$params);
        //分页查询
//        $list = $model->paginate($params['pageSize']);
        //直接查询
        $list = $model->select();

        [获取器输出]

        return $list;
    }

    /**
     * 获取数据详情
     * @param $params
     */
    static function getDetail($params){
        $model = new [文件名]Model();
        //设置查询条件
        self::getListWhere($model,$params);
        $data = $model->find();

[获取器输出2]

        return $data;
    }

    /**
     * 删除数据
     * @param $params
     */
    static function delete($params){
        $model = new [文件名]Model();
        self::getListWhere($model,$params);
        $data = $model->find();
        if(empty($data)){
            throw new ApiException("记录不存在！");
        }
        $res = $model->delete();

        return $res;
    }

    /**
     * 更新数据
     * @param $params
     */
    static function update($params){
        $where = [
            'id'=>$params['id'],
        ];
        $bannerId = [文件名]Model::findOne($where,'id');
        if(empty($bannerId)){
            throw new ApiException("记录不存在！");
        }
        $res = [文件名]Model::where($where)->update($params);

        return $res;
    }

    /**
     * 新增数据记录
     * @param $params
     */
    static function add($params){
        $model = new [文件名]Model();

        return $model->save($params);
    }

    /**
     * 列表查询条件处理
     * @param [文件名]Model $model
     * @param array $param
     */
    static private function getListWhere(&$model,$params=[]){
        if(empty($params)){
            return $model;
        }
        $where = [];
        if($params['id']){
            $where['id'] = $params['id'];
        }
        if($where){
            $model = $model->where($where);
        }
    }

}