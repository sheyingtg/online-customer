<?php


namespace app\api\validate;


class [文件名] extends Base
{
    protected $rule = [
        [rule字段]
    ];

    protected $message = [
    ];

    //验证场景
    protected $scene = [
        //列表
        'lists'=>[],
        //查看详情
        'detail'=>['id'],
        //更新
        'update'=>['id'],
        //删除
        'delete'=>['id'],
        //新增
        'add'=>[[scene字段]]
    ];
}