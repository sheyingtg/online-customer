<?php
namespace app\admin\controller;

use app\common\controller\AdminBase;
use app\common\model\[文件名]Model;
use app\common\model\AuthRule;
use think\Db;

class [文件名] extends AdminBase
{

    [新代码插槽]


    //首页
    function Index(){
        if($this->request->isAjax()){
            $param = $this->request->param();
            $model = new [文件名]Model();
            $list = $model->getList($param);
            [获取器输出]
            $this->success('ok',"",$list);
        }
        $reData = [
            [状态变量]
        ];
        return view('',$reData);
    }

    //新增or编辑数据
    function edit(){
        if( $this->request->isPost() ) {
            $param = $this->request->param();
            [复选框字段输入]
            $result = [文件名]Model::saveData($param);
            if( $result ) {
                $this->success('操作成功');
            } else {
                $this->error('操作失败');
            }
        }
        $id = $this->request->get('id');
        $copy = $this->request->get('copy');
        $data = [];
        if(!empty($id)){
            $data = [文件名]Model::where('id',$id)->find();
            if($copy==1){
                unset($data['id']);
            }
            [复选框字段输出]
        }
        $reData = [
            'data'=>$data,
            [breadcrumb]
            [状态变量]
        ];
        return view('index_edit',$reData);
    }

    /**
     * 删除节点
     */
    public function delete()
    {
        $id = $this->request->param('id');
        !($id>1) && $this->error('参数错误');
        [文件名]Model::where([['id','in',$id]])->delete();
        $this->success('删除成功');
    }

    //数据导出
    public function exportData(){
        $param = $this->request->param();
        $model = new [文件名]Model();
        $exportData = $model->getExport($param,'[模块名]列表');
        if($exportData){
            $this->success('ok','',$exportData);
        }
        $this->error('没有数据被导出！');
    }
}
