{extend name="main" /}

{block name="body"}
{include file="breadcrumb" /}
<div class="layui-fluid">
    <div class="layui-card">
        <div class=" layui-card-header layuiadmin-card-header-auto">
            {include file="[文件夹名]/index_form"}
        </div>
        <div class="layui-card-body">
            <table class="layui-hide layui-table" id="test"  lay-filter="test"></table>
        </div>
    </div>



<!--html模版列表-->
{include file="[文件夹名]/index_tpl"}

</div>
{/block}
{block name="js"}
<script src="/static/admin/js/layuiTableApi.js"></script>
<script>
        //  <i class="layui-icon layui-icon-edit "></i>  编辑图标
        var table = layui.table,form = layui.form,laytpl  = layui.laytpl ;
        var cols = [列表字段组];
        //表格初始化
        table.render(getLayuiInit('#test','index.html',cols));

        //监听单行工具条
        //监听单行工具条
        table.on('tool(test)',(obj)=>{
            var data = obj.data;
            if(obj.event === 'del'){
                layuiApiDel(data,()=>{obj.del()})
            } else if(obj.event === 'edit'){
                //窗口编辑
                //[ajax_open]('[模块名]编辑','edit.html?id='+data.id,'[width]','[height]')
                //新页面编辑
                //[window.open]('edit.html?id='+data.id,'_self');
            }
        });

        //表格顶部工具栏事件
        table.on('toolbar(test)', (obj)=>{
            var checkStatus = table.checkStatus(obj.config.id); //获取选中行状态
            switch(obj.event){
                //批量删除
                case 'dels':
                    var data = checkStatus.data;  //获取选中行数据
                    if(data.length==0){
                        layer.msg('请选择要删除的数据！',{icon:5});return ;
                    }
                    layuiApiDel(data,()=>{table.reload('test',{})});break;
                case 'export':
                    apiExport();
                    break;
                case 'edit':
                    //窗口编辑
                    //[ajax_open]('[模块名]编辑','edit.html','[width]','[height]')
                    //新页面编辑
                    //[window.open]('edit.html','_self');
                    break;
            };
        });

        //监听单元格编辑
        table.on('edit(test)', OnLayuiEdit);

        //触发排序事件
        table.on('sort(test)', OnLayuiOrder);

        //监听状态操作
        form.on('switch(switchTpl)', OnLayuiSwitch);

        //监听数据提交事件
        form.on('submit(save)', OnLayuiSave);
</script>
{/block}
