<?php


namespace app\home\controller;


use app\common\model\ClientLogModel;
use app\common\model\ClientModel;
use app\common\model\MessageLogModel;
use think\App;

class Api extends Base
{

    function __construct(App $app)
    {
        parent::__construct($app);

    }

    /**
     * 获取用户的消息数
     * https://imservice.hnbjsj.com/home/api/getMessageNumber.html?ticket=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTAwMDAxOSwidXVpZCI6MTAwMDAxOSwiYWRtaW5faWQiOjI0LCJuaWNrbmFtZSI6IuWFu-eLl-WtkCIsImhlYWRpbWciOiJodHRwczpcL1wvdGhpcmR3eC5xbG9nby5jblwvbW1vcGVuXC92aV8zMlwvUTBqNFR3R1RmVExoTTVqbXRUTlZSaWJHMTUxRkkyeFNyUWJLNXZGWjhYMG9IM2hjSzdFYWljOUxoVkp1akQwb2lib0lod2Y1VTBYM2dwZ2hKamE5WmpWQndcLzEzMiIsInVpZCI6IjE1MzMzIiwicm9sZSI6MiwidG9faWQiOjB9.hYVkzkPuNB0BrRBVo-LPsbpv-rIFcooRbRyMLC13hBE
     */
    function getMessageNumber(){
        $where = [
            'to_user_id'=>$this->userInfo['id'],
            'is_reda'=>1,
        ];
        $count = MessageLogModel::findCount($where);
        $to_id = 0;
        $where['from_role'] = 2;
        $from_user_id = MessageLogModel::findValue($where,'from_user_id');
//        dump($from_user_id);exit;
        if($from_user_id){
            $to_id = ClientModel::findValue(['id'=>$from_user_id],'uid')?:0;
        }
       success('ok',[
           'count'=>$count,
           'to_id'=>$to_id
       ]);
    }


}