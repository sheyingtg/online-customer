<?php


namespace app\home\controller;


use app\common\model\ClientModel;
use GatewayWorker\Lib\Gateway;

class Debug extends \app\common\controller\Base
{
    /**
     * 查看所有在线的 连接
     * https://imservice.hnbjsj.com/home/debug/look
     */
    function look(){
        $group_key = "sala_list:24";

        //总在线人数
        $count = Gateway::getClientIdCountByGroup($group_key);
        $List = Gateway::getClientSessionsByGroup($group_key);

        $userList = [];
        foreach ($List as $client=>$v){
            $uid = Gateway::getUidByClientId($client);
            $userList[$uid][] = $client;
        }

        //获取用户的 基本信息
        $field= "id,nickname,mobile,headimg";
        foreach ($userList as $k=>$v){
            $info = ClientModel::findOne(['id'=>$k],$field);
            $userList[$k] = [
                'connect'=>count($v),
                'info'=>$info,
            ];
        }

        $reData = [
            'countAll'=>count($userList),
            'userList'=>$userList,
        ];

        return view('',$reData);

    }

    /**
     * 踢人下线
     */
    function ti(){
        $id = $this->request->param('id');
        $list = Gateway::getClientIdByUid($id);

        if($list){
            Gateway::closeClient($list[0]);
            unset($list[0]);
        }

        $this->success('ok','',count($list));
    }
}