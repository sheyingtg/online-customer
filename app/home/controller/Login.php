<?php


namespace app\home\controller;


use app\common\model\Admin;
use app\common\model\ClientModel;
use app\common\model\StaffModel;
use think\exception\ValidateException;
use think\facade\Cookie;
use think\facade\Session;
use utils\Jwt;

//use think\facade\Cookie;

class Login extends Base
{

    //客服登录界面
    function Index(){
        $this->redirect('https://imserviceh5.hnbjsj.com/#/');

        if($this->request->isAjax()){
            //登录请求
            $param = $this->request->param();
            try {
                $this->validate($param,'Login.Login');
            } catch (ValidateException $e) {
                $this->error($e->getError());
            }

            //是否开启验证码
            if( xn_cfg('base.login_vercode') == 1 ) {
                if( !captcha_check($param['vercode']) ) {
                    $this->error('验证码错误');
                }
            }
            $info = StaffModel::login($param);
            if(!$info){
                $this->error('登录失败！');
            }

            $staffInfo = StaffModel::getInfo($info['id']);
            if($staffInfo){
                //创建客服的信息
                $info = ClientModel::createInfo2($staffInfo);
                $ticket = Jwt::getToken($info);
                Session::set('ticket', $ticket);
                $param['ticket'] = $ticket;
                $this->redirect(url('main/index',$param));
            }else{
                $this->error('快捷登陆失败');
            }

            //保存登陆信息
//            StaffModel::setTicket($info);
            //存储登录过的账户
            cookie('login_username',$param['username']);
            $this->success('登录成功','',['ticket'=> Session::get('ticket')]);
        }
        $data = [
            'username'=>Cookie::get('login_username'),
        ];

        return view('',$data);
    }

    //快捷自动登录
    function autoLogin(){
        $param = $this->request->param();

        $ticket = $param['ticket'];
        if(empty($ticket)){
            $this->error('缺少参数');
        }
        $verify = Jwt::verifyToken($ticket);
        $staffId = $verify['userid'];
        if(empty($staffId)){
            $this->error('参数错误');
        }
        $staffInfo = StaffModel::getInfo($staffId);
        if($staffInfo){
            //创建客服的信息
            $info = ClientModel::createInfo2($staffInfo);
            $ticket = Jwt::getToken($info);
            Session::set('ticket', $ticket);
            $param['ticket'] = $ticket;
            $this->redirect(url('main/index',$param));
        }else{
            $this->error('快捷登陆失败');
        }
    }

    /**
     * 客户进入 介入登录
     * http://www.imservice.cn/home/login/AccessLogin?secret=123456&userid=123&depart_id=1&staff_id=1&username=兔白白
     * http://imservice.xl.cn/home/login/AccessLogin?secret=123456&userid=123&depart_id=1&staff_id=1&username=兔白白
     */
    function AccessLogin(){
        $param = $this->request->param();
        $to_id = $param['to_id']?:0;//连线非客服人员
//        dump($to_id);exit;
        if($param['ticket']){
            $info = Jwt::verifyToken($param['ticket']);
            $info['to_id'] = $to_id;
            $ticket = Jwt::getToken($info);
        }else{
            //发起登陆
            try{
                $this->validate($param,'Login.AccessLogin');//数据效验
            }catch (\Exception $e){
                $this->error($e->getMessage(),$_SERVER['HTTP_REFERER']);
            }
            //解析密钥正确性
            $admin_id = Admin::findValue([
                'secret'=>$param['secret'],
                'status'=>1,
            ],'id');
            if(empty($admin_id)){
                $this->error('密钥有误，请联系开发者！',$_SERVER['HTTP_REFERER']);
            }
            //创建客户的信息
            $info = ClientModel::createInfo($param,$admin_id);
            $info['to_id'] = $to_id;
            $ticket = Jwt::getToken($info);
        }

        if($this->request->isAjax() || $param['ajax'] == 1){
            success('ok',['ticket'=>$ticket]);
        }else{
            return view('main/index',['ticket'=>$ticket,'userinfo'=>$info]);
        }
    }


}