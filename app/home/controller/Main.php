<?php


namespace app\home\controller;


use app\admin\controller\CommonDis;
use app\common\model\ClientModel;
use app\common\model\CommonDisModel;
use app\common\model\MessageLogModel;
use app\common\model\StaffModel;
use app\common\model\UploadFiles;
use app\common\services\RedisService;
use app\socket\services\UserInfoService;
use think\facade\Db;
use think\facade\Filesystem;
use think\facade\Request;
use think\facade\Session;

class Main extends Base
{

    //聊天界面
    function Index(){
        if($this->userInfo['role']!=1){
            $this->error('错误的访问，请先登录！',url('Login/index'));
        }
        if(request()->isAjax()){
            //获取初始化数据
            $where = [
                'admin_id'=>$this->userInfo['admin_id'],
            ];
            $reData = [
                'commit_dis'=>CommonDisModel::findAll($where,"id,msg",'order_desc desc,id desc'),
            ];
            $this->success('ok','',$reData);
        }else{
            //获取更多的客服信息
            $_userinfo = StaffModel::getInfo($this->userInfo['uid']);
            $userinfo = StaffModel::handleUserInfo($_userinfo);
            $userinfo['uuid'] = $this->userInfo['uuid'];
            return view('',['ticket'=>Session::get('ticket'),'userinfo'=>$userinfo]);
        }
    }

    /**
     * 获取历史聊天记录
     */
    function getHistStart(){

        $to_user_id = input('to_user_id');//对方id
        $to_role = input('to_role');
        $from_user_id = input('from_user_id');// 我方id

        if($to_role == 1 or $this->userInfo['role']==1){
            //双方 有一人是客服时  获取对方 跟 所有客服的聊天信息
            //得到所有的客服
            $kefuId = join(',',ClientModel::getKefuIds($this->userInfo['admin_id']));
            $to_user_id = $to_role == 1 ?$from_user_id :$to_user_id;
            $where = "  (to_user_id = {$to_user_id} and from_role = 1) or (to_user_id in ({$kefuId}) and from_user_id = {$to_user_id}) ";
        }else{
            $where = "  (to_user_id = {$to_user_id} and from_user_id = {$from_user_id}) or (to_user_id = {$from_user_id} and from_user_id = {$to_user_id}) ";
        }
//        dump($where);exit;
        $param = [
            'where'=>$where,
            'order'=>'id desc',
        ];
        $model = new MessageLogModel();
        $list = $model->getList($param);
//        foreach ($list as $k=>$v){
//            $list[$k]['datetime'] = $v->datetime_text;
//        }

        $list = json_decode(json_encode($list),1);



        //倒序排列
        usort($list['data'], function($a, $b) {
            $aid = $a['id'];
            $bid = $b['id'];
            return ($aid < $bid) ? -1 : 1;
        });
//        dump($list);exit;
        $this->success('ok','',$list);
//        dump($this->userInfo);
    }

    //ajax请求 获取 基本信息
    function getInit(){
        $data = [
            //当前用户的基本信息
            'userinfo'=>StaffModel::handleUserInfo($this->userInfo),
        ];
        $this->success('ok','',$data);
    }


    //聊天窗口
    function msgView(){
        return view('',['ticket'=>Session::get('ticket')]);
    }


}