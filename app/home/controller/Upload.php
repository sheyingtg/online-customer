<?php


namespace app\home\controller;


use app\common\model\UploadFiles;
use think\facade\Filesystem;
use think\facade\Request;

class Upload extends Base
{



    /**
     * ajax 请求 上传图片
     */
    function Image(){
        $folder_name = $this->request->param('fileUpload/s','file');
        $result = UploadFiles::upload($folder_name);
        if($result['code']==1){
            $file =comple_host( $result['file']);
            $data = ['picCover'=>$file,'fileName'=>''];
            $this->success('上传成功','',$data);
        }
        $this->error('上传失败');
    }

    // ajax 请求 文件上传
    function File(){
        $file = Request::file('file');
        //文件校验
        $fileSize = 1024 * 1024 * 1024 * 10; // 10M   ,fileExt:gif,jpg,png
        $result=validate(['file' => ['fileSize:'.$fileSize]])->check(['file' => $file]);
        if($result){
            //上传到服务器
            $path = Filesystem::disk('public')->putFile('uploadFiles',$file);
            //获取图片路径
            //获取绝对路径
            $picCover = comple_host($path);
            //原始文件名
            $fileName = $file->getOriginalName();
            $data = ['picCover'=>$picCover,'fileName'=>$fileName];
            $this->success('上传成功','',$data);
        }
    }

}