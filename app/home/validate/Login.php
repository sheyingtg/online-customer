<?php


namespace app\home\validate;


use think\Validate;

class Login extends Validate
{
    protected $rule = [
        'secret' => 'require',//公司密钥
        'depart_id' => 'number',//所属部门
        'staff_id' => 'number',//专属客服
        'userid'=>'require',//平台id


        'username' => 'require',
        'password' => 'require',


    ];

    protected $message = [
        'userid.require'=>'请先登录!',
    ];

    protected $scene = [
        'Login'=>['username','password'],
        'AccessLogin'=>[
            'userid',
            'depart_id',
            'staff_id',
            'secret',
        ],
    ];
}