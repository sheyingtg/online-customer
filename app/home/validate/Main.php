<?php


namespace app\home\validate;


use think\Validate;

class Main extends Validate
{
    protected $rule = [
        'id|顾客id' => 'require',
        'newMobile|手机号' => 'require|mobile',
    ];

    protected $message = [];

    protected $scene = [
        'upMobile'=>['id','newMobile'],

    ];
}