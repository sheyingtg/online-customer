<?php


namespace app\socket\controller;


use app\common\model\DebugModel;

class Base
{
    public function __construct(){
        DebugModel::addInfo();
    }
}