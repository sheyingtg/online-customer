<?php


namespace app\socket\controller;

use GatewayWorker\BusinessWorker;
use GatewayWorker\Gateway;
use GatewayWorker\Register;
use Workerman\Worker;

class Config
{
    /**
     * 构造函数
     * Config constructor.
     */
    public function __construct(){

        //初始化各个GatewayWorker
        //初始化register
        // register 服务必须是text协议
        $register = new Register('text://0.0.0.0:1237');

        //初始化 bussinessWorker 进程
        $worker = new BusinessWorker();
        $worker->name = 'IMServiceBusinessWorker';
        //启动4个进程
        $worker->count = 4;
        // 服务注册地址
        $worker->registerAddress = '127.0.0.1:1237';

        //设置处理业务的类,此处制定Events的命名空间
        $worker->eventHandler = '\app\socket\controller\Events';

        // 初始化 gateway 进程
        $gateway = new Gateway("websocket://0.0.0.0:9092");
        // 设置名称，方便status时查看
        $gateway->name = 'IMServiceGateway';
        // 设置进程数，gateway进程数建议与cpu核数相同
        $gateway->count = 4;
        // 分布式部署时请设置成内网ip（非127.0.0.1）
        $gateway->lanIp = '127.0.0.1';
        // 内部通讯起始端口。假如$gateway->count=4，起始端口为5900
        // 则一般会使用5900 5901 5902 5903 4个端口作为内部通讯端口
        $gateway->startPort = 5900;
        // 心跳间隔
        $gateway->pingInterval = 20;
        // 1 客户端必须要反应  0 不需要
        $gateway->pingNotResponseLimit = 1;
        // 心跳数据
        $gateway->pingData = '{"type":"ping"}';
        // 服务注册地址
        $gateway->registerAddress = '127.0.0.1:1237';

        //运行所有Worker;
        Worker::runAll();
    }
}