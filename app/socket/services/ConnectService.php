<?php


namespace app\socket\services;


class ConnectService extends BaseService
{

    //所有的连接列表
    private $ConnectList = [];
    //总连接数量
    private $connectNumber = 0;

    private static $instance = null;

    public static function getInstance()
    {
        if(null === self::$instance){
            self::$instance = new self();
        }
        return self::$instance;
    }

    //向列表中添加一个连接
    public function addConnect($conn){
        $this->ConnectList[] = $conn;
    }

    //防止使用new 创建多个实例
    public function __construct()
    {
    }
}