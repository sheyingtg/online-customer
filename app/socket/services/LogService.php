<?php


namespace app\socket\services;


class LogService
{

    /**
     * 写出日志
     * @param $title
     * @param $content
     */
    static function save($title,$content,$file=''){
        //文件绝对路径
//        $path = APP_SOCKET_PATH."/log/".date('Y/m/');
        $path = __DIR__."/../log/".date('Y/m/');
        if(empty($file)){
            $file = date('d').'.txt';
        }
        //快速创建文件
        if(!file_exists($path)) self::mkdirs($path);

        if(is_array($content)){
            $content = json_encode($content,'256');
        }

        $old_content = @file_get_contents($path.$file);

        $put_text = $title.'    ----'.date('Y-m-d H:i:s')."\r\n";
        $put_text.=$content."\r\n";

        //保存日志
        file_put_contents($path.$file,$put_text.$old_content);
    }

    // 首次 清理日志
    static function clear($file=''){
        $path = __DIR__."/../log/".date('Y/m/');
        if(empty($file)){
            $file = date('d').'.txt';
        }
        if(file_exists($path.$file)) unlink ($path.$file);

    }

    //循环创建目录
    static function mkdirs($dir, $mode = 0777)
    {
        if (!is_dir($dir)) {
            self::mkdirs(dirname($dir), $mode);
            return mkdir($dir, $mode);
        }
        return true;
    }
}