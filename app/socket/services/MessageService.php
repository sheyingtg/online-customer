<?php


namespace app\socket\services;


use app\api\services\UserListService;
use app\common\model\MessageLogModel;
use GatewayWorker\Lib\Gateway;

class MessageService extends BaseService
{

    /**
     * 给某某发送消息
     * @param $message_data
     * @param $client_id
     */
    static function say($message_data,$client_id){

    }

    /**
     * 消息已读 更新数据库消息状态
     */
    static function upRedaMsg($message_data,$client_id){
        $params = [
            'uid'=>Gateway::getUidByClientId($client_id),
            'mid'=>$message_data['mid'],
        ];
        UserListService::setRead($params);
    }

}