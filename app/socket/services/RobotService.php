<?php

/**
 * 辅助工作的回复机器人
 */

namespace app\socket\services;


use think\facade\Cache;

class RobotService extends BaseService
{

    //发送消息的身份信息
    private static $userInfo = [
        'from_user_id'=>0,
        'from_user_nickname'=>'机器人客服',
        'from_user_headimg'=>'/static/head/robot.png',
        'from_role'=>1,
    ];
    // {from_user_id:1,from_user_nickname:'用户一',from_user_headimg:'/static/head/1.jpg',to_user_id:3,from_role:1,msg:'在吗，找你有事',datetime:'2021-5-1 10:12:01'},
    /**
     * 新用户首次进入 推送欢迎语句
     * 10 分钟内 同一个用户只发送一次
     */
    static function UserFirstWelcome(){
        $hasKey = "UserFirstWelcome:{$_SESSION['userinfo']['id']}";
        if(Cache::has($hasKey)){
            return ;
        }
        Cache::set($hasKey,1,600);
//        $time = time();
        //10分钟内 只发送一次
//        if($time - $_SESSION['UserFirstWelcome_time']< 600){
//            return false;
//        }
        //获取机器人信息
        $data = self::$userInfo;
        $data['type'] = 'say';//消息类型
        $nickname = isset($_SESSION['userinfo']['name'])? $_SESSION['userinfo']['name'] .'，' : '';
        $data['msg'] = $nickname. '您好，请问有什么可以帮您~';//  . mt_rand(11,99)
        $data['to_user_id'] = $_SESSION['userinfo']['uuid'];
        $data['datetime'] = mdate2(time()-1);


        //发送时间消息
        self::sendTime();
        //发送消息
        self::success($data);
    }

    //发送一条时间消息
    static function sendTime(){
//        $uid = self::$kehuRole.$_SESSION['userinfo']['id'];
        $data = self::$userInfo;
        $data['type'] = 'system';
        $data['msg'] = 'time';
        $data['datetime'] = date('Y-m-d H:i:s');
        self::success($data);

    }

}