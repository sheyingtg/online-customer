<?php

use \Workerman\Worker;
use \GatewayWorker\BusinessWorker;

//自动加载类
require __DIR__ . '/../../../vendor/autoload.php';
//require_once __DIR__ . '/../autoload.php';


// bussinessWorker 进程
$worker = new BusinessWorker();
// worker名称
$worker->name = 'IMServiceBusinessWorker';
// bussinessWorker进程数量
$worker->count = 4;
// 服务注册地址
$worker->registerAddress = '127.0.0.1:1237';

//设置处理业务的类,此处制定Events的命名空间
//$worker->eventHandler = '\app\socket\controller\Events';
$worker->eventHandler = '\app\socket\controller\Events2';
//$worker->eventHandler = '\app\socket\controller\Websocket';

// 如果不是在根目录启动，则运行runAll方法
if(!defined('GLOBAL_START'))
{
    Worker::runAll();
}