<?php


use \Workerman\Worker;
use \GatewayWorker\Gateway;

//自动加载类
require __DIR__ . '/../../../vendor/autoload.php';
//require_once __DIR__ . '/../autoload.php';

// gateway 进程
$gateway = new Gateway("Websocket://0.0.0.0:9092");
// 设置名称，方便status时查看
$gateway->name = 'IMServiceGateway';
// 设置进程数，gateway进程数建议与cpu核数相同
$gateway->count = 4;
// 分布式部署时请设置成内网ip（非127.0.0.1）
$gateway->lanIp = '127.0.0.1';
// 内部通讯起始端口。假如$gateway->count=4，起始端口为2300
// 则一般会使用2300 2301 2302 2303 4个端口作为内部通讯端口
$gateway->startPort = 5900;
// 心跳间隔
$gateway->pingInterval = 20;
// 1 客户端必须要反应  0 不需要
$gateway->pingNotResponseLimit = 1;
// 心跳数据
$gateway->pingData = '{"type":"ping"}';
// 服务注册地址
$gateway->registerAddress = '127.0.0.1:1237';

// 如果不是在根目录启动，则运行runAll方法
if(!defined('GLOBAL_START'))
{
    Worker::runAll();
}
