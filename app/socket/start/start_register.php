<?php


use \Workerman\Worker;
use \GatewayWorker\Register;
ini_set('display_errors', 'on');

require __DIR__ . '/../../../vendor/autoload.php';
//require_once __DIR__ . '/../autoload.php';


//设置当前socket根目录
if (!defined('PHPEXCEL_ROOT')) define('APP_SOCKET_PATH', __DIR__.'/../../');


// register 服务必须是text协议
$register = new Register('text://0.0.0.0:1237');

// 如果不是在根目录启动，则运行runAll方法
if(!defined('GLOBAL_START'))
{
    Worker::runAll();
}