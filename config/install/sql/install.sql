/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : imser_debug

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 14/03/2022 12:54:01
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tu_admin
-- ----------------------------
DROP TABLE IF EXISTS `tu_admin`;
CREATE TABLE `tu_admin`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户名',
  `password` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '登录密码；mb_password加密',
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户头像，相对于upload/avatar目录',
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '登录邮箱',
  `email_code` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '激活码',
  `phone` bigint(11) UNSIGNED NULL DEFAULT NULL COMMENT '手机号',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '用户状态 0：禁用； 1：正常',
  `register_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '注册时间',
  `last_login_ip` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '最后登录ip',
  `last_login_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '最后登录时间',
  `remark` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `secret` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密钥',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_login_key`(`username`) USING BTREE,
  INDEX `secret`(`secret`) USING BTREE COMMENT '密钥'
) ENGINE = MyISAM AUTO_INCREMENT = 25 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tu_admin
-- ----------------------------
INSERT INTO `tu_admin` VALUES (1, 'admin', 'a4aa017b9193bb869d526a16fbc69a63', '/uploads/20200318/137b548c5ece2e8efc3dfa54c0b3f7e1.png', '1020857701@qq.com', NULL, 17373143155, 1, 1625274838, '', 0, '', '123456');
INSERT INTO `tu_admin` VALUES (20, 'test', '24026eee8538c996f677ee775d316013', '', '', NULL, NULL, 1, 0, '', 0, NULL, NULL);

-- ----------------------------
-- Table structure for tu_auth_group
-- ----------------------------
DROP TABLE IF EXISTS `tu_auth_group`;
CREATE TABLE `tu_auth_group`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `rules` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '规则id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户组表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tu_auth_group
-- ----------------------------
INSERT INTO `tu_auth_group` VALUES (1, '超级管理员', 1, '1,1,2,3,4,5,6,7,8,9,10,11,12,13,14,14');
INSERT INTO `tu_auth_group` VALUES (2, '一般管理员', 1, '14,14,15,15,16,17,18,18,19,20,20');
INSERT INTO `tu_auth_group` VALUES (7, 'IM运营', 1, '14,14,15,15,10,11,12,48,40,40,41,42,46,43,43,45,44,32,32,33,34,37,37,38,39,47');

-- ----------------------------
-- Table structure for tu_auth_group_access
-- ----------------------------
DROP TABLE IF EXISTS `tu_auth_group_access`;
CREATE TABLE `tu_auth_group_access`  (
  `admin_id` int(11) UNSIGNED NOT NULL COMMENT '用户id',
  `group_id` int(11) UNSIGNED NOT NULL COMMENT '用户组id',
  UNIQUE INDEX `uid_group_id`(`admin_id`, `group_id`) USING BTREE,
  INDEX `uid`(`admin_id`) USING BTREE,
  INDEX `group_id`(`group_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户组明细表' ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of tu_auth_group_access
-- ----------------------------
INSERT INTO `tu_auth_group_access` VALUES (1, 1);
INSERT INTO `tu_auth_group_access` VALUES (20, 2);
INSERT INTO `tu_auth_group_access` VALUES (24, 7);

-- ----------------------------
-- Table structure for tu_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `tu_auth_rule`;
CREATE TABLE `tu_auth_rule`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pid` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '父级id',
  `name` char(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '规则唯一标识',
  `title` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '规则中文名称',
  `status` tinyint(1) NULL DEFAULT 1 COMMENT '状态：为1正常，为0禁用',
  `is_menu` tinyint(1) UNSIGNED NULL DEFAULT 0 COMMENT '菜单显示',
  `condition` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '规则表达式，为空表示存在就验证，不为空表示按照条件验证',
  `type` tinyint(1) NULL DEFAULT 1,
  `sort` int(5) NULL DEFAULT 999,
  `icon` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `z_index` int(11) NOT NULL DEFAULT 0 COMMENT '菜单位置 0 左侧  1 顶部(只有一级菜单 才能开启顶部展示)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 56 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '规则表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tu_auth_rule
-- ----------------------------
INSERT INTO `tu_auth_rule` VALUES (1, 15, 'admin/auth/index', '权限控制', 1, 1, '', 1, 50, 'layui-icon-auz', 0);
INSERT INTO `tu_auth_rule` VALUES (2, 1, 'admin/auth/index', '权限管理', 1, 1, '', 1, 1, NULL, 0);
INSERT INTO `tu_auth_rule` VALUES (3, 2, 'admin/auth/add', '添加', 1, 0, '', 1, 2, NULL, 0);
INSERT INTO `tu_auth_rule` VALUES (4, 2, 'admin/auth/edit', '编辑', 1, 0, '', 1, 3, NULL, 0);
INSERT INTO `tu_auth_rule` VALUES (5, 2, 'admin/auth/delete', '删除', 1, 0, '', 1, 4, NULL, 0);
INSERT INTO `tu_auth_rule` VALUES (6, 1, 'admin/AuthGroup/index', '用户组管理', 1, 1, '', 1, 999, NULL, 0);
INSERT INTO `tu_auth_rule` VALUES (7, 6, 'admin/AuthGroup/add', '添加', 1, 0, '', 1, 999, NULL, 0);
INSERT INTO `tu_auth_rule` VALUES (8, 6, 'admin/AuthGroup/edit', '编辑', 1, 0, '', 1, 999, NULL, 0);
INSERT INTO `tu_auth_rule` VALUES (9, 6, 'admin/AuthGroup/delete', '删除', 1, 0, '', 1, 39, NULL, 0);
INSERT INTO `tu_auth_rule` VALUES (10, 1, 'admin/admin/index', '后台管理员', 1, 1, '', 1, 3, '', 0);
INSERT INTO `tu_auth_rule` VALUES (11, 10, 'admin/admin/add', '添加', 1, 0, '', 1, 999, NULL, 0);
INSERT INTO `tu_auth_rule` VALUES (12, 10, 'admin/admin/edit', '编辑', 1, 0, '', 1, 999, NULL, 0);
INSERT INTO `tu_auth_rule` VALUES (13, 10, 'admin/admin/delete', '删除', 1, 0, '', 1, 999, NULL, 0);
INSERT INTO `tu_auth_rule` VALUES (14, 0, 'admin/index/home', '管理首页', 1, 1, '', 1, 1, 'layui-icon-home', 0);
INSERT INTO `tu_auth_rule` VALUES (15, 0, 'admin/config/base', '系统管理', 1, 1, '', 1, 999, 'layui-icon-set', 0);
INSERT INTO `tu_auth_rule` VALUES (16, 21, 'admin/config/base', '基本设置', 1, 1, '', 1, 999, '', 0);
INSERT INTO `tu_auth_rule` VALUES (17, 21, 'admin/config/upload', '上传配置', 1, 1, '', 1, 999, '', 0);
INSERT INTO `tu_auth_rule` VALUES (18, 0, 'admin/upload_files/index', '上传管理', 1, 1, '', 1, 40, 'layui-icon-picture', 0);
INSERT INTO `tu_auth_rule` VALUES (19, 18, 'admin/upload_files/delete', '删除文件', 1, 0, '', 1, 999, '', 0);
INSERT INTO `tu_auth_rule` VALUES (20, 0, 'admin/test/index', '使用示例', 1, 0, '', 1, 999, 'layui-icon-face-surprised', 0);
INSERT INTO `tu_auth_rule` VALUES (21, 15, '', '配置管理', 1, 1, '', 1, 999, '', 0);
INSERT INTO `tu_auth_rule` VALUES (22, 15, 'admin/webLog/index', '日志管理', 1, 1, '', 1, 999, '', 0);
INSERT INTO `tu_auth_rule` VALUES (23, 15, 'admin/NewModule/index', 'CURD生成', 1, 1, '', 1, 999, '', 0);
INSERT INTO `tu_auth_rule` VALUES (24, 21, 'admin/config/payment', '支付配置', 1, 1, '', 1, 999, '', 0);
INSERT INTO `tu_auth_rule` VALUES (25, 0, 'admin/Demo/index', 'curd功能演示demo', 1, 0, '', 1, 999, '', 0);
INSERT INTO `tu_auth_rule` VALUES (41, 40, 'admin/Staff/edit', '编辑', 1, 0, '', 1, 999, '', 0);
INSERT INTO `tu_auth_rule` VALUES (40, 0, 'admin/Staff/index', '员工列表', 1, 0, '', 1, 999, '', 0);
INSERT INTO `tu_auth_rule` VALUES (45, 43, 'admin/MessageLog/delete', '删除', 1, 0, '', 1, 999, '', 0);
INSERT INTO `tu_auth_rule` VALUES (44, 43, 'admin/MessageLog/edit', '编辑', 1, 0, '', 1, 999, '', 0);
INSERT INTO `tu_auth_rule` VALUES (43, 0, '/curd/page/message_log', '消息列表', 1, 1, '', 1, 999, '', 0);
INSERT INTO `tu_auth_rule` VALUES (32, 0, 'admin/Department/index', '部门列表', 1, 1, '', 1, 999, '', 0);
INSERT INTO `tu_auth_rule` VALUES (33, 32, 'admin/Department/edit', '编辑', 1, 0, '', 1, 999, '', 0);
INSERT INTO `tu_auth_rule` VALUES (34, 32, 'admin/Department/delete', '删除', 1, 0, '', 1, 999, '', 0);
INSERT INTO `tu_auth_rule` VALUES (35, 25, 'admin/Demo/edit', '编辑', 1, 0, '', 1, 999, '', 0);
INSERT INTO `tu_auth_rule` VALUES (36, 25, 'admin/Demo/delete', '删除', 1, 0, '', 1, 999, '', 0);
INSERT INTO `tu_auth_rule` VALUES (37, 0, '/curd/page/client', '客户列表', 1, 1, '', 1, 999, '', 0);
INSERT INTO `tu_auth_rule` VALUES (38, 37, 'admin/Client/edit', '编辑', 1, 0, '', 1, 999, '', 0);
INSERT INTO `tu_auth_rule` VALUES (39, 37, 'admin/Client/delete', '删除', 1, 0, '', 1, 999, '', 0);
INSERT INTO `tu_auth_rule` VALUES (42, 40, 'admin/Staff/delete', '删除', 1, 0, '', 1, 999, '', 0);
INSERT INTO `tu_auth_rule` VALUES (46, 40, 'admin/Staff/autoLogin', '快捷登录', 1, 0, '', 1, 999, '', 0);
INSERT INTO `tu_auth_rule` VALUES (47, 37, 'admin/Client/autoLogin', '快捷登录', 1, 0, '', 1, 999, '', 0);
INSERT INTO `tu_auth_rule` VALUES (48, 10, 'admin/admin/info', '查看', 1, 0, '', 1, 999, '', 0);
INSERT INTO `tu_auth_rule` VALUES (49, 0, '/curd/page/user_group', '群组列表', 1, 1, '', 1, 999, '', 0);
INSERT INTO `tu_auth_rule` VALUES (50, 49, 'admin/UserGroup/edit', '编辑', 1, 0, '', 1, 999, '', 0);
INSERT INTO `tu_auth_rule` VALUES (51, 49, 'admin/UserGroup/delete', '删除', 1, 0, '', 1, 999, '', 0);
INSERT INTO `tu_auth_rule` VALUES (52, 0, 'admin/NewFriends/index', '新的好友申请记录', 1, 1, '', 1, 999, '', 0);
INSERT INTO `tu_auth_rule` VALUES (53, 52, 'admin/NewFriends/edit', '编辑', 1, 0, '', 1, 999, '', 0);
INSERT INTO `tu_auth_rule` VALUES (54, 52, 'admin/NewFriends/delete', '删除', 1, 0, '', 1, 999, '', 0);
INSERT INTO `tu_auth_rule` VALUES (55, 0, '/curd/page/staff', '客服列表', 1, 1, '', 1, 999, '', 0);

-- ----------------------------
-- Table structure for tu_client
-- ----------------------------
DROP TABLE IF EXISTS `tu_client`;
CREATE TABLE `tu_client`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` int(1) NULL DEFAULT 2 COMMENT '平台是1 顾客是2',
  `wxpusher` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'wxpusher 推送uid',
  `admin_id` int(11) NULL DEFAULT 0 COMMENT '所属公司',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户姓名 真实姓名',
  `mobile` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户手机号',
  `password` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '登录密码',
  `uid` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户平台id',
  `headimg` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户头像id',
  `nickname` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户昵称',
  `sex` int(1) NULL DEFAULT 0 COMMENT '性别 未知 0  男 1 女 2',
  `signature` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '个性签名',
  `area` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '地区',
  `from` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '来源',
  `desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `extend` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '客户扩展信息',
  `create_time` int(11) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) NULL DEFAULT NULL COMMENT '更新时间',
  `online` int(11) NULL DEFAULT 0 COMMENT '最后活动时间 1 在线 0 离线',
  `auto_pass` int(1) NULL DEFAULT 1 COMMENT '是否自动通过好友  1 自动通过 0 不自动',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 100000 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '接入客户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tu_client_log
-- ----------------------------
DROP TABLE IF EXISTS `tu_client_log`;
CREATE TABLE `tu_client_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NULL DEFAULT 0 COMMENT '客户id',
  `depart_id` int(11) NULL DEFAULT 0 COMMENT '部门id',
  `ip` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '来访ip',
  `ip_text` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'ip所在地',
  `staff_id` int(11) NULL DEFAULT 0 COMMENT '接待人id',
  `source` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '来源网址',
  `extend` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL COMMENT '客户附加信息',
  `header` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '客户请求头',
  `os` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '客户请求',
  `create_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '客户访问日志' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tu_client_sign
-- ----------------------------
DROP TABLE IF EXISTS `tu_client_sign`;
CREATE TABLE `tu_client_sign`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `ticket` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '凭证',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `past_time` int(11) NULL DEFAULT NULL COMMENT '过期时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '客户端登录记录表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tu_common_dis
-- ----------------------------
DROP TABLE IF EXISTS `tu_common_dis`;
CREATE TABLE `tu_common_dis`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) NULL DEFAULT NULL COMMENT '所属公司',
  `msg` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '常用语',
  `uid` int(11) NULL DEFAULT NULL COMMENT '操作人id',
  `order_desc` int(1) NULL DEFAULT 50 COMMENT '排序',
  `create_time` int(11) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '常用语记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tu_common_dis
-- ----------------------------
INSERT INTO `tu_common_dis` VALUES (1, 1, '您好，有什么可能帮您吗？', 1, 50, 1, 0);

-- ----------------------------
-- Table structure for tu_debug
-- ----------------------------
DROP TABLE IF EXISTS `tu_debug`;
CREATE TABLE `tu_debug`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NULL DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `params` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `createtime` datetime(0) NULL DEFAULT NULL,
  `ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `header` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '调试日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tu_demo
-- ----------------------------
DROP TABLE IF EXISTS `tu_demo`;
CREATE TABLE `tu_demo`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `img` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片',
  `tag` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '多选标签 1 美女 2 最热',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `status` int(11) NULL DEFAULT NULL COMMENT '是否显示 1 显示 2 隐藏',
  `display_order` int(11) NULL DEFAULT NULL COMMENT '排序',
  `create_time` int(11) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'curd功能演示demo' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tu_demo
-- ----------------------------
INSERT INTO `tu_demo` VALUES (1, '/uploads/20200318/137b548c5ece2e8efc3dfa54c0b3f7e1.png', '1,2', '测试', 1, 0, 1622711639, 1622711647);
INSERT INTO `tu_demo` VALUES (2, '/uploads/20200318/137b548c5ece2e8efc3dfa54c0b3f7e1.png', '2', '测试', 0, 1, 1622711968, 1622711968);

-- ----------------------------
-- Table structure for tu_department
-- ----------------------------
DROP TABLE IF EXISTS `tu_department`;
CREATE TABLE `tu_department`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `admin_id` int(11) NULL DEFAULT 0 COMMENT '公司id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门名',
  `number` int(11) NULL DEFAULT 0 COMMENT '部门人数',
  `status` int(1) NULL DEFAULT 1 COMMENT '部门状态 1 启用 2禁用',
  `priority` int(1) NULL DEFAULT 50 COMMENT '优先级 越大越优先',
  `create_time` int(11) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '部门名称' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tu_department
-- ----------------------------
INSERT INTO `tu_department` VALUES (1, 1, '小帮手客服', 1, 1, 99, 1626138513, 1626138762);
INSERT INTO `tu_department` VALUES (2, 1, '设计师客服', 2, 1, 50, 1626138620, 1626138753);
INSERT INTO `tu_department` VALUES (7, 24, '小帮手客服', 2, 1, 50, 1627713998, 1627713998);
INSERT INTO `tu_department` VALUES (8, 24, '专家咨询', 1, 1, 0, 1628151776, 1628151776);
INSERT INTO `tu_department` VALUES (9, 24, '法务咨询', 0, 1, 0, 1628151786, 1628151786);
INSERT INTO `tu_department` VALUES (10, 24, '工程托管', 0, 1, 0, 1628157932, 1628157932);

-- ----------------------------
-- Table structure for tu_message_log
-- ----------------------------
DROP TABLE IF EXISTS `tu_message_log`;
CREATE TABLE `tu_message_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) NULL DEFAULT NULL COMMENT '所属平台id',
  `is_group` int(11) NULL DEFAULT 0 COMMENT '是否是群组消息 1 是  0 不是',
  `type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '消息类型 1 文字 2 图片  3 系统消息  4 仅自身可见  5 系统消息，不补充人名  6 名片消息  7 文件',
  `from_user_id` int(11) NULL DEFAULT NULL COMMENT '发送人id',
  `from_user_nickname` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发送人昵称',
  `from_user_headimg` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发送人头像',
  `from_role` int(1) NULL DEFAULT NULL COMMENT '发送人的级别 1 客服 2 顾客',
  `to_user_id` int(11) NULL DEFAULT NULL COMMENT '接收人id',
  `msg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '消息内容',
  `datetime` datetime(0) NULL DEFAULT NULL COMMENT '消息发送时间',
  `create_time` int(11) NULL DEFAULT NULL COMMENT '时间戳',
  `is_reda` int(1) NULL DEFAULT 1 COMMENT '是否已读 1 未读  2已读',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '消息内容历史记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tu_new_friends
-- ----------------------------
DROP TABLE IF EXISTS `tu_new_friends`;
CREATE TABLE `tu_new_friends`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NULL DEFAULT NULL COMMENT '发起申请用户id',
  `mid` int(11) NULL DEFAULT NULL COMMENT '被申请人id',
  `desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '申请理由',
  `msg` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '拒绝理由',
  `status` int(1) NULL DEFAULT NULL COMMENT '状态 0 待审核 1 已通过 2 已拒绝',
  `create_time` int(11) NULL DEFAULT NULL COMMENT '申请时间',
  `update_time` int(11) NULL DEFAULT NULL COMMENT '处理时间',
  `from` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '来源',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '新的好友申请记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tu_new_friends
-- ----------------------------
INSERT INTO `tu_new_friends` VALUES (1, 1000062, 1000063, '麻烦通过一下', NULL, 1, NULL, NULL, '群聊');
INSERT INTO `tu_new_friends` VALUES (2, 1000062, 1000064, '麻烦通过一下', NULL, 0, NULL, NULL, '群聊');
INSERT INTO `tu_new_friends` VALUES (3, 1000042, 1000062, '麻烦通过一下', '', 1, NULL, NULL, '群聊');
INSERT INTO `tu_new_friends` VALUES (18, 1000017, 1000062, '我是群聊”群聊“的杨骞本骞', '', 1, 1642836009, 1642836009, '群聊');
INSERT INTO `tu_new_friends` VALUES (19, 1000062, 1000017, '我是杨骞本骞', NULL, 0, 1642840231, 1642840231, '账户搜索');
INSERT INTO `tu_new_friends` VALUES (21, 1000062, 1000022, '我是售前客服', '', 1, 1643099029, 1643099029, '账户搜索');

-- ----------------------------
-- Table structure for tu_new_module
-- ----------------------------
DROP TABLE IF EXISTS `tu_new_module`;
CREATE TABLE `tu_new_module`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模块名',
  `table_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表名',
  `file_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成的文件名',
  `field` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '字段列表',
  `field_text` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '字段配置表',
  `file_number` int(11) NULL DEFAULT NULL COMMENT '字段数量',
  `status` int(11) NULL DEFAULT 1 COMMENT '1 未生成 2 已生成',
  `is_model` int(1) NULL DEFAULT 0 COMMENT '是否仅生成模块  1 仅模块  0 生成全部',
  `create_time` int(11) NULL DEFAULT NULL COMMENT '创建时间',
  `up_time` int(11) NULL DEFAULT NULL COMMENT '更新时间',
  `output_time` int(11) NULL DEFAULT NULL COMMENT '输出成文件的时间',
  `is_output` int(1) NULL DEFAULT 0 COMMENT '是否已经输出文件 1 输出 0 未输出',
  `is_output_api` int(1) NULL DEFAULT 0 COMMENT '是否已经输出api模块文件 1 输出 0 未输出',
  `is_edit_new` int(1) NULL DEFAULT 0 COMMENT '编辑是否新窗口打开 1 新窗口 0 弹框',
  `is_show` int(1) NULL DEFAULT 1 COMMENT '是否隐藏  1 隐藏  0 显示',
  `table_comment` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表注释',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '根据表模型 创建curd模块关联文件' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tu_new_module
-- ----------------------------
INSERT INTO `tu_new_module` VALUES (1, '客户列表', 'tu_client', 'Client', '[{\"field\":\"id\",\"type\":\"int(11)\",\"name\":\"\",\"column_comment\":\"\",\"is_edit\":false,\"show_list\":1,\"is_time\":0},{\"field\":\"role\",\"type\":\"int(1)\",\"name\":\"\\u5e73\\u53f0\\u662f1\",\"column_comment\":\"\\u5e73\\u53f0\\u662f1 \\u987e\\u5ba2\\u662f2\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"admin_id\",\"type\":\"int(11)\",\"name\":\"\\u6240\\u5c5e\\u516c\\u53f8\",\"column_comment\":\"\\u6240\\u5c5e\\u516c\\u53f8\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"name\",\"type\":\"varchar(255)\",\"name\":\"\\u5ba2\\u6237\\u59d3\\u540d\",\"column_comment\":\"\\u5ba2\\u6237\\u59d3\\u540d \\u771f\\u5b9e\\u59d3\\u540d\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"mobile\",\"type\":\"varchar(11)\",\"name\":\"\\u5ba2\\u6237\\u624b\\u673a\\u53f7\",\"column_comment\":\"\\u5ba2\\u6237\\u624b\\u673a\\u53f7\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"password\",\"type\":\"varchar(32)\",\"name\":\"\\u767b\\u5f55\\u5bc6\\u7801\",\"column_comment\":\"\\u767b\\u5f55\\u5bc6\\u7801\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"uid\",\"type\":\"varchar(30)\",\"name\":\"\\u5ba2\\u6237\\u5e73\\u53f0id\",\"column_comment\":\"\\u5ba2\\u6237\\u5e73\\u53f0id\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"headimg\",\"type\":\"varchar(500)\",\"name\":\"\\u5ba2\\u6237\\u5934\\u50cfid\",\"column_comment\":\"\\u5ba2\\u6237\\u5934\\u50cfid\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"nickname\",\"type\":\"varchar(30)\",\"name\":\"\\u7528\\u6237\\u6635\\u79f0\",\"column_comment\":\"\\u7528\\u6237\\u6635\\u79f0\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"sex\",\"type\":\"int(1)\",\"name\":\"\\u6027\\u522b\",\"column_comment\":\"\\u6027\\u522b \\u672a\\u77e5 0  \\u7537 1 \\u5973 2\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"signature\",\"type\":\"varchar(255)\",\"name\":\"\\u4e2a\\u6027\\u7b7e\\u540d\",\"column_comment\":\"\\u4e2a\\u6027\\u7b7e\\u540d\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"area\",\"type\":\"varchar(30)\",\"name\":\"\\u5730\\u533a\",\"column_comment\":\"\\u5730\\u533a\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"from\",\"type\":\"varchar(30)\",\"name\":\"\\u6765\\u6e90\",\"column_comment\":\"\\u6765\\u6e90\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"desc\",\"type\":\"varchar(255)\",\"name\":\"\\u63cf\\u8ff0\",\"column_comment\":\"\\u63cf\\u8ff0\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"extend\",\"type\":\"text\",\"name\":\"\\u5ba2\\u6237\\u6269\\u5c55\\u4fe1\\u606f\",\"column_comment\":\"\\u5ba2\\u6237\\u6269\\u5c55\\u4fe1\\u606f\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"create_time\",\"type\":\"int(11)\",\"name\":\"\\u521b\\u5efa\\u65f6\\u95f4\",\"column_comment\":\"\\u521b\\u5efa\\u65f6\\u95f4\",\"is_edit\":0,\"show_list\":1,\"is_time\":1},{\"field\":\"update_time\",\"type\":\"int(11)\",\"name\":\"\\u66f4\\u65b0\\u65f6\\u95f4\",\"column_comment\":\"\\u66f4\\u65b0\\u65f6\\u95f4\",\"is_edit\":0,\"show_list\":1,\"is_time\":1},{\"field\":\"online\",\"type\":\"int(11)\",\"name\":\"\\u6700\\u540e\\u6d3b\\u52a8\\u65f6\\u95f4\",\"column_comment\":\"\\u6700\\u540e\\u6d3b\\u52a8\\u65f6\\u95f4 1 \\u5728\\u7ebf 0 \\u79bb\\u7ebf\",\"is_edit\":1,\"show_list\":1,\"is_time\":0}]', '[{\"id\":1,\"field\":\"id\",\"type\":\"int(11)\",\"name\":\"id\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":\"false\",\"is_time\":0,\"column_comment\":\"\",\"LAY_TABLE_INDEX\":0},{\"id\":2,\"field\":\"role\",\"type\":\"int(1)\",\"name\":\"role\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":1,\"is_time\":0,\"column_comment\":\"平台是1 顾客是2\",\"LAY_TABLE_INDEX\":1},{\"id\":3,\"field\":\"admin_id\",\"type\":\"int(11)\",\"name\":\"所属公司\",\"field_type\":1,\"field_default\":\"\",\"show_list\":0,\"is_edit\":0,\"is_time\":0,\"column_comment\":\"所属公司\",\"LAY_TABLE_INDEX\":2},{\"id\":4,\"field\":\"depart_id\",\"type\":\"int(11)\",\"name\":\"接待部门\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":0,\"is_time\":0,\"column_comment\":\"接待部门(废弃掉)\",\"LAY_TABLE_INDEX\":3},{\"id\":5,\"field\":\"staff_id\",\"type\":\"int(11)\",\"name\":\"接待人\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":1,\"is_time\":0,\"column_comment\":\"接待人(废弃掉)\",\"LAY_TABLE_INDEX\":4},{\"id\":6,\"field\":\"name\",\"type\":\"varchar(255)\",\"name\":\"客户姓名\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":1,\"is_time\":0,\"column_comment\":\"客户姓名 真实姓名\",\"LAY_TABLE_INDEX\":5},{\"id\":7,\"field\":\"mobile\",\"type\":\"varchar(11)\",\"name\":\"客户手机号\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":1,\"is_time\":0,\"column_comment\":\"客户手机号\",\"LAY_TABLE_INDEX\":6},{\"id\":8,\"field\":\"password\",\"type\":\"varchar(32)\",\"name\":\"password\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":1,\"is_time\":0,\"column_comment\":\"登录密码\",\"LAY_TABLE_INDEX\":7},{\"id\":9,\"field\":\"uid\",\"type\":\"varchar(30)\",\"name\":\"客户平台id\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":1,\"is_time\":0,\"column_comment\":\"客户平台id\",\"LAY_TABLE_INDEX\":8},{\"id\":10,\"field\":\"headimg\",\"type\":\"varchar(500)\",\"name\":\"headimg\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":1,\"is_time\":0,\"column_comment\":\"客户头像id\",\"LAY_TABLE_INDEX\":9},{\"id\":11,\"field\":\"nickname\",\"type\":\"varchar(30)\",\"name\":\"nickname\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":1,\"is_time\":0,\"column_comment\":\"用户昵称\",\"LAY_TABLE_INDEX\":10},{\"id\":12,\"field\":\"sex\",\"type\":\"int(1)\",\"name\":\"sex\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":1,\"is_time\":0,\"column_comment\":\"性别 未知 0  男 1 女 2\",\"LAY_TABLE_INDEX\":11},{\"id\":13,\"field\":\"signature\",\"type\":\"varchar(255)\",\"name\":\"signature\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":1,\"is_time\":0,\"column_comment\":\"个性签名\",\"LAY_TABLE_INDEX\":12},{\"id\":14,\"field\":\"area\",\"type\":\"varchar(30)\",\"name\":\"area\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":1,\"is_time\":0,\"column_comment\":\"地区\",\"LAY_TABLE_INDEX\":13},{\"id\":15,\"field\":\"from\",\"type\":\"varchar(30)\",\"name\":\"from\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":1,\"is_time\":0,\"column_comment\":\"来源\",\"LAY_TABLE_INDEX\":14},{\"id\":16,\"field\":\"desc\",\"type\":\"varchar(255)\",\"name\":\"desc\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":1,\"is_time\":0,\"column_comment\":\"描述\",\"LAY_TABLE_INDEX\":15},{\"id\":17,\"field\":\"extend\",\"type\":\"text\",\"name\":\"客户扩展信息\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":0,\"is_time\":0,\"column_comment\":\"客户扩展信息\",\"LAY_TABLE_INDEX\":16},{\"id\":18,\"field\":\"create_time\",\"type\":\"int(11)\",\"name\":\"创建时间\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":0,\"is_time\":1,\"column_comment\":\"创建时间\",\"LAY_TABLE_INDEX\":17},{\"id\":19,\"field\":\"update_time\",\"type\":\"int(11)\",\"name\":\"更新时间\",\"field_type\":1,\"field_default\":\"\",\"show_list\":0,\"is_edit\":0,\"is_time\":1,\"column_comment\":\"更新时间\",\"LAY_TABLE_INDEX\":18},{\"id\":20,\"field\":\"online\",\"type\":\"int(11)\",\"name\":\"最后活动时间\",\"field_type\":1,\"field_default\":\"\",\"show_list\":0,\"is_edit\":0,\"is_time\":0,\"column_comment\":\"最后活动时间\",\"LAY_TABLE_INDEX\":19}]', 18, 1, 0, 1625727211, 1642930442, 1642930478, 1, 0, 0, 1, '接入客户表');
INSERT INTO `tu_new_module` VALUES (2, 'curd功能演示demo', 'tu_demo', 'Demo', '[{\"field\":\"id\",\"type\":\"int(11)\",\"name\":\"\",\"column_comment\":\"\",\"is_edit\":false,\"show_list\":1,\"is_time\":0},{\"field\":\"img\",\"type\":\"varchar(255)\",\"name\":\"\\u56fe\\u7247\",\"column_comment\":\"\\u56fe\\u7247\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"tag\",\"type\":\"varchar(255)\",\"name\":\"\\u591a\\u9009\\u6807\\u7b7e\",\"column_comment\":\"\\u591a\\u9009\\u6807\\u7b7e 1 \\u7f8e\\u5973 2 \\u6700\\u70ed\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"title\",\"type\":\"varchar(255)\",\"name\":\"\\u6807\\u9898\",\"column_comment\":\"\\u6807\\u9898\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"status\",\"type\":\"int(11)\",\"name\":\"\\u662f\\u5426\\u663e\\u793a\",\"column_comment\":\"\\u662f\\u5426\\u663e\\u793a 1 \\u663e\\u793a 2 \\u9690\\u85cf\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"display_order\",\"type\":\"int(11)\",\"name\":\"\\u6392\\u5e8f\",\"column_comment\":\"\\u6392\\u5e8f\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"create_time\",\"type\":\"int(11)\",\"name\":\"\\u521b\\u5efa\\u65f6\\u95f4\",\"column_comment\":\"\\u521b\\u5efa\\u65f6\\u95f4\",\"is_edit\":0,\"show_list\":1,\"is_time\":1},{\"field\":\"update_time\",\"type\":\"int(11)\",\"name\":\"\\u66f4\\u65b0\\u65f6\\u95f4\",\"column_comment\":\"\\u66f4\\u65b0\\u65f6\\u95f4\",\"is_edit\":0,\"show_list\":1,\"is_time\":1}]', '[{\"field\":\"id\",\"type\":\"int(11)\",\"name\":\"\",\"column_comment\":\"\",\"is_edit\":false,\"show_list\":1,\"is_time\":0},{\"field\":\"img\",\"type\":\"varchar(255)\",\"name\":\"\\u56fe\\u7247\",\"column_comment\":\"\\u56fe\\u7247\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"tag\",\"type\":\"varchar(255)\",\"name\":\"\\u591a\\u9009\\u6807\\u7b7e\",\"column_comment\":\"\\u591a\\u9009\\u6807\\u7b7e 1 \\u7f8e\\u5973 2 \\u6700\\u70ed\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"title\",\"type\":\"varchar(255)\",\"name\":\"\\u6807\\u9898\",\"column_comment\":\"\\u6807\\u9898\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"status\",\"type\":\"int(11)\",\"name\":\"\\u662f\\u5426\\u663e\\u793a\",\"column_comment\":\"\\u662f\\u5426\\u663e\\u793a 1 \\u663e\\u793a 2 \\u9690\\u85cf\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"display_order\",\"type\":\"int(11)\",\"name\":\"\\u6392\\u5e8f\",\"column_comment\":\"\\u6392\\u5e8f\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"create_time\",\"type\":\"int(11)\",\"name\":\"\\u521b\\u5efa\\u65f6\\u95f4\",\"column_comment\":\"\\u521b\\u5efa\\u65f6\\u95f4\",\"is_edit\":0,\"show_list\":1,\"is_time\":1},{\"field\":\"update_time\",\"type\":\"int(11)\",\"name\":\"\\u66f4\\u65b0\\u65f6\\u95f4\",\"column_comment\":\"\\u66f4\\u65b0\\u65f6\\u95f4\",\"is_edit\":0,\"show_list\":1,\"is_time\":1}]', 8, 1, 0, 1625727211, 1642930443, 1626138003, 1, 0, 0, 1, 'curd功能演示demo');
INSERT INTO `tu_new_module` VALUES (3, '部门列表', 'tu_department', 'Department', '[{\"field\":\"id\",\"type\":\"int(11)\",\"name\":\"\\u90e8\\u95e8id\",\"column_comment\":\"\\u90e8\\u95e8id\",\"is_edit\":false,\"show_list\":1,\"is_time\":0},{\"field\":\"admin_id\",\"type\":\"int(11)\",\"name\":\"\\u516c\\u53f8id\",\"column_comment\":\"\\u516c\\u53f8id\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"name\",\"type\":\"varchar(255)\",\"name\":\"\\u90e8\\u95e8\\u540d\",\"column_comment\":\"\\u90e8\\u95e8\\u540d\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"number\",\"type\":\"int(11)\",\"name\":\"\\u90e8\\u95e8\\u4eba\\u6570\",\"column_comment\":\"\\u90e8\\u95e8\\u4eba\\u6570\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"status\",\"type\":\"int(1)\",\"name\":\"\\u90e8\\u95e8\\u72b6\\u6001\",\"column_comment\":\"\\u90e8\\u95e8\\u72b6\\u6001 1 \\u542f\\u7528 2\\u7981\\u7528\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"priority\",\"type\":\"int(1)\",\"name\":\"\\u4f18\\u5148\\u7ea7\",\"column_comment\":\"\\u4f18\\u5148\\u7ea7 \\u8d8a\\u5927\\u8d8a\\u4f18\\u5148\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"create_time\",\"type\":\"int(11)\",\"name\":\"\\u521b\\u5efa\\u65f6\\u95f4\",\"column_comment\":\"\\u521b\\u5efa\\u65f6\\u95f4\",\"is_edit\":0,\"show_list\":1,\"is_time\":1},{\"field\":\"update_time\",\"type\":\"int(11)\",\"name\":\"\\u66f4\\u65b0\\u65f6\\u95f4\",\"column_comment\":\"\\u66f4\\u65b0\\u65f6\\u95f4\",\"is_edit\":0,\"show_list\":1,\"is_time\":1}]', '[{\"id\":1,\"field\":\"id\",\"type\":\"int(11)\",\"name\":\"ID\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":\"false\",\"is_time\":0,\"column_comment\":\"部门id\",\"LAY_TABLE_INDEX\":0},{\"id\":2,\"field\":\"admin_id\",\"type\":\"int(11)\",\"name\":\"公司ID\",\"field_type\":1,\"field_default\":\"\",\"show_list\":0,\"is_edit\":0,\"is_time\":0,\"column_comment\":\"公司id\",\"LAY_TABLE_INDEX\":1},{\"id\":3,\"field\":\"name\",\"type\":\"varchar(255)\",\"name\":\"部门名\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":1,\"is_time\":0,\"column_comment\":\"部门名\",\"LAY_TABLE_INDEX\":2},{\"id\":4,\"field\":\"number\",\"type\":\"int(11)\",\"name\":\"部门人数\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":1,\"is_time\":0,\"column_comment\":\"部门人数\",\"LAY_TABLE_INDEX\":3},{\"id\":5,\"field\":\"status\",\"type\":\"int(1)\",\"name\":\"部门状态\",\"field_type\":2,\"field_default\":\"{\\u00221\\u0022:\\u0022启用\\u0022,\\u00222\\u0022:\\u0022禁用\\u0022}\",\"show_list\":1,\"is_edit\":1,\"is_time\":0,\"column_comment\":\"部门状态 1 启用 2禁用\",\"LAY_TABLE_INDEX\":4},{\"id\":6,\"field\":\"priority\",\"type\":\"int(1)\",\"name\":\"优先级\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":1,\"is_time\":0,\"column_comment\":\"优先级 越大越优先\",\"LAY_TABLE_INDEX\":5},{\"id\":7,\"field\":\"create_time\",\"type\":\"int(11)\",\"name\":\"创建时间\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":0,\"is_time\":1,\"column_comment\":\"创建时间\",\"LAY_TABLE_INDEX\":6},{\"id\":8,\"field\":\"update_time\",\"type\":\"int(11)\",\"name\":\"更新时间\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":0,\"is_time\":1,\"column_comment\":\"更新时间\",\"LAY_TABLE_INDEX\":7}]', 8, 1, 0, 1625727211, 1642930443, 1626139200, 1, 0, 0, 1, '部门名称');
INSERT INTO `tu_new_module` VALUES (4, '消息列表', 'tu_message_log', 'MessageLog', '[{\"field\":\"id\",\"type\":\"int(11)\",\"name\":\"\",\"column_comment\":\"\",\"is_edit\":false,\"show_list\":1,\"is_time\":0},{\"field\":\"is_group\",\"type\":\"int(11)\",\"name\":\"\\u662f\\u5426\\u662f\\u7fa4\\u7ec4\\u6d88\\u606f\",\"column_comment\":\"\\u662f\\u5426\\u662f\\u7fa4\\u7ec4\\u6d88\\u606f 1 \\u662f  0 \\u4e0d\\u662f\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"type\",\"type\":\"varchar(20)\",\"name\":\"\\u6d88\\u606f\\u7c7b\\u578b\",\"column_comment\":\"\\u6d88\\u606f\\u7c7b\\u578b 1 \\u6587\\u5b57 2 \\u56fe\\u7247  3 \\u7cfb\\u7edf\\u6d88\\u606f  4 \\u4ec5\\u81ea\\u8eab\\u53ef\\u89c1  5 \\u7cfb\\u7edf\\u6d88\\u606f\\uff0c\\u4e0d\\u8865\\u5145\\u4eba\\u540d  6 \\u540d\\u7247\\u6d88\\u606f\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"from_user_id\",\"type\":\"int(11)\",\"name\":\"\\u53d1\\u9001\\u4ebaid\",\"column_comment\":\"\\u53d1\\u9001\\u4ebaid\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"from_user_nickname\",\"type\":\"varchar(30)\",\"name\":\"\\u53d1\\u9001\\u4eba\\u6635\\u79f0\",\"column_comment\":\"\\u53d1\\u9001\\u4eba\\u6635\\u79f0\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"from_user_headimg\",\"type\":\"varchar(500)\",\"name\":\"\\u53d1\\u9001\\u4eba\\u5934\\u50cf\",\"column_comment\":\"\\u53d1\\u9001\\u4eba\\u5934\\u50cf\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"from_role\",\"type\":\"int(1)\",\"name\":\"\\u53d1\\u9001\\u4eba\\u7684\\u7ea7\\u522b\",\"column_comment\":\"\\u53d1\\u9001\\u4eba\\u7684\\u7ea7\\u522b 1 \\u5ba2\\u670d 2 \\u987e\\u5ba2\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"to_user_id\",\"type\":\"int(11)\",\"name\":\"\\u63a5\\u6536\\u4ebaid\",\"column_comment\":\"\\u63a5\\u6536\\u4ebaid\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"msg\",\"type\":\"varchar(255)\",\"name\":\"\\u6d88\\u606f\\u5185\\u5bb9\",\"column_comment\":\"\\u6d88\\u606f\\u5185\\u5bb9\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"datetime\",\"type\":\"datetime\",\"name\":\"\\u6d88\\u606f\\u53d1\\u9001\\u65f6\\u95f4\",\"column_comment\":\"\\u6d88\\u606f\\u53d1\\u9001\\u65f6\\u95f4\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"create_time\",\"type\":\"int(11)\",\"name\":\"\\u65f6\\u95f4\\u6233\",\"column_comment\":\"\\u65f6\\u95f4\\u6233\",\"is_edit\":0,\"show_list\":1,\"is_time\":1},{\"field\":\"is_reda\",\"type\":\"int(1)\",\"name\":\"\\u662f\\u5426\\u5df2\\u8bfb\",\"column_comment\":\"\\u662f\\u5426\\u5df2\\u8bfb 1 \\u672a\\u8bfb  2\\u5df2\\u8bfb\",\"is_edit\":1,\"show_list\":1,\"is_time\":0}]', '[{\"id\":1,\"field\":\"id\",\"type\":\"int(11)\",\"name\":\"id\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":\"false\",\"is_time\":0,\"column_comment\":\"\",\"LAY_TABLE_INDEX\":0},{\"id\":2,\"field\":\"type\",\"type\":\"varchar(20)\",\"name\":\"消息类型\",\"field_type\":3,\"field_default\":\"{\\u00221\\u0022:\\u0022文本\\u0022,\\u00222\\u0022:\\u0022图片\\u0022,\\u00223\\u0022:\\u0022视频\\u0022,\\u00224\\u0022:\\u0022音频\\u0022,\\u00225\\u0022:\\u0022系统\\u0022}\",\"show_list\":1,\"is_edit\":0,\"is_time\":0,\"column_comment\":\"消息类型\",\"LAY_TABLE_INDEX\":1},{\"id\":3,\"field\":\"from_user_id\",\"type\":\"int(11)\",\"name\":\"发送人id\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":0,\"is_time\":0,\"column_comment\":\"发送人id\",\"LAY_TABLE_INDEX\":2},{\"id\":4,\"field\":\"from_user_nickname\",\"type\":\"varchar(30)\",\"name\":\"发送人昵称\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":0,\"is_time\":0,\"column_comment\":\"发送人昵称\",\"LAY_TABLE_INDEX\":3},{\"id\":5,\"field\":\"from_user_headimg\",\"type\":\"varchar(100)\",\"name\":\"发送人头像\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":0,\"is_time\":0,\"column_comment\":\"发送人头像\",\"LAY_TABLE_INDEX\":4},{\"id\":6,\"field\":\"from_role\",\"type\":\"int(1)\",\"name\":\"级别\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":0,\"is_time\":0,\"column_comment\":\"发送人的级别 1 客服 2 顾客\",\"LAY_TABLE_INDEX\":5},{\"id\":7,\"field\":\"to_user_id\",\"type\":\"int(1)\",\"name\":\"接收人id\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":0,\"is_time\":0,\"column_comment\":\"接收人id\",\"LAY_TABLE_INDEX\":6},{\"id\":8,\"field\":\"msg\",\"type\":\"varchar(255)\",\"name\":\"msg\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":0,\"is_time\":0,\"column_comment\":\"消息内容\",\"LAY_TABLE_INDEX\":7},{\"id\":9,\"field\":\"datetime\",\"type\":\"datetime\",\"name\":\"发送时间\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":0,\"is_time\":0,\"column_comment\":\"消息发送时间\",\"LAY_TABLE_INDEX\":8},{\"id\":10,\"field\":\"is_reda\",\"type\":\"int(1)\",\"name\":\"是否已读\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":0,\"is_time\":0,\"column_comment\":\"是否已读 1 未读  2已读\",\"LAY_TABLE_INDEX\":9}]', 12, 1, 0, 1625727211, 1642930443, 1627028616, 1, 0, 0, 1, '消息内容历史记录');
INSERT INTO `tu_new_module` VALUES (5, '员工列表', 'tu_staff', 'Staff', '[{\"field\":\"id\",\"type\":\"int(11)\",\"name\":\"\",\"column_comment\":\"\",\"is_edit\":false,\"show_list\":1,\"is_time\":0},{\"field\":\"admin_id\",\"type\":\"int(11)\",\"name\":\"\\u6240\\u5c5e\\u516c\\u53f8\",\"column_comment\":\"\\u6240\\u5c5e\\u516c\\u53f8\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"depart_id\",\"type\":\"varchar(100)\",\"name\":\"\\u6240\\u5c5e\\u90e8\\u95e8\",\"column_comment\":\"\\u6240\\u5c5e\\u90e8\\u95e8 \\u591a\\u4e2a\\u90e8\\u95e8\\u7528\\u9017\\u53f7\\u5206\\u5272\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"name\",\"type\":\"varchar(255)\",\"name\":\"\\u59d3\\u540d\",\"column_comment\":\"\\u59d3\\u540d\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"mobile\",\"type\":\"varchar(255)\",\"name\":\"\\u624b\\u673a\\u53f7\",\"column_comment\":\"\\u624b\\u673a\\u53f7\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"password\",\"type\":\"varchar(255)\",\"name\":\"\\u5bc6\\u7801\",\"column_comment\":\"\\u5bc6\\u7801\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"head\",\"type\":\"varchar(255)\",\"name\":\"\\u5934\\u50cf\",\"column_comment\":\"\\u5934\\u50cf\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"priority\",\"type\":\"int(11)\",\"name\":\"\\u4f18\\u5148\\u7ea7\",\"column_comment\":\"\\u4f18\\u5148\\u7ea7 \\u8d8a\\u5927\\u8d8a\\u4f18\\u5148\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"status\",\"type\":\"int(1)\",\"name\":\"\\u72b6\\u6001\",\"column_comment\":\"\\u72b6\\u6001 1 \\u542f\\u7528  2 \\u7981\\u7528\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"work_status\",\"type\":\"int(1)\",\"name\":\"\\u5de5\\u4f5c\\u72b6\\u6001\",\"column_comment\":\"\\u5de5\\u4f5c\\u72b6\\u6001  1 \\u6b63\\u5e38 2 \\u5fd9\\u788c 3 \\u6302\\u8d77\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"online\",\"type\":\"int(11)\",\"name\":\"\\u6700\\u540e\\u6d3b\\u52a8\\u65f6\\u95f4\",\"column_comment\":\"\\u6700\\u540e\\u6d3b\\u52a8\\u65f6\\u95f4 \\u4f4e\\u4e8e300\\u79d2\\u672a\\u6d3b\\u52a8 \\u89c6\\u4e3a\\u79bb\\u7ebf\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"create_time\",\"type\":\"int(11)\",\"name\":\"\\u521b\\u5efa\\u65f6\\u95f4\",\"column_comment\":\"\\u521b\\u5efa\\u65f6\\u95f4\",\"is_edit\":0,\"show_list\":1,\"is_time\":1},{\"field\":\"update_time\",\"type\":\"int(11)\",\"name\":\"\\u66f4\\u65b0\\u65f6\\u95f4\",\"column_comment\":\"\\u66f4\\u65b0\\u65f6\\u95f4\",\"is_edit\":0,\"show_list\":1,\"is_time\":1},{\"field\":\"login_time\",\"type\":\"int(11)\",\"name\":\"\\u6700\\u540e\\u767b\\u9646\\u65f6\\u95f4\",\"column_comment\":\"\\u6700\\u540e\\u767b\\u9646\\u65f6\\u95f4\",\"is_edit\":0,\"show_list\":1,\"is_time\":1},{\"field\":\"client_id\",\"type\":\"int(11)\",\"name\":\"\\u7ed1\\u5b9aclient_id\",\"column_comment\":\"\\u7ed1\\u5b9aclient_id \",\"is_edit\":1,\"show_list\":1,\"is_time\":0}]', '[{\"id\":1,\"field\":\"id\",\"type\":\"int(11)\",\"name\":\"id\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":\"false\",\"is_time\":0,\"column_comment\":\"\",\"LAY_TABLE_INDEX\":0},{\"id\":2,\"field\":\"admin_id\",\"type\":\"int(11)\",\"name\":\"所属公司\",\"field_type\":1,\"field_default\":\"\",\"show_list\":0,\"is_edit\":0,\"is_time\":0,\"column_comment\":\"所属公司\",\"LAY_TABLE_INDEX\":1},{\"id\":3,\"field\":\"depa_id\",\"type\":\"varchar(100)\",\"name\":\"所属部门\",\"field_type\":5,\"field_default\":\"{\\u00221\\u0022:\\u0022默认部门\\u0022}\",\"show_list\":1,\"is_edit\":1,\"is_time\":0,\"column_comment\":\"所属部门 多个部门用逗号分割\",\"LAY_TABLE_INDEX\":2},{\"id\":4,\"field\":\"name\",\"type\":\"varchar(255)\",\"name\":\"姓名\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":1,\"is_time\":0,\"column_comment\":\"姓名\",\"LAY_TABLE_INDEX\":3},{\"id\":5,\"field\":\"mobile\",\"type\":\"varchar(255)\",\"name\":\"手机号\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":1,\"is_time\":0,\"column_comment\":\"手机号\",\"LAY_TABLE_INDEX\":4},{\"id\":6,\"field\":\"password\",\"type\":\"varchar(255)\",\"name\":\"密码\",\"field_type\":1,\"field_default\":\"\",\"show_list\":0,\"is_edit\":1,\"is_time\":0,\"column_comment\":\"密码\",\"LAY_TABLE_INDEX\":5},{\"id\":7,\"field\":\"head\",\"type\":\"varchar(255)\",\"name\":\"头像\",\"field_type\":6,\"field_default\":\"\",\"show_list\":1,\"is_edit\":1,\"is_time\":0,\"column_comment\":\"头像\",\"LAY_TABLE_INDEX\":6},{\"id\":8,\"field\":\"priority\",\"type\":\"int(11)\",\"name\":\"优先级\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":1,\"is_time\":0,\"column_comment\":\"优先级 越大越优先\",\"LAY_TABLE_INDEX\":7},{\"id\":9,\"field\":\"status\",\"type\":\"int(1)\",\"name\":\"状态\",\"field_type\":2,\"field_default\":\"{\\u00221\\u0022:\\u0022启用\\u0022,\\u00222\\u0022:\\u0022禁用\\u0022}\",\"show_list\":1,\"is_edit\":1,\"is_time\":0,\"column_comment\":\"状态 1 启用  2 禁用\",\"LAY_TABLE_INDEX\":8},{\"id\":10,\"field\":\"work_status\",\"type\":\"int(1)\",\"name\":\"工作状态\",\"field_type\":3,\"field_default\":\"{\\u00221\\u0022:\\u0022正常\\u0022,\\u00222\\u0022:\\u0022忙碌\\u0022,\\u00223\\u0022:\\u0022挂起\\u0022}\",\"show_list\":1,\"is_edit\":0,\"is_time\":0,\"column_comment\":\"工作状态  1 正常 2 忙碌 3 挂起\",\"LAY_TABLE_INDEX\":9},{\"id\":11,\"field\":\"online\",\"type\":\"int(11)\",\"name\":\"最后活动时间\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":0,\"is_time\":0,\"column_comment\":\"最后活动时间 低于300秒未活动 视为离线\",\"LAY_TABLE_INDEX\":10},{\"id\":12,\"field\":\"create_time\",\"type\":\"int(11)\",\"name\":\"创建时间\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":0,\"is_time\":1,\"column_comment\":\"创建时间\",\"LAY_TABLE_INDEX\":11},{\"id\":13,\"field\":\"update_time\",\"type\":\"int(11)\",\"name\":\"更新时间\",\"field_type\":1,\"field_default\":\"\",\"show_list\":0,\"is_edit\":0,\"is_time\":1,\"column_comment\":\"更新时间\",\"LAY_TABLE_INDEX\":12},{\"id\":14,\"field\":\"login_time\",\"type\":\"int(11)\",\"name\":\"最后登陆时间\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":0,\"is_time\":1,\"column_comment\":\"最后登陆时间\",\"LAY_TABLE_INDEX\":13}]', 15, 1, 0, 1625727211, 1642930443, 1626140084, 1, 0, 0, 1, '公司员工表');
INSERT INTO `tu_new_module` VALUES (6, '调试日志表', 'tu_debug', 'Debug', '[{\"field\":\"id\",\"type\":\"int(11)\",\"name\":\"\",\"column_comment\":\"\",\"is_edit\":false,\"show_list\":1,\"is_time\":0},{\"field\":\"uid\",\"type\":\"int(11)\",\"name\":\"\",\"column_comment\":\"\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"url\",\"type\":\"varchar(255)\",\"name\":\"\",\"column_comment\":\"\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"params\",\"type\":\"varchar(255)\",\"name\":\"\",\"column_comment\":\"\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"createtime\",\"type\":\"datetime\",\"name\":\"\",\"column_comment\":\"\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"ip\",\"type\":\"varchar(255)\",\"name\":\"\",\"column_comment\":\"\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"type\",\"type\":\"varchar(255)\",\"name\":\"\",\"column_comment\":\"\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"header\",\"type\":\"text\",\"name\":\"\",\"column_comment\":\"\",\"is_edit\":1,\"show_list\":1,\"is_time\":0}]', '[{\"field\":\"id\",\"type\":\"int(11)\",\"name\":\"\",\"column_comment\":\"\",\"is_edit\":false,\"show_list\":1,\"is_time\":0},{\"field\":\"uid\",\"type\":\"int(11)\",\"name\":\"\",\"column_comment\":\"\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"url\",\"type\":\"varchar(255)\",\"name\":\"\",\"column_comment\":\"\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"params\",\"type\":\"varchar(255)\",\"name\":\"\",\"column_comment\":\"\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"createtime\",\"type\":\"datetime\",\"name\":\"\",\"column_comment\":\"\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"ip\",\"type\":\"varchar(255)\",\"name\":\"\",\"column_comment\":\"\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"type\",\"type\":\"varchar(255)\",\"name\":\"\",\"column_comment\":\"\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"header\",\"type\":\"varchar(255)\",\"name\":\"\",\"column_comment\":\"\",\"is_edit\":1,\"show_list\":1,\"is_time\":0}]', 8, 1, 1, 1626255450, 1642930443, 1626255462, 1, 0, 0, 1, '调试日志表');
INSERT INTO `tu_new_module` VALUES (7, '员工登录凭证', 'tu_staff_ticket', 'StaffTicket', '[{\"field\":\"id\",\"type\":\"int(11)\",\"name\":\"\",\"column_comment\":\"\",\"is_edit\":false,\"show_list\":1,\"is_time\":0},{\"field\":\"uid\",\"type\":\"int(11)\",\"name\":\"\\u7528\\u6237id\",\"column_comment\":\"\\u7528\\u6237id\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"ticket\",\"type\":\"varchar(32)\",\"name\":\"\\u767b\\u5f55\\u51ed\\u8bc1\",\"column_comment\":\"\\u767b\\u5f55\\u51ed\\u8bc1\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"equi\",\"type\":\"varchar(30)\",\"name\":\"\\u8bbe\\u5907\\u4fe1\\u606f\",\"column_comment\":\"\\u8bbe\\u5907\\u4fe1\\u606f\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"create_time\",\"type\":\"datetime\",\"name\":\"\\u521b\\u5efa\\u65f6\\u95f4\",\"column_comment\":\"\\u521b\\u5efa\\u65f6\\u95f4\",\"is_edit\":0,\"show_list\":1,\"is_time\":1},{\"field\":\"update_time\",\"type\":\"datetime\",\"name\":\"\\u66f4\\u65b0\\u65f6\\u95f4\",\"column_comment\":\"\\u66f4\\u65b0\\u65f6\\u95f4\",\"is_edit\":0,\"show_list\":1,\"is_time\":1}]', '[{\"id\":1,\"field\":\"id\",\"type\":\"int(11)\",\"name\":\"id\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":\"false\",\"is_time\":0,\"column_comment\":\"\",\"LAY_TABLE_INDEX\":0},{\"id\":2,\"field\":\"uid\",\"type\":\"int(11)\",\"name\":\"用户id\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":1,\"is_time\":0,\"column_comment\":\"用户id\",\"LAY_TABLE_INDEX\":1},{\"id\":3,\"field\":\"ticket\",\"type\":\"varchar(32)\",\"name\":\"登录凭证\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":1,\"is_time\":0,\"column_comment\":\"登录凭证\",\"LAY_TABLE_INDEX\":2},{\"id\":4,\"field\":\"create_time\",\"type\":\"datetime\",\"name\":\"创建时间\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":0,\"is_time\":1,\"column_comment\":\"创建时间\",\"LAY_TABLE_INDEX\":3},{\"id\":5,\"field\":\"update_time\",\"type\":\"datetime\",\"name\":\"更新时间\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":0,\"is_time\":1,\"column_comment\":\"更新时间\",\"LAY_TABLE_INDEX\":4}]', 6, 1, 1, 1626336162, 1642930443, 1626336202, 1, 0, 0, 1, '员工登录凭证表');
INSERT INTO `tu_new_module` VALUES (9, '客户访问日志', 'tu_client_log', 'ClientLog', '[{\"field\":\"id\",\"type\":\"int(11)\",\"name\":\"\",\"column_comment\":\"\",\"is_edit\":false,\"show_list\":1,\"is_time\":0},{\"field\":\"uid\",\"type\":\"int(11)\",\"name\":\"\\u5ba2\\u6237id\",\"column_comment\":\"\\u5ba2\\u6237id\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"depart_id\",\"type\":\"int(11)\",\"name\":\"\\u90e8\\u95e8id\",\"column_comment\":\"\\u90e8\\u95e8id\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"ip\",\"type\":\"varchar(30)\",\"name\":\"\\u6765\\u8bbfip\",\"column_comment\":\"\\u6765\\u8bbfip\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"ip_text\",\"type\":\"varchar(50)\",\"name\":\"ip\\u6240\\u5728\\u5730\",\"column_comment\":\"ip\\u6240\\u5728\\u5730\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"staff_id\",\"type\":\"int(11)\",\"name\":\"\\u63a5\\u5f85\\u4ebaid\",\"column_comment\":\"\\u63a5\\u5f85\\u4ebaid\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"source\",\"type\":\"varchar(255)\",\"name\":\"\\u6765\\u6e90\\u7f51\\u5740\",\"column_comment\":\"\\u6765\\u6e90\\u7f51\\u5740\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"extend\",\"type\":\"text\",\"name\":\"\\u5ba2\\u6237\\u9644\\u52a0\\u4fe1\\u606f\",\"column_comment\":\"\\u5ba2\\u6237\\u9644\\u52a0\\u4fe1\\u606f\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"header\",\"type\":\"varchar(255)\",\"name\":\"\\u5ba2\\u6237\\u8bf7\\u6c42\\u5934\",\"column_comment\":\"\\u5ba2\\u6237\\u8bf7\\u6c42\\u5934\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"os\",\"type\":\"varchar(255)\",\"name\":\"\\u5ba2\\u6237\\u8bf7\\u6c42\",\"column_comment\":\"\\u5ba2\\u6237\\u8bf7\\u6c42\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"create_time\",\"type\":\"datetime\",\"name\":\"\",\"column_comment\":\"\",\"is_edit\":0,\"show_list\":1,\"is_time\":1}]', '[{\"field\":\"id\",\"type\":\"int(11)\",\"name\":\"\",\"column_comment\":\"\",\"is_edit\":false,\"show_list\":1,\"is_time\":0},{\"field\":\"uid\",\"type\":\"int(11)\",\"name\":\"\\u5ba2\\u6237id\",\"column_comment\":\"\\u5ba2\\u6237id\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"depart_id\",\"type\":\"int(11)\",\"name\":\"\\u90e8\\u95e8id\",\"column_comment\":\"\\u90e8\\u95e8id\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"staff_id\",\"type\":\"int(11)\",\"name\":\"\\u63a5\\u5f85\\u4ebaid\",\"column_comment\":\"\\u63a5\\u5f85\\u4ebaid\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"source\",\"type\":\"varchar(255)\",\"name\":\"\\u6765\\u6e90\\u7f51\\u5740\",\"column_comment\":\"\\u6765\\u6e90\\u7f51\\u5740\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"extend\",\"type\":\"text\",\"name\":\"\\u5ba2\\u6237\\u9644\\u52a0\\u4fe1\\u606f\",\"column_comment\":\"\\u5ba2\\u6237\\u9644\\u52a0\\u4fe1\\u606f\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"header\",\"type\":\"varchar(255)\",\"name\":\"\\u5ba2\\u6237\\u8bf7\\u6c42\\u5934\",\"column_comment\":\"\\u5ba2\\u6237\\u8bf7\\u6c42\\u5934\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"os\",\"type\":\"varchar(255)\",\"name\":\"\\u5ba2\\u6237\\u8bf7\\u6c42\",\"column_comment\":\"\\u5ba2\\u6237\\u8bf7\\u6c42\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"create_time\",\"type\":\"datetime\",\"name\":\"\",\"column_comment\":\"\",\"is_edit\":0,\"show_list\":1,\"is_time\":1}]', 11, 1, 1, 1626508656, 1642930442, 1626508662, 1, 0, 0, 1, '客户访问日志');
INSERT INTO `tu_new_module` VALUES (10, '常用语记录', 'tu_common_dis', 'CommonDis', '[{\"field\":\"id\",\"type\":\"int(11)\",\"name\":\"\",\"column_comment\":\"\",\"is_edit\":false,\"show_list\":1,\"is_time\":0},{\"field\":\"admin_id\",\"type\":\"int(11)\",\"name\":\"\\u6240\\u5c5e\\u516c\\u53f8\",\"column_comment\":\"\\u6240\\u5c5e\\u516c\\u53f8\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"msg\",\"type\":\"varchar(255)\",\"name\":\"\\u5e38\\u7528\\u8bed\",\"column_comment\":\"\\u5e38\\u7528\\u8bed\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"uid\",\"type\":\"int(11)\",\"name\":\"\\u64cd\\u4f5c\\u4ebaid\",\"column_comment\":\"\\u64cd\\u4f5c\\u4ebaid\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"order_desc\",\"type\":\"int(1)\",\"name\":\"\\u6392\\u5e8f\",\"column_comment\":\"\\u6392\\u5e8f\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"create_time\",\"type\":\"int(11)\",\"name\":\"\\u521b\\u5efa\\u65f6\\u95f4\",\"column_comment\":\"\\u521b\\u5efa\\u65f6\\u95f4\",\"is_edit\":0,\"show_list\":1,\"is_time\":1},{\"field\":\"update_time\",\"type\":\"int(11)\",\"name\":\"\\u66f4\\u65b0\\u65f6\\u95f4\",\"column_comment\":\"\\u66f4\\u65b0\\u65f6\\u95f4\",\"is_edit\":0,\"show_list\":1,\"is_time\":1}]', '[{\"field\":\"id\",\"type\":\"int(11)\",\"name\":\"\",\"column_comment\":\"\",\"is_edit\":false,\"show_list\":1,\"is_time\":0},{\"field\":\"admin_id\",\"type\":\"int(11)\",\"name\":\"\\u6240\\u5c5e\\u516c\\u53f8\",\"column_comment\":\"\\u6240\\u5c5e\\u516c\\u53f8\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"msg\",\"type\":\"varchar(255)\",\"name\":\"\\u5e38\\u7528\\u8bed\",\"column_comment\":\"\\u5e38\\u7528\\u8bed\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"uid\",\"type\":\"int(11)\",\"name\":\"\\u64cd\\u4f5c\\u4ebaid\",\"column_comment\":\"\\u64cd\\u4f5c\\u4ebaid\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"order_desc\",\"type\":\"int(1)\",\"name\":\"\\u6392\\u5e8f\",\"column_comment\":\"\\u6392\\u5e8f\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"create_time\",\"type\":\"int(11)\",\"name\":\"\\u521b\\u5efa\\u65f6\\u95f4\",\"column_comment\":\"\\u521b\\u5efa\\u65f6\\u95f4\",\"is_edit\":0,\"show_list\":1,\"is_time\":1},{\"field\":\"update_time\",\"type\":\"int(11)\",\"name\":\"\\u66f4\\u65b0\\u65f6\\u95f4\",\"column_comment\":\"\\u66f4\\u65b0\\u65f6\\u95f4\",\"is_edit\":0,\"show_list\":1,\"is_time\":1}]', 7, 1, 0, 1640660522, 1642930443, NULL, 0, 0, 0, 1, '常用语记录');
INSERT INTO `tu_new_module` VALUES (11, '我的好友列表', 'tu_user_list', 'UserList', '[{\"field\":\"id\",\"type\":\"int(11)\",\"name\":\"\",\"column_comment\":\"\",\"is_edit\":false,\"show_list\":1,\"is_time\":0},{\"field\":\"uid\",\"type\":\"int(11)\",\"name\":\"\\u7528\\u6237id\",\"column_comment\":\"\\u7528\\u6237id\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"mid\",\"type\":\"int(11)\",\"name\":\"\\u804a\\u5929\\u5bf9\\u8c61id\",\"column_comment\":\"\\u804a\\u5929\\u5bf9\\u8c61id\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"type\",\"type\":\"int(1)\",\"name\":\"\\u804a\\u5929\\u7c7b\\u578b\",\"column_comment\":\"\\u804a\\u5929\\u7c7b\\u578b  1 \\u597d\\u53cb 2 \\u7fa4\\u7ec4\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"create_time\",\"type\":\"int(11)\",\"name\":\"\\u521b\\u5efa\\u65f6\\u95f4(\\u6dfb\\u52a0\\u597d\\u53cb\\u7684\\u65f6\\u95f4)\",\"column_comment\":\"\\u521b\\u5efa\\u65f6\\u95f4(\\u6dfb\\u52a0\\u597d\\u53cb\\u7684\\u65f6\\u95f4)\",\"is_edit\":0,\"show_list\":1,\"is_time\":1},{\"field\":\"online\",\"type\":\"int(11)\",\"name\":\"\\u6700\\u540e\\u53d1\\u9001\\u6d88\\u606f\\u65f6\\u95f4\",\"column_comment\":\"\\u6700\\u540e\\u53d1\\u9001\\u6d88\\u606f\\u65f6\\u95f4\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"status\",\"type\":\"int(1)\",\"name\":\"\\u597d\\u53cb\\u72b6\\u6001\",\"column_comment\":\"\\u597d\\u53cb\\u72b6\\u6001   1 \\u6b63\\u5e38  2 \\u5c4f\\u853d  3 \\u62c9\\u9ed1 4 \\u5220\\u9664  5 \\u7981\\u8a00(\\u7fa4\\u804a\\u53ef\\u7528)\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"is_show\",\"type\":\"int(1)\",\"name\":\"\\u662f\\u5426\\u663e\\u793a\\u5728\\u6d88\\u606f\\u5217\\u8868\",\"column_comment\":\"\\u662f\\u5426\\u663e\\u793a\\u5728\\u6d88\\u606f\\u5217\\u8868 1 \\u663e\\u793a 2 \\u9690\\u85cf\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"remark\",\"type\":\"varchar(30)\",\"name\":\"\\u5907\\u6ce8(\\u597d\\u53cb\\u5907\\u6ce8)\",\"column_comment\":\"\\u5907\\u6ce8(\\u597d\\u53cb\\u5907\\u6ce8)\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"quiet\",\"type\":\"int(1)\",\"name\":\"\\u6d88\\u606f\\u514d\\u6253\\u6270\",\"column_comment\":\"\\u6d88\\u606f\\u514d\\u6253\\u6270  1 \\u514d\\u6253\\u6270 0 \\u6b63\\u5e38\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"is_top\",\"type\":\"int(1)\",\"name\":\"\\u662f\\u5426\\u7f6e\\u9876\",\"column_comment\":\"\\u662f\\u5426\\u7f6e\\u9876 1 \\u7f6e\\u9876 0 \\u4e0d\\u7f6e\\u9876\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"msg_reda_number\",\"type\":\"int(11)\",\"name\":\"\\u672a\\u8bfb\\u6d88\\u606f\\u6570\\u91cf\",\"column_comment\":\"\\u672a\\u8bfb\\u6d88\\u606f\\u6570\\u91cf\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"delete_time\",\"type\":\"int(10)\",\"name\":\"\\u5220\\u9664\\u65f6\\u95f4\",\"column_comment\":\"\\u5220\\u9664\\u65f6\\u95f4\",\"is_edit\":0,\"show_list\":1,\"is_time\":1}]', '[{\"field\":\"id\",\"type\":\"int(11)\",\"name\":\"\",\"column_comment\":\"\",\"is_edit\":false,\"show_list\":1,\"is_time\":0},{\"field\":\"uid\",\"type\":\"int(11)\",\"name\":\"\\u7528\\u6237id\",\"column_comment\":\"\\u7528\\u6237id\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"mid\",\"type\":\"int(11)\",\"name\":\"\\u804a\\u5929\\u5bf9\\u8c61id\",\"column_comment\":\"\\u804a\\u5929\\u5bf9\\u8c61id\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"create_time\",\"type\":\"int(11)\",\"name\":\"\\u521b\\u5efa\\u65f6\\u95f4\",\"column_comment\":\"\\u521b\\u5efa\\u65f6\\u95f4\",\"is_edit\":0,\"show_list\":1,\"is_time\":1},{\"field\":\"online\",\"type\":\"int(11)\",\"name\":\"\\u6700\\u540e\\u53d1\\u9001\\u6d88\\u606f\\u65f6\\u95f4\",\"column_comment\":\"\\u6700\\u540e\\u53d1\\u9001\\u6d88\\u606f\\u65f6\\u95f4\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"status\",\"type\":\"int(1)\",\"name\":\"1\",\"column_comment\":\"1  \\u663e\\u793a  2 \\u9690\\u85cf\",\"is_edit\":1,\"show_list\":1,\"is_time\":0}]', 13, 1, 0, 1640660522, 1642930443, NULL, 0, 0, 0, 1, '好友列表');
INSERT INTO `tu_new_module` VALUES (12, '客户端登录记录表', 'tu_client_sign', 'ClientSign', '[{\"field\":\"id\",\"type\":\"int(11)\",\"name\":\"\",\"column_comment\":\"\",\"is_edit\":false,\"show_list\":1,\"is_time\":0},{\"field\":\"uid\",\"type\":\"int(11)\",\"name\":\"\\u7528\\u6237id\",\"column_comment\":\"\\u7528\\u6237id\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"ticket\",\"type\":\"varchar(255)\",\"name\":\"\\u51ed\\u8bc1\",\"column_comment\":\"\\u51ed\\u8bc1\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"create_time\",\"type\":\"datetime\",\"name\":\"\\u521b\\u5efa\\u65f6\\u95f4\",\"column_comment\":\"\\u521b\\u5efa\\u65f6\\u95f4\",\"is_edit\":0,\"show_list\":1,\"is_time\":1},{\"field\":\"past_time\",\"type\":\"int(11)\",\"name\":\"\\u8fc7\\u671f\\u65f6\\u95f4\",\"column_comment\":\"\\u8fc7\\u671f\\u65f6\\u95f4\",\"is_edit\":0,\"show_list\":1,\"is_time\":1}]', '[{\"id\":1,\"field\":\"id\",\"type\":\"int(11)\",\"name\":\"id\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":\"false\",\"is_time\":0,\"column_comment\":\"\",\"LAY_TABLE_INDEX\":0},{\"id\":2,\"field\":\"uid\",\"type\":\"int(11)\",\"name\":\"用户id\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":1,\"is_time\":0,\"column_comment\":\"用户id\",\"LAY_TABLE_INDEX\":1},{\"id\":3,\"field\":\"ticket\",\"type\":\"varchar(255)\",\"name\":\"凭证\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":1,\"is_time\":0,\"column_comment\":\"凭证\",\"LAY_TABLE_INDEX\":2},{\"id\":4,\"field\":\"create_time\",\"type\":\"datetime\",\"name\":\"创建时间\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":0,\"is_time\":1,\"column_comment\":\"创建时间\",\"LAY_TABLE_INDEX\":3},{\"id\":5,\"field\":\"past_time\",\"type\":\"int(11)\",\"name\":\"过期时间\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":0,\"is_time\":1,\"column_comment\":\"过期时间\",\"LAY_TABLE_INDEX\":4}]', 5, 1, 1, 1640661241, 1642930442, 1640661253, 1, 0, 0, 1, '客户端登录记录表');
INSERT INTO `tu_new_module` VALUES (13, '群组列表', 'tu_user_group', 'UserGroup', '[{\"field\":\"id\",\"type\":\"int(11)\",\"name\":\"\",\"column_comment\":\"\",\"is_edit\":false,\"show_list\":1,\"is_time\":0},{\"field\":\"admin_id\",\"type\":\"int(11)\",\"name\":\"\",\"column_comment\":\"\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"admin_uid\",\"type\":\"int(11)\",\"name\":\"\\u7fa4\\u4e3bid\",\"column_comment\":\"\\u7fa4\\u4e3bid\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"name\",\"type\":\"varchar(30)\",\"name\":\"\\u7fa4\\u540d\\u79f0\",\"column_comment\":\"\\u7fa4\\u540d\\u79f0\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"member_number\",\"type\":\"int(11)\",\"name\":\"\\u7fa4\\u6210\\u5458\\u4eba\\u6570\",\"column_comment\":\"\\u7fa4\\u6210\\u5458\\u4eba\\u6570\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"create_time\",\"type\":\"int(11)\",\"name\":\"\\u7fa4\\u521b\\u5efa\\u65f6\\u95f4\",\"column_comment\":\"\\u7fa4\\u521b\\u5efa\\u65f6\\u95f4\",\"is_edit\":0,\"show_list\":1,\"is_time\":1},{\"field\":\"update_time\",\"type\":\"int(11)\",\"name\":\"\\u7fa4\\u66f4\\u65b0\\u65f6\\u95f4\",\"column_comment\":\"\\u7fa4\\u66f4\\u65b0\\u65f6\\u95f4\",\"is_edit\":0,\"show_list\":1,\"is_time\":1},{\"field\":\"delete_time\",\"type\":\"int(11)\",\"name\":\"\\u5220\\u9664\\u65f6\\u95f4\",\"column_comment\":\"\\u5220\\u9664\\u65f6\\u95f4\",\"is_edit\":0,\"show_list\":1,\"is_time\":1}]', '[{\"id\":1,\"field\":\"id\",\"type\":\"int(11)\",\"name\":\"id\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":\"false\",\"is_time\":0,\"column_comment\":\"\",\"LAY_TABLE_INDEX\":0},{\"id\":2,\"field\":\"admin_id\",\"type\":\"int(11)\",\"name\":\"admin_id\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":0,\"is_time\":0,\"column_comment\":\"\",\"LAY_TABLE_INDEX\":1},{\"id\":3,\"field\":\"admin_uid\",\"type\":\"int(11)\",\"name\":\"群主id\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":1,\"is_time\":0,\"column_comment\":\"群主id\",\"LAY_TABLE_INDEX\":2},{\"id\":4,\"field\":\"name\",\"type\":\"varchar(30)\",\"name\":\"群名称\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":1,\"is_time\":0,\"column_comment\":\"群名称\",\"LAY_TABLE_INDEX\":3},{\"id\":5,\"field\":\"member_number\",\"type\":\"int(11)\",\"name\":\"群成员人数\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":1,\"is_time\":0,\"column_comment\":\"群成员人数\",\"LAY_TABLE_INDEX\":4},{\"id\":6,\"field\":\"create_time\",\"type\":\"int(11)\",\"name\":\"群创建时间\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":0,\"is_time\":1,\"column_comment\":\"群创建时间\",\"LAY_TABLE_INDEX\":5},{\"id\":7,\"field\":\"update_time\",\"type\":\"int(11)\",\"name\":\"群更新时间\",\"field_type\":1,\"field_default\":\"\",\"show_list\":0,\"is_edit\":0,\"is_time\":1,\"column_comment\":\"群更新时间\",\"LAY_TABLE_INDEX\":6}]', 8, 1, 0, 1640680799, 1642930443, 1640680825, 1, 0, 0, 1, '群组列表');
INSERT INTO `tu_new_module` VALUES (14, '新的好友申请记录', 'tu_new_friends', 'NewFriends', '[{\"field\":\"id\",\"type\":\"int(11)\",\"name\":\"\",\"column_comment\":\"\",\"is_edit\":false,\"show_list\":1,\"is_time\":0},{\"field\":\"uid\",\"type\":\"int(11)\",\"name\":\"\\u53d1\\u8d77\\u7533\\u8bf7\\u7528\\u6237id\",\"column_comment\":\"\\u53d1\\u8d77\\u7533\\u8bf7\\u7528\\u6237id\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"mid\",\"type\":\"int(11)\",\"name\":\"\\u88ab\\u7533\\u8bf7\\u4ebaid\",\"column_comment\":\"\\u88ab\\u7533\\u8bf7\\u4ebaid\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"desc\",\"type\":\"varchar(255)\",\"name\":\"\\u7533\\u8bf7\\u7406\\u7531\",\"column_comment\":\"\\u7533\\u8bf7\\u7406\\u7531\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"msg\",\"type\":\"varchar(255)\",\"name\":\"\\u62d2\\u7edd\\u7406\\u7531\",\"column_comment\":\"\\u62d2\\u7edd\\u7406\\u7531\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"status\",\"type\":\"int(1)\",\"name\":\"\\u72b6\\u6001\",\"column_comment\":\"\\u72b6\\u6001 0 \\u5f85\\u5ba1\\u6838 1 \\u5df2\\u901a\\u8fc7 2 \\u5df2\\u62d2\\u7edd\",\"is_edit\":1,\"show_list\":1,\"is_time\":0},{\"field\":\"create_time\",\"type\":\"int(11)\",\"name\":\"\\u7533\\u8bf7\\u65f6\\u95f4\",\"column_comment\":\"\\u7533\\u8bf7\\u65f6\\u95f4\",\"is_edit\":0,\"show_list\":1,\"is_time\":1},{\"field\":\"update_time\",\"type\":\"int(11)\",\"name\":\"\\u5904\\u7406\\u65f6\\u95f4\",\"column_comment\":\"\\u5904\\u7406\\u65f6\\u95f4\",\"is_edit\":0,\"show_list\":1,\"is_time\":1},{\"field\":\"from\",\"type\":\"varchar(10)\",\"name\":\"\\u6765\\u6e90\",\"column_comment\":\"\\u6765\\u6e90\",\"is_edit\":1,\"show_list\":1,\"is_time\":0}]', '[{\"id\":1,\"field\":\"id\",\"type\":\"int(11)\",\"name\":\"id\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":\"false\",\"is_time\":0,\"column_comment\":\"\",\"LAY_TABLE_INDEX\":0},{\"id\":2,\"field\":\"uid\",\"type\":\"int(11)\",\"name\":\"用户id\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":1,\"is_time\":0,\"column_comment\":\"用户id\",\"LAY_TABLE_INDEX\":1},{\"id\":3,\"field\":\"mid\",\"type\":\"int(11)\",\"name\":\"申请人id\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":1,\"is_time\":0,\"column_comment\":\"申请人id\",\"LAY_TABLE_INDEX\":2},{\"id\":4,\"field\":\"desc\",\"type\":\"varchar(255)\",\"name\":\"申请理由\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":1,\"is_time\":0,\"column_comment\":\"申请理由\",\"LAY_TABLE_INDEX\":3},{\"id\":5,\"field\":\"msg\",\"type\":\"varchar(255)\",\"name\":\"拒绝理由\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":1,\"is_time\":0,\"column_comment\":\"拒绝理由\",\"LAY_TABLE_INDEX\":4},{\"id\":6,\"field\":\"status\",\"type\":\"int(1)\",\"name\":\"状态\",\"field_type\":3,\"field_default\":\"[\\u0022待审核\\u0022,\\u0022已通过\\u0022,\\u0022已拒绝\\u0022]\",\"show_list\":1,\"is_edit\":1,\"is_time\":0,\"column_comment\":\"状态 0 待审核 1 已通过 2 已拒绝\",\"LAY_TABLE_INDEX\":5},{\"id\":7,\"field\":\"create_time\",\"type\":\"int(11)\",\"name\":\"申请时间\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":0,\"is_time\":1,\"column_comment\":\"申请时间\",\"LAY_TABLE_INDEX\":6},{\"id\":8,\"field\":\"update_time\",\"type\":\"int(11)\",\"name\":\"处理时间\",\"field_type\":1,\"field_default\":\"\",\"show_list\":0,\"is_edit\":0,\"is_time\":0,\"column_comment\":\"处理时间\",\"LAY_TABLE_INDEX\":7},{\"id\":9,\"field\":\"form\",\"type\":\"varchar(10)\",\"name\":\"来源\",\"field_type\":1,\"field_default\":\"\",\"show_list\":1,\"is_edit\":1,\"is_time\":0,\"column_comment\":\"来源\",\"LAY_TABLE_INDEX\":8}]', 9, 1, 0, 1642752803, 1642930443, 1642753133, 1, 0, 0, 1, '新的好友申请记录');

-- ----------------------------
-- Table structure for tu_payment
-- ----------------------------
DROP TABLE IF EXISTS `tu_payment`;
CREATE TABLE `tu_payment`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `out_trade_no` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '商品订单',
  `pay_trade_no` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '支付订单号',
  `money` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '支付金额',
  `status` int(11) NOT NULL DEFAULT 0 COMMENT '订单状态 0 默认 1 成功  2 部分退款 3整单退款',
  `order_type` int(11) NULL DEFAULT 1 COMMENT '支付的订单类型 1 普通订单支付',
  `attach` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '附加字段，例如多个订单合并支付时 记录多个订单的订单id，逗号分割',
  `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '0=未支付 , 1 微信支付 ',
  `client_ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '支付ip',
  `product_body` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '订单描述',
  `openid` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '支付的openid',
  `create_time` int(11) NULL DEFAULT NULL COMMENT '排序',
  `update_time` int(10) UNSIGNED NULL DEFAULT NULL COMMENT '订单更新时间',
  `is_delete` int(11) NULL DEFAULT 0 COMMENT '是否删除 1 删除 0 未删除',
  `tui_money` double(10, 2) NULL DEFAULT 0.00 COMMENT '退款金额',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `type`(`type`) USING BTREE,
  INDEX `out_trade_no`(`out_trade_no`) USING BTREE,
  INDEX `pay_trade_no`(`pay_trade_no`) USING BTREE,
  INDEX `status`(`status`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '支付表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tu_payment_refund
-- ----------------------------
DROP TABLE IF EXISTS `tu_payment_refund`;
CREATE TABLE `tu_payment_refund`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NULL DEFAULT NULL COMMENT '支付流水表记录id',
  `refund_sn` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '退款单号',
  `money` double(10, 2) NULL DEFAULT NULL COMMENT '退款金额',
  `create_time` int(11) NULL DEFAULT NULL COMMENT '退款时间',
  `client_ip` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '退款ip',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '支付流水退款记录表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tu_staff
-- ----------------------------
DROP TABLE IF EXISTS `tu_staff`;
CREATE TABLE `tu_staff`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) NOT NULL COMMENT '所属公司',
  `depart_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属部门 多个部门用逗号分割',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `mobile` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `head` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像',
  `priority` int(11) NULL DEFAULT 50 COMMENT '优先级 越大越优先',
  `status` int(1) NULL DEFAULT NULL COMMENT '状态 1 启用  2 禁用',
  `work_status` int(1) NULL DEFAULT 1 COMMENT '工作状态  1 正常 2 忙碌 3 挂起',
  `online` int(11) NULL DEFAULT 0 COMMENT '最后活动时间 低于300秒未活动 视为离线',
  `create_time` int(11) NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(11) NULL DEFAULT 0 COMMENT '更新时间',
  `login_time` int(11) NULL DEFAULT 0 COMMENT '最后登陆时间',
  `client_id` int(11) NULL DEFAULT 0 COMMENT '绑定client_id ',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '公司员工表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tu_staff
-- ----------------------------
INSERT INTO `tu_staff` VALUES (1, 24, '7', '小帮手技术客服-小杰', '1383838438', '', 'https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLhM5jmtTNVRibG151FI2xSrQbK5vFZ8X0oH3hcK7Eaic9LhVJujD0oiboIhwf5U0X3gpghJja9ZjVBw/132', 1, 1, 1, 1626143953, 1626141607, 1626338868, 0, 100020);

-- ----------------------------
-- Table structure for tu_staff_ticket
-- ----------------------------
DROP TABLE IF EXISTS `tu_staff_ticket`;
CREATE TABLE `tu_staff_ticket`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NULL DEFAULT 0 COMMENT '用户id',
  `ticket` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '登录凭证',
  `equi` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '设备信息',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '员工登录凭证表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tu_staff_ticket
-- ----------------------------
INSERT INTO `tu_staff_ticket` VALUES (1, 1, '1aabbed34ab34bef4664233bf5e824c0', 'Windows 98', '2021-07-15 16:54:53', '2021-07-19 15:15:13');

-- ----------------------------
-- Table structure for tu_upload_files
-- ----------------------------
DROP TABLE IF EXISTS `tu_upload_files`;
CREATE TABLE `tu_upload_files`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `storage` tinyint(1) NULL DEFAULT 0 COMMENT '存储位置 0本地',
  `app` smallint(4) NULL DEFAULT 0 COMMENT '来自应用 0前台 1后台',
  `user_id` int(11) NOT NULL DEFAULT 0 COMMENT '根据app类型判断用户类型',
  `file_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `file_size` int(11) NULL DEFAULT 0,
  `extension` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '文件后缀',
  `url` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '图片路径',
  `create_time` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tu_user_group
-- ----------------------------
DROP TABLE IF EXISTS `tu_user_group`;
CREATE TABLE `tu_user_group`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) NULL DEFAULT NULL,
  `admin_uid` int(11) NULL DEFAULT NULL COMMENT '群主id',
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '群名称',
  `member_number` int(11) NULL DEFAULT NULL COMMENT '群成员人数',
  `create_time` int(11) NULL DEFAULT NULL COMMENT '群创建时间',
  `update_time` int(11) NULL DEFAULT NULL COMMENT '群更新时间',
  `delete_time` int(11) NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '群组列表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tu_user_list
-- ----------------------------
DROP TABLE IF EXISTS `tu_user_list`;
CREATE TABLE `tu_user_list`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `mid` int(11) NULL DEFAULT NULL COMMENT '聊天对象id',
  `type` int(1) NULL DEFAULT 1 COMMENT '聊天类型  1 好友 2 群组',
  `create_time` int(11) NULL DEFAULT NULL COMMENT '创建时间(添加好友的时间)',
  `online` int(11) NULL DEFAULT NULL COMMENT '最后发送消息时间',
  `status` int(1) NULL DEFAULT 1 COMMENT '好友状态   1 正常  2 屏蔽  3 拉黑 4 删除  5 禁言(群聊可用)',
  `is_show` int(1) NULL DEFAULT 1 COMMENT '是否显示在消息列表 1 显示 2 隐藏',
  `remark` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注(好友备注)',
  `quiet` int(1) NULL DEFAULT 0 COMMENT '消息免打扰  1 免打扰 0 正常',
  `is_top` int(1) NULL DEFAULT 0 COMMENT '是否置顶 1 置顶 0 不置顶',
  `msg_reda_number` int(11) NULL DEFAULT 0 COMMENT '未读消息数量',
  `delete_time` int(10) NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '好友列表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tu_web_log
-- ----------------------------
DROP TABLE IF EXISTS `tu_web_log`;
CREATE TABLE `tu_web_log`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `uid` smallint(5) UNSIGNED NOT NULL COMMENT '用户id',
  `ip` char(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '访客ip',
  `location` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '访客地址',
  `os` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '操作系统',
  `browser` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '浏览器',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'url',
  `module` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '模块',
  `controller` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '控制器',
  `action` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '操作方法',
  `method` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'GET' COMMENT '请求类型',
  `data` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '请求的param数据，serialize后的',
  `otime` int(10) UNSIGNED NOT NULL COMMENT '操作时间',
  `response_data` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '响应数据，serialize后的',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `uid`(`uid`) USING BTREE,
  INDEX `ip`(`ip`) USING BTREE,
  INDEX `otime`(`otime`) USING BTREE,
  INDEX `module`(`module`) USING BTREE,
  INDEX `controller`(`controller`) USING BTREE,
  INDEX `method`(`method`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '网站日志' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;

