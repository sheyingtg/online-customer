<?php

return [
    //是否开启 主从服务器
    'isUseCluster'=>false,
    //主服务器
    'master'=>[
        'host'    => '127.0.0.1',
        'port'    => 6379,
        'pwd'    => 'zxc112234',
    ],
    //从服务器 可配置多台
    'slave'=>[]

];