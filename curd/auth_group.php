<?php
 return array (
  'table' => 'auth_group',
  'title' => '用户组表',
  'description' => '用户组表',
  'datetime_fields' => 
  array (
  ),
  'button_default' => 
  array (
    0 => 'create',
    1 => 'update',
    2 => 'delete',
    3 => 'refresh',
  ),
  'status' => false,
  'page' => true,
  'extend' => 
  array (
  ),
  'pk' => 'id',
  'button' => 
  array (
  ),
  'fields' => 
  array (
    'id' => 
    array (
      'title' => 'id',
      'field' => 'id',
      'default' => NULL,
      'weight' => 1,
      'search_type' => '_',
      'search' => '=',
      'search_extend' => 
      array (
      ),
      'table_type' => 'column',
      'table_format' => 
      array (
      ),
      'table_sort' => false,
      'table_extend' => 
      array (
      ),
      'form_type' => 'hidden',
      'marker' => '',
      'form_format' => 
      array (
      ),
      'form_extend' => 
      array (
      ),
      'save_format' => 
      array (
      ),
      'relation' => false,
      'option_type' => 'option_default',
      'option_config' => 
      array (
      ),
      'option_lang' => '',
      'option_relation' => '',
      'option_remote_relation' => '',
    ),
    'title' => 
    array (
      'title' => 'title',
      'field' => 'title',
      'default' => '',
      'weight' => 15,
      'search_type' => '_',
      'search' => '=',
      'search_extend' => 
      array (
      ),
      'table_type' => 'column',
      'table_format' => 
      array (
      ),
      'table_sort' => false,
      'table_extend' => 
      array (
      ),
      'form_type' => 'input',
      'marker' => '',
      'form_format' => 
      array (
      ),
      'form_extend' => 
      array (
      ),
      'save_format' => 
      array (
      ),
      'relation' => false,
      'option_type' => 'option_default',
      'option_config' => 
      array (
      ),
      'option_lang' => '',
      'option_relation' => '',
      'option_remote_relation' => '',
    ),
    'status' => 
    array (
      'title' => 'status',
      'field' => 'status',
      'default' => '1',
      'weight' => 20,
      'search_type' => '_',
      'search' => '=',
      'search_extend' => 
      array (
      ),
      'table_type' => 'column',
      'table_format' => 
      array (
      ),
      'table_sort' => false,
      'table_extend' => 
      array (
      ),
      'form_type' => 'input',
      'marker' => '',
      'form_format' => 
      array (
      ),
      'form_extend' => 
      array (
      ),
      'save_format' => 
      array (
      ),
      'relation' => false,
      'option_type' => 'option_default',
      'option_config' => 
      array (
      ),
      'option_lang' => '',
      'option_relation' => '',
      'option_remote_relation' => '',
    ),
    'rules' => 
    array (
      'title' => '规则id',
      'field' => 'rules',
      'default' => NULL,
      'weight' => 25,
      'search_type' => '_',
      'search' => '=',
      'search_extend' => 
      array (
      ),
      'table_type' => 'column',
      'table_format' => 
      array (
      ),
      'table_sort' => false,
      'table_extend' => 
      array (
      ),
      'form_type' => 'input',
      'marker' => '',
      'form_format' => 
      array (
      ),
      'form_extend' => 
      array (
      ),
      'save_format' => 
      array (
      ),
      'relation' => false,
      'option_type' => 'option_default',
      'option_config' => 
      array (
      ),
      'option_lang' => '',
      'option_relation' => '',
      'option_remote_relation' => '',
    ),
  ),
);