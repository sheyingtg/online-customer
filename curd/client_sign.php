<?php
 return array (
  'table' => 'client_sign',
  'title' => '客户端登录记录表',
  'description' => '客户端登录记录表',
  'datetime_fields' => 
  array (
    0 => 'create_time',
  ),
  'button_default' => 
  array (
    0 => 'create',
    1 => 'update',
    2 => 'delete',
    3 => 'refresh',
  ),
  'status' => false,
  'page' => true,
  'extend' => 
  array (
  ),
  'pk' => 'id',
  'button' => 
  array (
  ),
  'fields' => 
  array (
    'id' => 
    array (
      'title' => 'id',
      'field' => 'id',
      'default' => NULL,
      'weight' => 1,
      'search_type' => '_',
      'search' => '=',
      'search_extend' => 
      array (
      ),
      'table_type' => 'column',
      'table_format' => 
      array (
      ),
      'table_sort' => false,
      'table_extend' => 
      array (
      ),
      'form_type' => 'hidden',
      'marker' => '',
      'form_format' => 
      array (
      ),
      'form_extend' => 
      array (
      ),
      'save_format' => 
      array (
      ),
      'relation' => false,
      'option_type' => 'option_default',
      'option_config' => 
      array (
      ),
      'option_lang' => '',
      'option_relation' => '',
      'option_remote_relation' => '',
    ),
    'uid' => 
    array (
      'title' => '用户id',
      'field' => 'uid',
      'default' => NULL,
      'weight' => 15,
      'search_type' => '_',
      'search' => '=',
      'search_extend' => 
      array (
      ),
      'table_type' => 'column',
      'table_format' => 
      array (
      ),
      'table_sort' => false,
      'table_extend' => 
      array (
      ),
      'form_type' => 'input',
      'marker' => '',
      'form_format' => 
      array (
      ),
      'form_extend' => 
      array (
      ),
      'save_format' => 
      array (
      ),
      'relation' => false,
      'option_type' => 'option_default',
      'option_config' => 
      array (
      ),
      'option_lang' => '',
      'option_relation' => '',
      'option_remote_relation' => '',
    ),
    'ticket' => 
    array (
      'title' => '凭证',
      'field' => 'ticket',
      'default' => NULL,
      'weight' => 20,
      'search_type' => '_',
      'search' => '=',
      'search_extend' => 
      array (
      ),
      'table_type' => 'column',
      'table_format' => 
      array (
      ),
      'table_sort' => false,
      'table_extend' => 
      array (
      ),
      'form_type' => 'input',
      'marker' => '',
      'form_format' => 
      array (
      ),
      'form_extend' => 
      array (
      ),
      'save_format' => 
      array (
      ),
      'relation' => false,
      'option_type' => 'option_default',
      'option_config' => 
      array (
      ),
      'option_lang' => '',
      'option_relation' => '',
      'option_remote_relation' => '',
    ),
    'create_time' => 
    array (
      'title' => '创建时间',
      'field' => 'create_time',
      'default' => NULL,
      'weight' => 25,
      'search_type' => '_',
      'search' => '=',
      'search_extend' => 
      array (
      ),
      'table_type' => 'column',
      'table_format' => 
      array (
      ),
      'table_sort' => false,
      'table_extend' => 
      array (
      ),
      'form_type' => 'input',
      'marker' => '',
      'form_format' => 
      array (
      ),
      'form_extend' => 
      array (
      ),
      'save_format' => 
      array (
      ),
      'relation' => false,
      'option_type' => 'option_default',
      'option_config' => 
      array (
      ),
      'option_lang' => '',
      'option_relation' => '',
      'option_remote_relation' => '',
    ),
    'past_time' => 
    array (
      'title' => '过期时间',
      'field' => 'past_time',
      'default' => NULL,
      'weight' => 30,
      'search_type' => '_',
      'search' => '=',
      'search_extend' => 
      array (
      ),
      'table_type' => 'column',
      'table_format' => 
      array (
      ),
      'table_sort' => false,
      'table_extend' => 
      array (
      ),
      'form_type' => 'input',
      'marker' => '',
      'form_format' => 
      array (
      ),
      'form_extend' => 
      array (
      ),
      'save_format' => 
      array (
      ),
      'relation' => false,
      'option_type' => 'option_default',
      'option_config' => 
      array (
      ),
      'option_lang' => '',
      'option_relation' => '',
      'option_remote_relation' => '',
    ),
  ),
);