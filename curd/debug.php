<?php
 return array (
  'table' => 'debug',
  'title' => '调试日志表',
  'description' => '调试日志表',
  'datetime_fields' => 
  array (
  ),
  'button_default' => 
  array (
    0 => 'create',
    1 => 'update',
    2 => 'delete',
    3 => 'refresh',
  ),
  'status' => false,
  'page' => true,
  'extend' => 
  array (
  ),
  'pk' => 'id',
  'button' => 
  array (
  ),
  'fields' => 
  array (
    'id' => 
    array (
      'title' => 'id',
      'field' => 'id',
      'default' => NULL,
      'weight' => 1,
      'search_type' => '_',
      'search' => '=',
      'search_extend' => 
      array (
      ),
      'table_type' => 'column',
      'table_format' => 
      array (
      ),
      'table_sort' => false,
      'table_extend' => 
      array (
      ),
      'form_type' => 'hidden',
      'marker' => '',
      'form_format' => 
      array (
      ),
      'form_extend' => 
      array (
      ),
      'save_format' => 
      array (
      ),
      'relation' => false,
      'option_type' => 'option_default',
      'option_config' => 
      array (
      ),
      'option_lang' => '',
      'option_relation' => '',
      'option_remote_relation' => '',
    ),
    'uid' => 
    array (
      'title' => 'uid',
      'field' => 'uid',
      'default' => NULL,
      'weight' => 15,
      'search_type' => '_',
      'search' => '=',
      'search_extend' => 
      array (
      ),
      'table_type' => 'column',
      'table_format' => 
      array (
      ),
      'table_sort' => false,
      'table_extend' => 
      array (
      ),
      'form_type' => 'input',
      'marker' => '',
      'form_format' => 
      array (
      ),
      'form_extend' => 
      array (
      ),
      'save_format' => 
      array (
      ),
      'relation' => false,
      'option_type' => 'option_default',
      'option_config' => 
      array (
      ),
      'option_lang' => '',
      'option_relation' => '',
      'option_remote_relation' => '',
    ),
    'url' => 
    array (
      'title' => 'url',
      'field' => 'url',
      'default' => NULL,
      'weight' => 20,
      'search_type' => '_',
      'search' => '=',
      'search_extend' => 
      array (
      ),
      'table_type' => 'column',
      'table_format' => 
      array (
      ),
      'table_sort' => false,
      'table_extend' => 
      array (
      ),
      'form_type' => 'input',
      'marker' => '',
      'form_format' => 
      array (
      ),
      'form_extend' => 
      array (
      ),
      'save_format' => 
      array (
      ),
      'relation' => false,
      'option_type' => 'option_default',
      'option_config' => 
      array (
      ),
      'option_lang' => '',
      'option_relation' => '',
      'option_remote_relation' => '',
    ),
    'params' => 
    array (
      'title' => 'params',
      'field' => 'params',
      'default' => NULL,
      'weight' => 25,
      'search_type' => '_',
      'search' => '=',
      'search_extend' => 
      array (
      ),
      'table_type' => 'column',
      'table_format' => 
      array (
      ),
      'table_sort' => false,
      'table_extend' => 
      array (
      ),
      'form_type' => 'input',
      'marker' => '',
      'form_format' => 
      array (
      ),
      'form_extend' => 
      array (
      ),
      'save_format' => 
      array (
      ),
      'relation' => false,
      'option_type' => 'option_default',
      'option_config' => 
      array (
      ),
      'option_lang' => '',
      'option_relation' => '',
      'option_remote_relation' => '',
    ),
    'createtime' => 
    array (
      'title' => 'createtime',
      'field' => 'createtime',
      'default' => NULL,
      'weight' => 30,
      'search_type' => '_',
      'search' => '=',
      'search_extend' => 
      array (
      ),
      'table_type' => 'column',
      'table_format' => 
      array (
      ),
      'table_sort' => false,
      'table_extend' => 
      array (
      ),
      'form_type' => 'input',
      'marker' => '',
      'form_format' => 
      array (
      ),
      'form_extend' => 
      array (
      ),
      'save_format' => 
      array (
      ),
      'relation' => false,
      'option_type' => 'option_default',
      'option_config' => 
      array (
      ),
      'option_lang' => '',
      'option_relation' => '',
      'option_remote_relation' => '',
    ),
    'ip' => 
    array (
      'title' => 'ip',
      'field' => 'ip',
      'default' => NULL,
      'weight' => 35,
      'search_type' => '_',
      'search' => '=',
      'search_extend' => 
      array (
      ),
      'table_type' => 'column',
      'table_format' => 
      array (
      ),
      'table_sort' => false,
      'table_extend' => 
      array (
      ),
      'form_type' => 'input',
      'marker' => '',
      'form_format' => 
      array (
      ),
      'form_extend' => 
      array (
      ),
      'save_format' => 
      array (
      ),
      'relation' => false,
      'option_type' => 'option_default',
      'option_config' => 
      array (
      ),
      'option_lang' => '',
      'option_relation' => '',
      'option_remote_relation' => '',
    ),
    'type' => 
    array (
      'title' => 'type',
      'field' => 'type',
      'default' => NULL,
      'weight' => 40,
      'search_type' => '_',
      'search' => '=',
      'search_extend' => 
      array (
      ),
      'table_type' => 'column',
      'table_format' => 
      array (
      ),
      'table_sort' => false,
      'table_extend' => 
      array (
      ),
      'form_type' => 'input',
      'marker' => '',
      'form_format' => 
      array (
      ),
      'form_extend' => 
      array (
      ),
      'save_format' => 
      array (
      ),
      'relation' => false,
      'option_type' => 'option_default',
      'option_config' => 
      array (
      ),
      'option_lang' => '',
      'option_relation' => '',
      'option_remote_relation' => '',
    ),
    'header' => 
    array (
      'title' => 'header',
      'field' => 'header',
      'default' => NULL,
      'weight' => 45,
      'search_type' => '_',
      'search' => '=',
      'search_extend' => 
      array (
      ),
      'table_type' => 'column',
      'table_format' => 
      array (
      ),
      'table_sort' => false,
      'table_extend' => 
      array (
      ),
      'form_type' => 'input',
      'marker' => '',
      'form_format' => 
      array (
      ),
      'form_extend' => 
      array (
      ),
      'save_format' => 
      array (
      ),
      'relation' => false,
      'option_type' => 'option_default',
      'option_config' => 
      array (
      ),
      'option_lang' => '',
      'option_relation' => '',
      'option_remote_relation' => '',
    ),
  ),
);