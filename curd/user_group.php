<?php
 return array (
  'table' => 'user_group',
  'title' => '群组列表',
  'description' => '群组列表',
  'datetime_fields' => 
  array (
    0 => 'create_time',
    1 => 'update_time',
    2 => 'delete_time',
  ),
  'button_default' => 
  array (
    0 => 'create',
    1 => 'update',
    2 => 'delete',
    3 => 'refresh',
  ),
  'status' => true,
  'page' => true,
  'extend' => 
  array (
  ),
  'pk' => 'id',
  'button' => 
  array (
  ),
  'fields' => 
  array (
    'id' => 
    array (
      'title' => 'id',
      'field' => 'id',
      'default' => NULL,
      'weight' => 1,
      'search_type' => '_',
      'search' => '=',
      'search_extend' => 
      array (
      ),
      'table_type' => 'column',
      'table_format' => 
      array (
      ),
      'table_sort' => false,
      'table_extend' => 
      array (
      ),
      'form_type' => 'hidden',
      'marker' => '',
      'form_format' => 
      array (
      ),
      'form_extend' => 
      array (
      ),
      'save_format' => 
      array (
      ),
      'relation' => false,
      'option_type' => 'option_default',
      'option_config' => 
      array (
      ),
      'option_lang' => '',
      'option_relation' => '',
      'option_remote_relation' => '',
    ),
    'admin_id' => 
    array (
      'title' => 'admin_id',
      'field' => 'admin_id',
      'default' => '',
      'weight' => 15,
      'search_type' => '_',
      'search' => '=',
      'search_extend' => 
      array (
      ),
      'table_type' => '_',
      'table_format' => 
      array (
      ),
      'table_sort' => false,
      'table_extend' => 
      array (
      ),
      'form_type' => '_',
      'marker' => '',
      'form_format' => 
      array (
      ),
      'form_extend' => 
      array (
      ),
      'save_format' => 
      array (
        0 => 'getAdminId',
      ),
      'relation' => false,
      'option_type' => 'option_default',
      'option_config' => 
      array (
      ),
      'option_lang' => '',
      'option_relation' => 
      array (
        0 => '',
      ),
      'option_remote_relation' => '',
    ),
    'admin_uid' => 
    array (
      'title' => '群主id',
      'field' => 'admin_uid',
      'default' => '',
      'weight' => 20,
      'search_type' => 'take',
      'search' => 'IN',
      'search_extend' => 
      array (
        'url' => '/curd/page/client',
        'selection_label' => 'nickname',
        'limit' => '10',
      ),
      'table_type' => 'column',
      'table_format' => 
      array (
      ),
      'table_sort' => false,
      'table_extend' => 
      array (
      ),
      'form_type' => 'take',
      'marker' => '',
      'form_format' => 
      array (
      ),
      'form_extend' => 
      array (
        'url' => '/curd/page/client',
        'selection_label' => 'nickname',
      ),
      'save_format' => 
      array (
      ),
      'relation' => false,
      'option_type' => 'option_relation',
      'option_config' => 
      array (
      ),
      'option_lang' => '',
      'option_relation' => 
      array (
        0 => 'client',
        1 => 'id',
        2 => 'nickname',
      ),
      'option_remote_relation' => '',
    ),
    'name' => 
    array (
      'title' => '群名称',
      'field' => 'name',
      'default' => '',
      'weight' => 25,
      'search_type' => 'input',
      'search' => 'LIKE',
      'search_extend' => 
      array (
      ),
      'table_type' => 'column',
      'table_format' => 
      array (
      ),
      'table_sort' => false,
      'table_extend' => 
      array (
      ),
      'form_type' => 'input',
      'marker' => '',
      'form_format' => 
      array (
      ),
      'form_extend' => 
      array (
      ),
      'save_format' => 
      array (
      ),
      'relation' => false,
      'option_type' => 'option_default',
      'option_config' => 
      array (
      ),
      'option_lang' => '',
      'option_relation' => 
      array (
        0 => '',
      ),
      'option_remote_relation' => '',
    ),
    'member_number' => 
    array (
      'title' => '群成员人数',
      'field' => 'member_number',
      'default' => NULL,
      'weight' => 30,
      'search_type' => '_',
      'search' => '=',
      'search_extend' => 
      array (
      ),
      'table_type' => 'column',
      'table_format' => 
      array (
      ),
      'table_sort' => true,
      'table_extend' => 
      array (
      ),
      'form_type' => '_',
      'marker' => '',
      'form_format' => 
      array (
      ),
      'form_extend' => 
      array (
      ),
      'save_format' => 
      array (
      ),
      'relation' => false,
      'option_type' => 'option_default',
      'option_config' => 
      array (
      ),
      'option_lang' => '',
      'option_relation' => '',
      'option_remote_relation' => '',
    ),
    'create_time' => 
    array (
      'title' => '群创建时间',
      'field' => 'create_time',
      'default' => '',
      'weight' => 35,
      'search_type' => '_',
      'search' => '=',
      'search_extend' => 
      array (
      ),
      'table_type' => 'column',
      'table_format' => 
      array (
        0 => 'toDatetime',
      ),
      'table_sort' => false,
      'table_extend' => 
      array (
      ),
      'form_type' => 'date',
      'marker' => '',
      'form_format' => 
      array (
        0 => 'toDate',
      ),
      'form_extend' => 
      array (
      ),
      'save_format' => 
      array (
      ),
      'relation' => false,
      'option_type' => 'option_default',
      'option_config' => 
      array (
      ),
      'option_lang' => '',
      'option_relation' => 
      array (
        0 => '',
      ),
      'option_remote_relation' => '',
    ),
    'update_time' => 
    array (
      'title' => '群更新时间',
      'field' => 'update_time',
      'default' => NULL,
      'weight' => 40,
      'search_type' => '_',
      'search' => '=',
      'search_extend' => 
      array (
      ),
      'table_type' => '_',
      'table_format' => 
      array (
      ),
      'table_sort' => false,
      'table_extend' => 
      array (
      ),
      'form_type' => '_',
      'marker' => '',
      'form_format' => 
      array (
      ),
      'form_extend' => 
      array (
      ),
      'save_format' => 
      array (
      ),
      'relation' => false,
      'option_type' => 'option_default',
      'option_config' => 
      array (
      ),
      'option_lang' => '',
      'option_relation' => '',
      'option_remote_relation' => '',
    ),
    'delete_time' => 
    array (
      'title' => '删除时间',
      'field' => 'delete_time',
      'default' => '0',
      'weight' => 45,
      'search_type' => '_',
      'search' => '=',
      'search_extend' => 
      array (
      ),
      'table_type' => 'column',
      'table_format' => 
      array (
      ),
      'table_sort' => false,
      'table_extend' => 
      array (
      ),
      'form_type' => '_',
      'marker' => '',
      'form_format' => 
      array (
      ),
      'form_extend' => 
      array (
      ),
      'save_format' => 
      array (
      ),
      'relation' => false,
      'option_type' => 'option_default',
      'option_config' => 
      array (
      ),
      'option_lang' => '',
      'option_relation' => '',
      'option_remote_relation' => '',
    ),
  ),
);