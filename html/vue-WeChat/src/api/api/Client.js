import http from '../http'

// 获取好友信息
export const ApiGetUserInfo = (data) => { return http.get('/api/Client/getUserInfo',data)} 

// 查看好友申请列表
export const ApiGetNewFriends = (data) => { return http.get('/api/Client/getNewFriends',data)} 

//通过好友申请
export const ApiPassFriends = (data) => { return http.post('/api/Client/passFriends',data)} 

// 删除好友
export const ApideleteFriend = (data) => { return http.post('/api/Client/deleteFriend',data)} 

// 发起好友申请
export const ApiaddFrinedMsg = (data) => { return http.post('/api/Client/addFrinedMsg',data)} 

//查询好友申请状态
export const ApiGetNewFriendStatus = (data) => { return http.post('/api/Client/getNewFriendStatus',data)} 

