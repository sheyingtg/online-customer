//自动导入本文件夹目录下的所有资源图片
const files = require.context('.', false, /\.png$/)
const imageList = {}

files.keys().forEach((key) => {
    imageList[key.replace(/(\.\/|\.png)/g, '')] = files(key)
  })
  
  // 使用 <img :src="imageList['doc']" />
export default imageList;