//自动导入本文件夹目录下的所有资源图片
const files = require.context('.', false, /\.gif$/)
const imageList = {}

files.keys().forEach((key) => {
    imageList[key.replace(/(\.\/|\.gif)/g, '')] = files(key)
  })
  
export default imageList;