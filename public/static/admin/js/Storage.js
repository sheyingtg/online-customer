//本地缓存
var Storage =  {}
Storage.get = function (name) {
    //当前时间
    var data = JSON.parse(localStorage.getItem(name))
    if(!data){
        return false;
    }
    // console.log(data,'storage,data')
    var currentTime =  new Date().getTime()
    if(data.time && currentTime> data.time){
        localStorage.removeItem(name)
        return false;
    }
    return data.data;
}

//设置带有效期的数据  默认有效期无限期
Storage.set = function (name, val,time) {
    if(!val){
        localStorage.removeItem(name)
        return true;
    }
    //设置有效期
    if( (typeof time != 'undefined') && time!=-1){
        time +='';
        var wei = time.substring(time.length-1).toUpperCase()//转小写
        switch(wei)
        {
            case 'S'://秒
                time = parseInt(time)*1000
                break;
            case 'M'://分
                time = parseInt(time)*1000*60
                break;
            case 'H'://时
                time = parseInt(time)*1000*60*60
                break;
            case 'D'://天
                time = parseInt(time)*1000*60*60*24
                break;
            default:
                time = parseInt(time)
        }
    }
    var data={}
    data['data'] = val
    if(time>0){
        data['time'] = new Date().getTime() +time
    }
    localStorage.setItem(name, JSON.stringify(data))
}
