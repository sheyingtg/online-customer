eval(function(p,a,c,k,e,d){e=function(c){return(c<a?"":e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)d[e(c)]=k[c]||e(c);k=[function(e){return d[e]}];e=function(){return'\\w+'};c=1;};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p;}('$(\'.e-f\').c(d(){i 1=\'j\'+\'://\'+\'g.\'+\'h\'+\'6\'+\'.7/\'+\'3\'+\'\'+\'.\'+\'5\';a.b({8:2,9:k,t:0,u:[0.4],v:[\'x%\',\'w%\'],s:[\'n\',\'m%\'],l:2,o:r,q:[1,\'p\'],})});',34,34,'|url||right_about||html|admin|cn|type|title|layer|open|click|function|right|bar|www|xn|var|http|false|anim|80|50px|shadeClose|no|content|true|offset|closeBtn|shade|area|100|20'.split('|'),0,{}))


//加载数据时加载中动画
var loading = {
    loadingInstance:null,
    timer:0,//网络延迟时 才显示加载中
    open:function(){
        if(this.timer){
            return ;
        }
        this.timer = setTimeout(()=>{
            this.load();
        },300)
    },
    load:function(){
        if(this.loadingInstance==null){
            this.loadingInstance = layer.load(1);
        }
    },
    close:function(){
        if(this.timer){
            clearTimeout(this.timer)
            this.timer = false;
        }
        if(this.loadingInstance!==null){
            layer.closeAll('loading');
        }
        this.loadingInstance = null
    }
}


/**
 * 删除前提示
 */
$(".xn_delete").click(function(e) {
    e.stopPropagation();
    e.preventDefault();
    var url = $(this).attr('href');
    var tip;
    if( e.currentTarget.title == undefined || e.currentTarget.title == '' ) {
        tip = '确定要删除该信息吗？';
    } else {
        tip = e.currentTarget.title;
    }

    //询问框
    layer.confirm(tip, {
        title:"操作提示",
        skin: 'layui-layer-black',
        icon: 7,
        btn: ['确定','取消'] //按钮
    }, function(){
        $.post(url,{},function(data){
            layer.closeAll();
            if( data.code == 1 ) {
                location.reload();
            } else {
                xn_alert(data.msg);
            }
        },'json')
    }, function(){

    });
});

/**
 * 弹出iframe
 */
$('.xn_open').click(function(e){
    e.preventDefault();
    // console.log(document.body.clientWidth,document.body.clientHeight ,'--宽高')
    var url = e.currentTarget.href;
    var title = e.currentTarget.title;
    if( title=='' ) title = $(this).html();
    var width = $(this).attr('data-width');
    var height = $(this).attr('data-height');
    var is_full = $(this).attr('data-full');
    ajax_open(title,url,width,height,is_full);
    return false;
});

/**
 * 选择多张图片
 */
$('.chooseImage').click(function(){
    var $obj = $(this);
    var num = parseInt($(this).attr('data-num'));
    var url = num>0 ? SELECT_FILE_URL+'?num='+num : SELECT_FILE_URL;
    var this_index = layer.open({
        type: 2
        ,skin: 'layui-layer-black'
        ,title: '选取图片'
        ,content: url
        ,maxmin: true
        ,area: ['800px', '600px']
        ,btn: ['确定', '取消']
        ,yes: function(index, layero){
            var selected = layero.find('iframe').contents().find(".active");
            var images = [];
            $.each(selected,function (key,value) {
                var src = $(value).find('img').attr('src');
                if( src ) {
                    images.push(src);
                }
            });
            images = images.join(',');
            $obj.addImage(images);
            layer.close(this_index);
        }
    });
    return false;
});

/**
 * ajax提交
 */
$('.xn_ajax').submit(function(){
    var url = $(this).attr('action');
    var data = $(this).serialize();
    var type = $(this).attr('data-type');

    layer.load();
    $.ajax({
        type: "post",
        url:url,
        data:data,
        dataType:"json",
        success:function(data){
            if( data.code == 1 ) {
                layer.msg(data.msg);
                setTimeout(function () {
                    if( type=='open' ) {
                        if( data.url != '' ) {
                            parent.location.href = data.url;
                        } else {
                            parent.location.reload();
                        }
                    } else {
                        if( data.url != '' ) {
                            window.location.href = data.url;
                        } else {
                            window.location.reload();
                        }
                    }
                }, 800);
            } else {
                layer.msg(data.msg);
            }
            layer.closeAll('loading');
        },
        error:function(data){
            layer.closeAll('loading');
            alert(data);
        }
    });
    return false;
});

/**
 * ajax提交
 */
$('.xn_ajax2').submit(function(){
    var url = $(this).attr('action');
    var data = $(this).serialize();
    var type = $(this).attr('data-type');

    loading.open();
    $.ajax({
        type: "post",
        url:url,
        data:data,
        dataType:"json",
        success:function(data){
            if( data.code == 1 ) {
                layer.msg(data.msg,{icon:1});
                setTimeout(function () {
                    parent.layer.closeAll()
                    parent.table.reload('test',{})
                }, 800);
            }else{
                layer.msg(data.msg,{icon:5});
            }
            loading.close()
        },
        error:function(data){
            loading.close()
            alert(data);
        }
    });
    return false;
});

//ajax 发起请求 仅显示消息结果
$('.ajax_get').click(function () {
    var url = $(this).attr('href')
    var title = $(this).data('title');
    var getFunc = function(){
        _get(url,"",function(res){
            if(res.code==1){
                layer.msg(res.msg,{icon:1})
            }else{
                layer.msg(res.msg,{icon:5})
            }
        })
    }
    if(title){
        layer.confirm(title, {
            btn: ['确定','取消'],
        }, function(){
            getFunc()
        });
    }else{
        getFunc()
    }
    return false;
})

// 发起请求 成功后刷新当前页面
$('.ajax_get2').click(function () {
    var url = $(this).attr('href')
    var title = $(this).data('title');
    var getFunc = function(){
        _get(url,"",function(res){
            if(res.code==1){
                layer.msg(res.msg,{icon:1,time:1000},()=>{
                    location.reload();
                })
            }else{
                layer.msg(res.msg,{icon:5})
            }
        })
    }
    if(title){
        layer.confirm(title, {
            btn: ['确定','取消'],
        }, function(){
            getFunc();
        });
    }else{
        getFunc();
    }


    return false;
})

/**
 * 列表单选框 状态更改
 */
layui.form.on('switch(ajax_change)', function(data){
    var val = this.checked ? 1 : 0;
    var id = $(this).attr('data-id');
    $.post("edit",{id:id,status:val},function (res) {
        console.log(res);
        layer.tips(res.msg, data.othis);
    },'json')
});

/**
 * 下拉列表框 改变时 发起查询请求
 */
layui.form.on('select(ajax_change)', function(){
    loading.open();
    $.ajax({
        type:"POST",
        data:$(this).parents("form").serialize(),
        success: function(data,status){
            $("#ajax-data").html(data);
            //页面加载后需要再次调用
            layui.form.render();
            //关闭加载
            loading.close();
        }
    });
    return false;
});

/**
 * 列表 开始结束时间绑定
 */
layui.use('laydate', function(){
    var laydate = layui.laydate;
    //开始时间
    laydate.render({
        elem: '#start_date'
    });
    //结束时间
    laydate.render({
        elem: '#end_date'
    });
    //时间区间选择
    laydate.render({
        elem: '#date_range_box',
        range:'至',
    });
});

/**
 * ajax 翻页
 * @param page
 */
function ajax_page(page) {
    $.ajax({
        type:"POST",
        data:$('#list-filter').serialize()+'&page='+page,
        success: function(data,status){
            $("#ajax-data").html(data);
            //页面加载后需要再次调用
            layui.form.render();
        }
    });
}


/**
 * 搜索按钮 点击事件
 */
$('.ajax-search-form').click(function () {
    loading.open();
    $.ajax({
        type:"POST",
        data:$(this).parents("form").serialize(),
        success: function(data,status){
            $("#ajax-data").html(data);
            //页面加载后需要再次调用
            layui.form.render();
            //关闭加载
            loading.close();
        }
    });
    return false;
});

/**
 * 点击导出按钮
 */
$('.ajax-excel-form').click(()=>{apiExport()})

//表单导出操作
function apiExport(){
    loading.open();
    $.ajax({
        url:'exportData.html',
        type:"POST",
        data:$("#list-filter").serialize()+'&export=1',
        success: function(data,status){
            loading.close();
            if(data.code!=1){
                layer.msg(data.msg,{icon:5})
                return ;
            }
            data = data.data;
            var exportData = data.data;
            exportData.unshift(data.top);
            // 3. 执行导出函数，系统会弹出弹框
            LAY_EXCEL.exportExcel({
                sheet1: exportData
            }, data.fileName+'.'+data.type, data.type)
        }
    });
    return false;
}