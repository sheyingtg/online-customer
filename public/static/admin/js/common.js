
/**
 * 弹出msg，自动关掉
 * @param tip
 */
function xn_msg(msg){
    var time = arguments[1] ? arguments[1]*1000 : 2000;
    layer.msg(msg, {
        time: time
    });
}

/**
 * 弹出对话窗  不会自动关掉
 * @param msg
 */
function xn_alert(msg){
    var icon = arguments[1] ? arguments[1] : '';
    var btn_text = arguments[2] ? arguments[2] : '确定';
    if( icon!='' ) {
        layer.open({
            content: msg,
            skin: 'layui-layer-black',
            btn: btn_text,
            icon: icon,
            yes: function(index){
                layer.close(index);
            }
        });
    } else {
        layer.open({
            content: msg,
            skin: 'layui-layer-black',
            btn: btn_text,
            yes: function(index){
                layer.close(index);
            }
        });
    }
}

//弹框 关闭 回调事件
var ajax_open_end_callBack;
function ajax_open(title,url,width,height,is_full,type){
    type = type ||2;
    // 最大窗口
    var maxWidth = document.documentElement.clientWidth;
    var maxHeigh = document.documentElement.clientHeight;
    if(!width || width > maxWidth)
        width = maxWidth;
    if(!height || height > maxHeigh)
        height = maxHeigh-10;

    if( width == undefined || width == '' ) {
        width = '900px';
    }
    if( height == undefined || height == '' ) {
        height = '700px';
    }
    if(width%1==0) width =width + 'px'
    if(height%1==0) height = height +'px'

    //窗口小于500统一设置为 90%宽高
    var client_w =  document.body.clientWidth;
    if( client_w < 500 ) {
        width = '90%';
        height = '90%';
    }

    var this_index = layer.open({
        type: type
        ,skin: 'layui-layer-black'
        ,title: title
        ,content: url
        ,maxmin: true
        ,area: [width, height]
        /*,btn: ['确定', '取消']*/
        ,yes: function(index, layero){
            console.log(11111,'222')
            //点击确认触发 iframe 内容中的按钮提交
            /*var submit = layero.find('iframe').contents().find("#layui-submit");
            submit.click();*/
        },
        end:function(){
            //数据重载
            // table.reload('test',{})
        }
    });
    if( is_full == 1 ) {
        layer.full(this_index);
    }
    return this_index
}

/**
 * 确定后刷新
 * @param msg
 */
function xn_alert_reload(msg){
    var icon = arguments[1] ? arguments[1] : '';
    var btn_text = arguments[2] ? arguments[2] : '确定';
    if( icon!='' ) {
        layer.open({
            content: msg,
            skin: 'layui-layer-black',
            btn: btn_text,
            icon: icon,
            yes: function(index){
                location.reload();
            }
        });
    } else {
        layer.open({
            content: msg,
            skin: 'layui-layer-black',
            btn: btn_text,
            yes: function(index){
                location.reload();
            }
        });
    }
}

/**
 * 点击确定后跳转URL
 * @param msg
 * @param btn_text
 * @param btn_url
 */
function xn_alert_gourl(msg,btn_url){
    var btn_text = arguments[2] ? arguments[2] : '确定';
    layer.open({
        content: msg,
        skin: 'layui-layer-black',
        btn: btn_text,
        yes: function(){
            window.location.href = btn_url;
        }
    });
}

//封装通用的请求
function _post(url,data,callBack){
    loading.open();
    $.ajax({
        url:url,
        type:'post',
        dataType:"json",
        data:data,
        success:function(res){
            loading.close();
            if(res.code==1){
                callBack(res)
            }else{
                layer.msg(res.msg,{icon:5})
            }
        },
        error:(res)=>{
            loading.close();
            console.log("ajax错误：",res)
        }
    })
}

//封装通用的请求
function _get(url,data,callBack){
    loading.open();
    $.ajax({
        url:url,
        type:'get',
        dataType:"json",
        data:data,
        success:function(res){
            loading.close();
            if(res.code==1){
                callBack(res)
            }else{
                layer.msg(res.msg,{icon:5})
            }
        },
        error:(res)=>{
            loading.close();
            console.log("ajax错误：",res)
        }
    })
}

//复制指定内容
function copy_text(str,successMsg){
    var html = document.createElement('div');
    html.innerText = str;
    html.id = 'copy-text';
    document.body.appendChild(html);
    //复制指定标签内容
    var my_copy = (element) => {
        /*对标签id中文本进行全选*/
        var text = document.getElementById(element);
        if (document.body.createTextRange) {
            var range = document.body.createTextRange();
            range.moveToElementText(text);
            range.select();
        } else if (window.getSelection) {
            var selection = window.getSelection();
            var range = document.createRange();
            range.selectNodeContents(text);
            selection.removeAllRanges();
            selection.addRange(range);
        } else {
            alert("none");
            return false;
        }
        // 复制选中的内容
        if (document.execCommand('copy', false, null)) {
            return 1;
        } else {
            return 0;
        }
    }
    var res = my_copy(html.id);
    html.remove();
    if(res && successMsg){
        layer.msg(successMsg,{icon:1})
    }
    return res;
}