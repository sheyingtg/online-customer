
//websocket类
function myWebSocket(wsUrl,port){
    var web = {
        ws:null,
    };
    var timer=null;//心跳
    var timer2=null;
    //连接服务端
    var connect = function (){
        ws = new WebSocket('ws://'+wsUrl+':'+port)

        // ws = new WebSocket('wss://'+wsUrl+'/api/wss')

        // 当socket连接打开时，发起登录
        ws.onopen = web.onopen;
        // 绑定新消息方法
        ws.onmessage = web.onmessage;
        // 绑定关闭的方法
        ws.onclose = function(){
            if(timer){
                clearInterval(timer);
                timer = null;
            }
            vDom.setConnect('服务器连接断开，无法进行对话!');
            //尝试重连
            if(timer2) clearTimeout(timer2)
            timer2 = setTimeout(()=>{
                connect()
            },3000)
        };
        // ws错误
        ws.onerror = function(){
            vDom.setConnect('服务器连接失败，无法进行对话!');
            //尝试重连
            if(timer2) clearTimeout(timer2)
            timer2 = setTimeout(()=>{
                connect()
            },3000)
        }
        web.ws = ws
    }
    //

    //心跳
    var heartbeat = function(){
         if(timer) clearInterval(timer)
         timer = setInterval(()=>{
             console.log('发送心跳')
             web.send({type:'ping'})
         },30000)
    }

//连接建立时 发送登录信息
    web.onopen = function (){
        //服务器连接成功
        vDom.setConnect('');
        console.log(ticket,'---ticket')
        // 登录
        // var login_data = '{"type":"login","client_name":"'+name.replace(/"/g, '\\"')+'","room_id":'+room_id+'}';
        var login_data = {
            type:'login',
            ticket:ticket,//ticket 中已经包含需要的个人基本信息
        }
        web.send(login_data);
        //启用心跳
        heartbeat();
    }

//服务器端发来消息
    web.onmessage = function (e){
        // console.log('收到消息',e.data)
        var data = JSON.parse(e.data)
        switch (data['type']){
            //心跳
            case 'ping':
                web.send({type:'pong'});
                break;
                //某人加入聊天
            case 'join':
                console.log(data.userinfo,'----join ---data.userinfo')
                vDom.addNewUser(data.userinfo,1)
                break;
                //初始化 批量加入聊天
            case 'joinList':
                data.gukeList.length>0 && data.gukeList.forEach(item=>{
                    vDom.addNewUser(item,1)
                })
                data.kefuList.length>0 && data.kefuList.forEach(item=>{
                    vDom.addNewUser(item,1)
                })
                //切换当前聊天
                vDom.checkUser(vDom.currentChatUserIndex);
                break;
                //切换聊天对象
            case 'ChatUser':
                vDom.updataChatUser(data.userinfo)
                break;
                //收到新消息
            case 'say':
                vDom.addMessage(data);
                break;
            case 'my_say':
                vDom.addMessage2(data);
                break;
                //用户下线
            case 'offline':
            case 'Online':
                vDom.Upline(data);
                break;
                //消息已阅读 回显
            case 'is_redaCallback':
                vDom.is_redaCallback(data.to_user_id);
                break;
            default:
                break;
        }

    }

    //发送消息
    web.send = function(data){
        if(data.type){
            var datastr = JSON.stringify(data)
        }else{
            datastr = data
        }
        web.ws.send(datastr)
    }

    connect();

    //发起连接
    return web;
}


