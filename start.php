<?php
/**
 * run with command
 * php start.php start
 */

ini_set('display_errors', 'on');
use Workerman\Worker;

if(strpos(strtolower(PHP_OS), 'win') === 0)
{
//    exit("start.php not support windows, please use start_for_win.bat\n");
}

// 检查扩展
if(!extension_loaded('pcntl'))
{
//    exit("Please install pcntl extension. See http://doc3.workerman.net/appendices/install-extension.html\n");
}

if(!extension_loaded('posix'))
{
//    exit("Please install posix extension. See http://doc3.workerman.net/appendices/install-extension.html\n");
}

// 标记是全局启动
define('GLOBAL_START', 1);

defined('DS') or define('DS', DIRECTORY_SEPARATOR);
//项目根目录目录
define('APP_PATH', __DIR__);

//自动加载类
//require_once APP_PATH . '/app/socket/autoload.php';

require __DIR__ . '/vendor/autoload.php';


//启动  windows无法在一个php文件里初始化多个 worker 所以需要用下面的方式初始化
//new app\socket\controller\Config();


// 加载所有app/socket/start/start*.php，以便启动所有服务
foreach(glob(APP_PATH.'/app/socket/start/start*.php') as $start_file)
{
    require_once $start_file;
}

// 运行所有服务
Worker::runAll();
